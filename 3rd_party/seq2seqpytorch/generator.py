from __future__ import unicode_literals, print_function, division
import logging
import os
import sys

try:
        reload(sys)
        sys.setdefaultencoding('utf-8')
except:
        pass

from utils import attention_viz, decode_sentence
from utils.data_util import tokenizer, detokenizer

import torch

logger = logging.getLogger(__name__)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")




class Generator:


        def __init__(self, opts, model, train_dataset, experiment_dir, output_file):
                
                self.opts = opts
                self.model = model
                self.train_dataset = train_dataset
                self.experiment_dir = experiment_dir
                #self.output_file = os.path.join(self.experiment_dir, output_file)
                self.output_file = output_file
                self.counter = 0


        def generate_batch(self, batch, batch_lengths, raw_batch, idx_map, visualize=False):

                with torch.no_grad():

                        if self.opts.pointer_gen:
                                pg_src_batches, max_oovs, oovs = self.train_dataset.pg_batch(batch, raw_batch)
                        else:
                                pg_src_batches, max_oovs, oovs = None, 0, None

                        decoder_outputs, decoder_hidden, attn_weights, coverages, p_gens = self.model(src_seqs=batch, src_lengths=batch_lengths, pg_src_batches=pg_src_batches, max_oovs=max_oovs)

                        decoded_sentences = decode_sentence.decode(decoder_outputs, attn_weights, raw_batch, self.train_dataset.target_lang, self.opts.UNK_rep, oovs, self.opts.beam_size)
                        decoded_sentences = [decoded_sentences[i] for i in idx_map]

                        #raw_batch and attn_weights are only needed for visualization (this has to be done outside  the loop)
                        if visualize:
                                raw_batch = [raw_batch[i] for i in idx_map]
                                attn_weights = [attn_weights[i] for i in idx_map]

                        for seq_i, decoded_sentence in enumerate(decoded_sentences):

                                self.counter+=1
                                
                                _decoded_sentence = detokenizer(decoded_sentence).replace('<EOS>', '').strip()
                                print(self.counter, ': ', _decoded_sentence)
                                self.writer = open(self.output_file, 'w')
                                self.writer.write(_decoded_sentence + '\n')
                                self.writer.close()
                                
                                if visualize:

                                        save_path = os.path.join(self.experiment_dir, 'attention')
                                        if not os.path.exists(save_path): os.makedirs(save_path)
                                        file_name = os.path.join(save_path, str(self.counter))
                                        
                                        attention_viz.show_attention(tokenizer(raw_batch[seq_i]), decoded_sentence, attn_weights[seq_i], file_name)


        def generate(self, trans_file, visualize=False):

                def pad_seq(seq, max_length, PAD_id):
                        seq += [PAD_id for i in range(max_length - len(seq))]
                        return seq

                def prepare_batch(batch, raw_batch):
                        
                        max_length = max([len(seq) for seq in batch])
                        batch_lengths = torch.LongTensor([len(seq) for seq in batch]).to(device)
                        batch_lengths, idx_map = batch_lengths.sort(descending=True)
                        
                        batch = [pad_seq(seq, max_length, self.train_dataset.source_lang.PAD_id) for seq in batch]
                        batch = torch.LongTensor(batch).to(device)
                        
                        batch = batch[idx_map]
                        raw_batch = [raw_batch[i] for i in idx_map]
                        _, idx_map = idx_map.sort(0)

                        return batch, batch_lengths, raw_batch, idx_map



                batch = []
                raw_batch = []

                with open(trans_file) as trans_input:
                        for src_line in trans_input:
                                src_line = src_line.strip()

                                src_text = tokenizer(src_line)[:self.opts.src_max_length]
                                src_raw = detokenizer(src_text)

                                raw_batch.append(src_raw)
                                src_seq = [self.train_dataset.source_lang.word2index[token] for token in src_text]
                                batch.append(src_seq)

                                if len(batch) == len(raw_batch) == self.opts.batch_size:

                                        batch, batch_lengths, raw_batch, idx_map = prepare_batch(batch, raw_batch)
                                        self.generate_batch(batch, batch_lengths, raw_batch, idx_map, visualize)
                                        
                                        batch = []
                                        raw_batch = []
                

                # generate what is left
                if len(batch) > 0 and len(raw_batch) > 0:

                        batch, batch_lengths, raw_batch, idx_map = prepare_batch(batch, raw_batch)
                        self.generate_batch(batch, batch_lengths, raw_batch, idx_map, visualize)


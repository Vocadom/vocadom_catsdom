# Seq2SeqPytorch
A sequence-to-sequence library based on pythorch. It can be used to do concept-to-text generation, summarization, and translation.
&nbsp;

## Supported models
* Sequence to sequence with attention mechanism
* Pointer generator model with coverage mechanism
&nbsp;

## Setup
### Requirements
* http://download.pytorch.org/whl/cu80/torch-0.4.1-cp36-cp36m-linux_x86_64.whl
* torchtext==0.2.3
* matplotlib
* numpy
&nbsp;



## Quick start guide
#### 1. First, prerpocess your data
```
python preprocessor.py  --train_file <training_file_name > \
                        --val_file <validation_file_name> \
                        --src_train <path_to_source_train_file> \
                        --trg_train  <path_to_target_train_file> \
                        --src_val  <path_to_source_validation_file> \ 
                        --trg_val  <path_to_target_train_file> \
                        --save_path <path_to_save_data> \
                        --src_max_length <source_max_length> \
                        --trg_max_length <target_max_length>
```
#### 2. Training
```
python main.py train \
                -c config_file.json \
                -t <path_to_save_data>/<training_file_name> \
                -v <path_to_save_data>/<validation_file_name> \
                [--src_emb] <source_embeddings_file> \
                [--trg_emb] <target_embeddings_file>
```

#### 3. Resume training
```
python main.py resume -c config_file.json -t (...)
```

#### 4. Generate text
```
python main.py generate -c config_file.json -t (...) -g <file_to_generate_text_from> -o <file_to_save_generated_text>
```
&nbsp;


## Details of parameters in the configuration file

| Parameter             | Possible values     | Explanation                                                                                                  |
|-----------------------|---------------------|--------------------------------------------------------------------------------------------------------------|
| experiment_name       | sample_experiment   | a folder in ./experiments will be created under this name to store experiment files, e.g., checkpoints, etc. |
| rnn_type              | LSTM, GRU           | type of RNN cell                                                                                             |
| bi_rnn                | true, false         | whether to use bi-directional rnn for the encoder or not                                                     |
| enc_n_layers          | 1                   | number of encoder layers                                                                                     |
| dec_n_layers          | 1                   | number of decoder layers                                                                                     |
| hidden_size           | 1000                | number of hidden units for rnn cells and the attention                                                       |
| embedding_dim         | 620                 | word embedding dimension                                                                                     |
| batch_size            | 128                 | number of sequences in a batch                                                                               |
| vocab_size            | 50000               | vocabulary size                                                                                              |
| src_max_length        | 50                  | maximum length of source sequences                                                                           |
| trg_max_length        | 50                  | maximum length of target sequences                                                                           |
| bridge                | true, false         | whether to pass the last encoder hidden state to the decoder or not                                          |
| attention_type        | bahdanau, luong_dot | type of attention mechanism                                                                                  |
| coverage              | true, false         | whether to use coverage mechanism or not                                                                     |
| pointer_gen           | true, false         | use pointer generator or not                                                                                 |
| beam_size             | 4                   | beam size                                                                                                    |
| beam_length_norm      | 0.5                 | beam length normalization (penalty). between 0 - 1                                                           |
| clip_grad             | 2                   | clip gradients at this value                                                                                 |
| share_vocab           | true, false         | share vocabulary between encoder and decoder embeddings, of true, embedding weights will be shared too       |
| teacher_forcing_ratio | 0.5                 | ratio at which teacher forcing will be used                                                                  |
| lr                    | 2e-4                | learning rate value                                                                                          |
| lr_scheduler_factor   | 0.5                 | reduce learning rate by this value                                                                           |
| lr_scheduler_start_at | 4000                | start reducing only after this many steps                                                                    |
| lr_scheduler_patience | 10                  | if validation loss didn't reduce for n times, reduce the learning rate                                       |
| weight_decay          | 0.2                 | weight decay for the optimizer                                                                               |
| dropout               | 0.2                 | dropout value                                                                                                |
| UNK_rep               | true, false         | whether to replace oov words or not                                                                          |
| manual_seed           | 1234                | seed value for reproducibility                                                                               |
| print_loss_every      | 10                  | print loss every n iterations                                                                                |
| save_model_every      | 2000                | save model every n iterations                                                                                |
| plot_loss_every       | 1000                | plot loss every n iterations                                                                                 |
| evaluate_model_every  | 1000                | evaluate model on validation data every n iterations                                                         |
| total_nb_iter         | 128000              | total number of training iterations                                                                          |


#### Notes: 
* currently it only supports training on a single GPU

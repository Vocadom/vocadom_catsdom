# coding: utf-8
from __future__ import unicode_literals, print_function, division
import unicodedata
import os
import logging
from shutil import copyfile
import argparse

import torch
from torchtext import data
import torchtext.vocab as vocab

from models import EncoderRNN, DecoderRNN, Seq2seq, BeamSearch
from utils import data_util
from utils import config_util
from utils import checkpoint_util

from trainer import Trainer
from generator import Generator


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


parser = argparse.ArgumentParser(description='Main')
parser.add_argument('mode', help='mode type: train, resume, generate', default='train')
parser.add_argument('-c', '--config_file', help='configuration file', required=True)
parser.add_argument('-t', '--train_file', help='train file path', required=True)
parser.add_argument('-v', '--val_file', help='val file path', required=True)
parser.add_argument('-g', '--gen_file', help='txt file to generate from', required=False)
parser.add_argument('-o', '--output_file', help='file to save generated sequences', required=False)

parser.add_argument('--src_emb', help='pretrained source embeddings, e.g., glove.840B.300d', required=False)
parser.add_argument('--trg_emb', help='pretrained target embeddings, e.g., glove.840B.300d', required=False)

args = parser.parse_args()

mode = args.mode
config_file = args.config_file

train_file = args.train_file
val_file = args.val_file

src_emb = args.src_emb
trg_emb = args.trg_emb





# Load configuration file
config = config_util.Config(config_file)
opts = config.load()



#create experiment directory
experiment_dir = '../3rd_party/seq2seqpytorch/experiments/'
experiment_dir = os.path.join(experiment_dir, opts.experiment_name)
if not os.path.exists(experiment_dir): os.makedirs(experiment_dir)
else:
        if mode == 'train':
                raise Exception('%s already exists' % experiment_dir)

if mode == 'train':
        config_file_name = os.path.basename(config_file)
        copyfile(config_file, os.path.join(experiment_dir, config_file_name))


checkpoint_dir = os.path.join(experiment_dir, 'checkpoints/')
if not os.path.exists(checkpoint_dir): os.makedirs(checkpoint_dir)






# Set manual seed 
torch.manual_seed(opts.manual_seed)
torch.cuda.manual_seed_all(opts.manual_seed)
torch.backends.cudnn.deterministic = True






# Load data
train_dataset, val_dataset = data_util.load_data(train_file, val_file, opts.vocab_size, opts.src_max_length, opts.trg_max_length, opts.share_vocab)
assert (train_dataset.source_lang.PAD_id == train_dataset.target_lang.PAD_id) and (train_dataset.source_lang.UNK_id == train_dataset.target_lang.UNK_id) 

opts.UNK_id = train_dataset.target_lang.UNK_id
opts.PAD_id = train_dataset.target_lang.PAD_id
opts.SOS_id = train_dataset.target_lang.SOS_id
opts.EOS_id = train_dataset.target_lang.EOS_id




if opts.pointer_gen:
        assert opts.pointer_gen == opts.share_vocab, 'when using pointer_gen, share_vocab has to be set to true.'
        assert train_dataset.target_lang.n_words == train_dataset.source_lang.n_words



# Load pretrained embeddings
src_emb_vect = None
trg_emb_vect = None

if mode == 'train':

        if src_emb:
                src_emb_vect = train_dataset.dataset.fields['src'].vocab.load_vectors(src_emb)
                # vectors = vocab.Vectors(src_emb)
                # train_dataset.dataset.fields['src'].vocab.set_vectors(vectors.stoi, vectors.vectors, vectors.dim)
                # src_emb_vect = train_dataset.dataset.fields['src'].vocab.vectors
        if trg_emb:
                trg_emb_vect = train_dataset.dataset.fields['trg'].vocab.load_vectors(trg_emb)
                # vectors = vocab.Vectors(trg_emb)
                # train_dataset.dataset.fields['trg'].vocab.set_vectors(vectors.stoi, vectors.vectors, vectors.dim)
                # trg_emb_vect = train_dataset.dataset.fields['trg'].vocab.vectors







# Logger
log_format = '[%(asctime)s %(levelname)s] %(message)s'
logging.basicConfig(filename='{}/{}.log'.format(experiment_dir, opts.experiment_name), format=log_format, filemode='a', level=logging.INFO)
logger = logging.getLogger('seq2seq')




def build_encoder_rnn():

        encoder = EncoderRNN(vocab_size=train_dataset.source_lang.n_words,
                                                embedding_dim=opts.embedding_dim,
                                                hidden_size=opts.hidden_size,
                                                pad_id = opts.PAD_id, 
                                                n_layers=opts.enc_n_layers,
                                                bidirectional=opts.bi_rnn,
                                                dropout=opts.dropout,
                                                rnn_type=opts.rnn_type,
                                                embedding = src_emb_vect)
        return encoder



def build_decoder_rnn():

        decoder = DecoderRNN(vocab_size=train_dataset.target_lang.n_words,
                                                embedding_dim=opts.embedding_dim,
                                                hidden_size=opts.hidden_size,
                                                pad_id = opts.PAD_id, 
                                                sos_id=opts.SOS_id,
                                                eos_id=opts.EOS_id,
                                                unk_id=opts.UNK_id,
                                                max_length=opts.trg_max_length,
                                                n_layers=opts.dec_n_layers,
                                                bidirectional=opts.bi_rnn,
                                                dropout=opts.dropout,
                                                rnn_type=opts.rnn_type,
                                                embedding = trg_emb_vect,
                                                pointer_gen = opts.pointer_gen,
                                                coverage = opts.coverage, 
                                                attn_type = opts.attention_type)

        return decoder

def build_beam_search():

        decoder = BeamSearch(vocab_size=train_dataset.target_lang.n_words,
                                                embedding_dim=opts.embedding_dim,
                                                hidden_size=opts.hidden_size,
                                                pad_id = opts.PAD_id, 
                                                sos_id=opts.SOS_id,
                                                eos_id=opts.EOS_id,
                                                unk_id=opts.UNK_id,
                                                max_length=opts.trg_max_length,
                                                n_layers=opts.dec_n_layers,
                                                bidirectional=opts.bi_rnn,
                                                dropout=opts.dropout,
                                                rnn_type=opts.rnn_type,
                                                embedding = trg_emb_vect,
                                                pointer_gen = opts.pointer_gen,
                                                coverage = opts.coverage, 
                                                attn_type = opts.attention_type,
                                                beam_size = opts.beam_size,
                                                beam_length_norm = opts.beam_length_norm)
        return decoder



# Build the seq2seq model 
def build_model():

        encoder = build_encoder_rnn()

        decoder = build_decoder_rnn()

        if opts.share_vocab:
                encoder.embedding.weight = decoder.embedding.weight

        seq2seq = Seq2seq(encoder, decoder, opts).to(device)

        return seq2seq




# Build the seq2seq model for generation
def build_infer_model():

        encoder = build_encoder_rnn()

        if opts.beam_size > 1:
                decoder = build_beam_search()
        else:
                decoder = build_decoder_rnn()

        if opts.share_vocab:
                encoder.embedding.weight = decoder.embedding.weight

        seq2seq = Seq2seq(encoder, decoder, opts).to(device)

        return seq2seq




if mode == 'train':

        # Initialize models
        seq2seq = build_model()
        print(seq2seq.summary())
        print(config.summary())

        logger.info(seq2seq.summary())
        logger.info(config.summary())

        trainer = Trainer(opts, seq2seq, train_dataset, val_dataset, experiment_dir, checkpoint_dir)
        trainer.train(mode)


elif mode == 'resume':


        # Initialize models
        seq2seq = build_model()
        print(config.summary())
        logger.info(config.summary())


        trainer = Trainer(opts, seq2seq, train_dataset, val_dataset, experiment_dir, checkpoint_dir)
        trainer.train(mode)


elif mode == 'generate':

        gen_file = args.gen_file
        output_file = args.output_file


        seq2seq = build_infer_model()

        checkpoint = checkpoint_util.load_checkpoint(checkpoint_dir=checkpoint_dir)
        seq2seq.load_state_dict(checkpoint['model_state_dict'])
        generator = Generator(opts, seq2seq, train_dataset, experiment_dir, output_file)

        mode = 0o600
        os.mkfifo("fifoNLUIn", mode)
        os.mkfifo("fifoNLUOut", mode)
        with open("fifoNLUIn","w") as fifoOut:
                fifoOut.write("open\n")
                fifoOut.flush()
                with open("fifoNLUOut","r") as fifoIn:
                        while True:
                                fifoIn.readline()
                                fifoIn.flush()
                                generator.generate(gen_file, visualize=False)
                                fifoOut.write("done\n");
                                fifoOut.flush()


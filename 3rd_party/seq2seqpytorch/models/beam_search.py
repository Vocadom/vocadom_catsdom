import random
import warnings
import heapq
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

from .attention import Attention
from .pointer_generator import PointerGenerator


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

'''
import time

prev_time = time.time()
def measure_time(msg):
	global prev_time
	end_time = time.time()
	print(msg, ',', end_time - prev_time)
	prev_time = end_time
'''


class Beam(object):
	def __init__(self, token, log_prob, hidden, coverage, p_gen, attn_weight, beam_length_norm):
		self.token = token
		self.log_prob = log_prob
		self.hidden = hidden
		self.coverage = coverage
		self.p_gen = p_gen
		self.attn_weight = attn_weight
		self.beam_length_norm = beam_length_norm

	def extend(self, token, log_prob, hidden, coverage, p_gen, attn_weight):
		return Beam(token = self.token + [token],
					log_prob = self.log_prob + [log_prob],
					hidden = hidden,
					coverage = self.coverage + [coverage] if coverage is not None else None,
					p_gen = self.p_gen + [p_gen] if p_gen is not None else None,
					attn_weight = self.attn_weight + [attn_weight] if attn_weight is not None else None,
					beam_length_norm= self.beam_length_norm)

	@property
	def latest_token(self):
		return self.token[-1]


	@property
	def avg_log_prob(self):

		if self.beam_length_norm > 0:

			# http://opennmt.net/OpenNMT/translation/beam_search/
			length_normalization_const = 5
			length_penalty = (length_normalization_const + len(self.token)) / (length_normalization_const + 1)

			return sum(self.log_prob)/(length_penalty ** self.beam_length_norm)
		else:
			return sum(self.log_prob)


	def __cmp__(self, other):
		"""Compares Sequences by score."""
		assert isinstance(other, Beam)
		if self.avg_log_prob == other.avg_log_prob:
			return 0
		elif self.avg_log_prob < other.avg_log_prob:
			return -1
		else:
			return 1

	# For Python 3 compatibility (__cmp__ is deprecated).
	def __lt__(self, other):
		assert isinstance(other, Beam)
		return self.avg_log_prob < other.avg_log_prob

	# Also for Python 3 compatibility.
	def __eq__(self, other):
		assert isinstance(other, Beam)
		return self.avg_log_prob == other.avg_log_prob




class TopBeams(object):
	"""Maintains the top n elements of an incrementally provided set."""

	def __init__(self, n):
		self._n = n
		self._data = []

	def size(self):
		assert self._data is not None
		return len(self._data)

	def push(self, x):
		"""Pushes a new element."""
		assert self._data is not None
		if len(self._data) < self._n:
			heapq.heappush(self._data, x)
		else:
			heapq.heappushpop(self._data, x)

	def extract(self, sort=False):
		"""Extracts all elements from the TopBeams. This is a destructive operation.
		The only method that can be called immediately after extract() is reset().
		Args:
		  sort: Whether to return the elements in descending sorted order.
		Returns:
		  A list of data; the top n elements provided to the set.
		"""
		assert self._data is not None
		data = self._data
		self._data = None
		if sort:
			data.sort(reverse=True)
		return data

	def reset(self):
		"""Returns the TopBeams to an empty state."""
		self._data = []


class BeamSearch(nn.Module):


	def __init__(self, vocab_size, embedding_dim, hidden_size, pad_id,
				 sos_id, eos_id, unk_id, max_length, n_layers=1, bidirectional=False,
				 dropout=0, rnn_type='GRU', use_attention=True, embedding=None,
				 update_embedding=True, pointer_gen=False, coverage=False,
				 attn_type='bahdanau', beam_size=1, beam_length_norm=0):

		super(BeamSearch, self).__init__()

		self.vocab_size = vocab_size
		self.embedding_dim = embedding_dim
		self.hidden_size = hidden_size
		self.n_layers = n_layers
		self.bidirectional = bidirectional
		self.dropout = dropout if self.n_layers > 1 else 0
		self.use_attention = use_attention
		self.pad_id = pad_id
		self.sos_id = sos_id
		self.eos_id = eos_id
		self.unk_id = unk_id
		self.max_length = max_length
		self.rnn_type = rnn_type
		self.coverage = coverage
		self.pointer_gen = pointer_gen
		self.beam_size = beam_size
		self.beam_length_norm = beam_length_norm



		self.embedding = nn.Embedding(self.vocab_size, self.embedding_dim, padding_idx=self.pad_id)
		if embedding is not None:
			self.embedding.weight = nn.Parameter(embedding)
		self.embedding.weight.requires_grad = update_embedding

		self.rnn = getattr(nn, self.rnn_type)(embedding_dim, self.hidden_size, n_layers, batch_first=True, dropout=self.dropout)
		self.input_dropout = nn.Dropout(p=self.dropout)

		if use_attention:
			self.attention = Attention(self.hidden_size, coverage=coverage, attn_type=attn_type)

		if pointer_gen:
			self.pointer_generator = PointerGenerator(self.hidden_size, self.embedding_dim, self.vocab_size)


		self.out = nn.Linear(self.hidden_size, self.vocab_size)




	def forward_step(self, trg_seq, hidden, encoder_outputs, src_lengths, coverage=None, pg_src_batches=None, max_oovs=0):

		batch_size = trg_seq.size(0)
		trg_size = trg_seq.size(1)

		embedded = self.embedding(trg_seq)
		embedded = self.input_dropout(embedded)

		# Run the forward pass of the RNN.
		outputs, hidden = self.rnn(embedded, hidden)

		# Calculate the attention.
		if self.use_attention:
			outputs, attn_weights, context_v = self.attention(
				outputs.contiguous(),
				encoder_outputs,
				src_lengths=src_lengths,
				coverage = coverage
			)


		outputs = self.out(outputs.contiguous().view(-1, self.hidden_size))

		p_gen = None
		
		if self.pointer_gen:
	
			outputs = F.softmax(outputs, dim=1)

			if isinstance(hidden, tuple):
				hidden_ = hidden[0] #LSTM
			else:
				hidden_ = hidden #GRU

			outputs, p_gen = self.pointer_generator(context_v, hidden_, embedded, outputs, attn_weights, pg_src_batches, max_oovs)

			outputs = torch.log(outputs).view(batch_size, trg_size, -1)
		
		else:
			outputs = F.log_softmax(outputs, dim=1).view(batch_size, trg_size, -1)

		return outputs, hidden, attn_weights, coverage, p_gen





	def forward(self, trg_seqs=None, encoder_hidden=None, encoder_outputs=None, src_lengths=None,
				teacher_forcing_ratio=0, coverage=None, pg_src_batches=None, max_oovs=0):

		src_size = encoder_outputs.size(1)
		batch_size  = encoder_outputs.size(0)

		hidden = self._init_state(encoder_hidden)


		decoder_input = torch.LongTensor([self.sos_id] * batch_size).view(batch_size, 1).to(device)
		step_outputs, hidden, step_attn, coverage, p_gen = self.forward_step(decoder_input, hidden, encoder_outputs, src_lengths, coverage, pg_src_batches, max_oovs)
		log_prob, candidates = torch.topk(step_outputs, self.beam_size)
				
		if coverage is not None:
			# Update the coverage attention.
			coverage = coverage + step_attn.squeeze(1)

		log_prob = log_prob.squeeze(1)
		candidates = candidates.squeeze(1)


		all_beams = [TopBeams(self.beam_size) for b in range(batch_size)]
		finished_beams = [TopBeams(self.beam_size) for b in range(batch_size)]





		for b in range(batch_size):
			
			if isinstance(hidden, tuple):
				hidden_ = (hidden[0][:, b, :], hidden[1][:, b, :]) #LSTM
			else:
				hidden_ = hidden[:, b, :] #GRU

			for k in range(self.beam_size):

				beam = Beam(token=[candidates[b][k]],
						  log_prob=[log_prob[b][k]],
						  hidden=hidden_,
						  coverage=[coverage[b]] if self.coverage else None,
						  p_gen = [p_gen[b]] if self.pointer_gen else None,
						  attn_weight = [step_attn[b]] if self.use_attention else None,
						  beam_length_norm=self.beam_length_norm)

				all_beams[b].push(beam)

		#inflate encoder outputs to match beam_size*batch_size
		encoder_outputs = torch.stack([encoder_outputs[b] for b in range(batch_size) for k in range(self.beam_size)])

		#inflate pg_src_batches to match beam_size*batch_size
		if self.pointer_gen:
			pg_src_batches = torch.stack([pg_src_batches[b] for b in range(batch_size) for k in range(self.beam_size)])


		for di in range(self.max_length - 1):

			tmp_all_beams = [beam.extract() for beam in all_beams]
			for beam in all_beams: beam.reset()

			latest_token = [[k.latest_token if k.latest_token < self.vocab_size else self.unk_id] for b in range(batch_size) for k in tmp_all_beams[b]]
			decoder_input = torch.LongTensor(latest_token).to(device)



			if isinstance(hidden, tuple):

				hidden_h =[]
				hidden_c = []
				for b in range(batch_size):
					for k in tmp_all_beams[b]:
						state_h, state_c = k.hidden
						hidden_h.append(state_h)
						hidden_c.append(state_c)
						
				hidden = (torch.stack(hidden_h, 0).transpose(0,1).contiguous(), torch.stack(hidden_c, 0).transpose(0,1).contiguous())
			else:
				hidden_h = []
				for b in range(batch_size):
					for k in tmp_all_beams[b]:
						state_h,  = k.hidden
						hidden_h.append(state_h)
				hidden = torch.stack(hidden_h, 0).unsqueeze(0).contiguous()

			if self.coverage:
				all_coverage = []
				for b in range(batch_size):
					for k in tmp_all_beams[b]:
						all_coverage.append(k.coverage[-1])

				coverage = torch.stack(all_coverage, 0)

				
			step_outputs, hidden, step_attn, coverage, p_gen = self.forward_step(decoder_input, hidden, encoder_outputs, src_lengths, coverage, pg_src_batches, max_oovs)
			log_prob, candidates = torch.topk(step_outputs, self.beam_size + 1)

			if coverage is not None:
				# Update the coverage attention.
				coverage = coverage + step_attn.squeeze(1)
				
			log_prob = log_prob.squeeze(1)
			candidates = candidates.squeeze(1)


			idx_counter = 0

			for b in range(batch_size):
				
				for k in tmp_all_beams[b]:

					cand = 0
					num_hyp = 0

					while num_hyp < self.beam_size:
						token_ = candidates[idx_counter][cand]
						log_prob_ = log_prob[idx_counter][cand]

						if isinstance(hidden, tuple):
							hidden_ = (hidden[0][:, idx_counter, :],hidden[1][:, idx_counter, :]) #LSTM
						else:
							hidden_ = hidden[:, idx_counter, :] #GRU


						cand+=1
						num_hyp+=1

						new_beam = k.extend(token=token_,
											log_prob=log_prob_,
											hidden=hidden_,
											coverage=coverage[idx_counter] if self.coverage else None,
											p_gen=p_gen[idx_counter] if self.pointer_gen else None,
											attn_weight=step_attn[idx_counter] if self.use_attention else None)
						
						if token_ == self.eos_id:
							finished_beams[b].push(new_beam)
							num_hyp-=1
						else:
							all_beams[b].push(new_beam)

					idx_counter+=1



		for b in range(batch_size):
			if not finished_beams[b].size():
				finished_beams[b] = all_beams[b]


		final_beams = [finished_beams[b].extract(sort=True)[0] for b in range(batch_size)]
		

		outputs = torch.ones(batch_size, self.max_length, dtype=torch.int32).to(device)
		attn_weights = torch.zeros(batch_size, self.max_length, src_size).to(device)
		coverages = torch.zeros(batch_size, self.max_length, src_size).to(device)
		p_gens = torch.zeros(batch_size, self.max_length).to(device)


		for b in range(batch_size):
			# skip this first token (BOS)
			for d_i in range(len(final_beams[b].token)):
				outputs[b:, d_i] = final_beams[b].token[d_i]
				attn_weights[b:, d_i] = final_beams[b].attn_weight[d_i]

				if self.coverage:
					coverages[b:, d_i] = final_beams[b].coverage[d_i]
				if self.pointer_gen:
					p_gens[b:, d_i] = final_beams[b].p_gen[d_i]


		return outputs, hidden, attn_weights, coverages, p_gens





	def _init_state(self, encoder_hidden):
		""" Initialize the decoder hidden state. """

		def _cat_directions(h):
			""" If the encoder is bidirectional, do the following transformation.
				(#directions * #layers, #batch, hiddensize) -> (#layers, #batch, #directions * hiddensize)
			"""
			if self.bidirectional:
				h = torch.cat([h[0:h.size(0):2], h[1:h.size(0):2]], 2)
			return h


		if encoder_hidden is None:
			return None
		if isinstance(encoder_hidden, tuple):
			encoder_hidden = tuple([_cat_directions(h) for h in encoder_hidden])
		else:
			encoder_hidden = _cat_directions(encoder_hidden)
		return encoder_hidden


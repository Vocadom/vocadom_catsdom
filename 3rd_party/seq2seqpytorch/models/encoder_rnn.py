import torch.nn as nn
import torch

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

class EncoderRNN(nn.Module):


	def __init__(self, vocab_size, embedding_dim, hidden_size, pad_id, n_layers=1, bidirectional=False, 
				 dropout=0, rnn_type='GRU', embedding=None, update_embedding=True):
	
		super(EncoderRNN, self).__init__()

		self.vocab_size = vocab_size
		self.embedding_dim = embedding_dim
		self.bidirectional = bidirectional
		self.n_directions = 2 if self.bidirectional else 1
		assert hidden_size % self.n_directions == 0
		self.hidden_size = hidden_size // self.n_directions
		self.pad_id = pad_id
		self.n_layers = n_layers
		self.dropout = dropout if self.n_layers > 1 else 0
		self.rnn_type = rnn_type
		
		self.embedding = nn.Embedding(self.vocab_size, self.embedding_dim, padding_idx=self.pad_id)
		if embedding is not None:
			self.embedding.weight = nn.Parameter(embedding)
		self.embedding.weight.requires_grad = update_embedding


		self.rnn = getattr(nn, self.rnn_type)(self.embedding_dim, self.hidden_size, self.n_layers, batch_first=True, 
											  bidirectional=self.bidirectional, dropout=self.dropout)
		self.input_dropout = nn.Dropout(p=self.dropout)



	def forward(self, src_seqs, src_lengths=None):

		embedded = self.embedding(src_seqs)
		embedded = self.input_dropout(embedded)

		if src_lengths is not None:
			embedded = nn.utils.rnn.pack_padded_sequence(embedded, src_lengths, batch_first=True)

		outputs, hidden = self.rnn(embedded)

		if src_lengths is not None:
			outputs, _ = nn.utils.rnn.pad_packed_sequence(outputs, batch_first=True)


		return outputs, hidden

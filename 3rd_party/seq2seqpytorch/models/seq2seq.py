import torch.nn as nn
import torch

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")



class Seq2seq(nn.Module):

	def __init__(self, encoder, decoder, opts):
		super(Seq2seq, self).__init__()
		self.encoder = encoder
		self.decoder = decoder
		self.bridge = opts.bridge
		self.use_coverage = opts.coverage

	def forward(self, src_seqs, src_lengths=None, trg_seqs=None, pg_src_batches=None, max_oovs=None, teacher_forcing_ratio=0):

		batch_size = src_seqs.size(0)
		src_size = src_seqs.size(1)


		encoder_outputs, encoder_hidden = self.encoder(src_seqs=src_seqs, src_lengths=src_lengths)


		if self.use_coverage:
			coverage = torch.zeros(batch_size, src_size).to(device) # coverage vector [Batch x SrcTimeSteps]
		else:
			coverage = None


		if self.bridge:
			if self.encoder.n_layers != self.decoder.n_layers:
				if isinstance(encoder_hidden, tuple): # When using LSTM
					encoder_hidden = tuple([h[-self.encoder.n_directions:].repeat(self.decoder.n_layers, 1, 1) for h in encoder_hidden])
				else: # GRU
					encoder_hidden = encoder_hidden[-self.encoder.n_directions:].repeat(self.decoder.n_layers, 1, 1) 
		else:
			encoder_hidden = None

		decoder_outputs, decoder_hidden, attn_weights, coverage, p_gens = self.decoder(trg_seqs=trg_seqs,
																				      encoder_hidden=encoder_hidden,
																				      encoder_outputs=encoder_outputs,
																				      src_lengths = src_lengths,
																				      teacher_forcing_ratio=teacher_forcing_ratio,
																				      coverage=coverage,
																				      pg_src_batches = pg_src_batches,
																				      max_oovs=max_oovs)
		
		return decoder_outputs, decoder_hidden, attn_weights, coverage, p_gens



	def summary(self):

		output_string = ''
		output_string += '='*100 + '\n'
		output_string += 'Model summary:' + '\n'
		output_string += str(self.encoder) + '\n'
		output_string += str(self.decoder) + '\n'


		total_enc_params = sum(p.numel() for p in self.encoder.parameters() if p.requires_grad)
		total_dec_params = sum(p.numel() for p in self.decoder.parameters() if p.requires_grad)
		total_params = total_enc_params + total_dec_params

		output_string += 'Total nb of trainable parameters: ' + str(total_params) + '\n'
		output_string += '='*100 + '\n'

		return output_string



import torch
import torch.nn as nn
import torch.nn.functional as F

# https://3qeqpr26caki16dnhd19sv6by6v-wpengine.netdna-ssl.com/wp-content/uploads/2017/10/Encoder-Decoder-Architecture-for-Neural-Machine-Translation.png
class Attention(nn.Module):

	def __init__(self, dim, coverage=False, attn_type='bahdanau'):
		super(Attention, self).__init__()

		self.dim = dim
		self.attn_type = attn_type

		if self.attn_type == 'bahdanau':
			self.linear_query = nn.Linear(dim, dim, bias=True)
			self.linear_context = nn.Linear(dim, dim, bias=False)
			self.v = nn.Linear(dim, 1, bias=False)
			self.linear_out = nn.Linear(dim * 2, dim, bias=True)

		elif self.attn_type == 'luong_dot':
			self.linear_out = nn.Linear(dim * 2, dim, bias=False)

		if coverage:
			self.linear_cover = nn.Linear(1, dim, bias=False)


	def set_mask(self, mask):
		self.mask = mask



	def score(self, h_t, h_s):

		src_batch, src_len, src_dim = h_s.size()
		tgt_batch, tgt_len, tgt_dim = h_t.size()
		dim = self.dim

		if self.attn_type == 'bahdanau':
			wq = self.linear_query(h_t.view(-1, dim))
			wq = wq.view(tgt_batch, tgt_len, 1, dim)
			wq = wq.expand(tgt_batch, tgt_len, src_len, dim)

			uh = self.linear_context(h_s.contiguous().view(-1, dim))
			uh = uh.view(src_batch, 1, src_len, dim)
			uh = uh.expand(src_batch, tgt_len, src_len, dim)

			# (batch, t_len, s_len, d)
			wquh = torch.tanh(wq + uh)

			return self.v(wquh.view(-1, dim)).view(tgt_batch, tgt_len, src_len)

		elif self.attn_type == 'luong_dot':
			h_s_ = h_s.transpose(1, 2)
			return torch.bmm(h_t, h_s_)



	def forward(self, dec_output, enc_outputs, src_lengths=None, coverage=None):


		batch, source_l, dim = enc_outputs.size()
		batch_, target_l, dim_ = dec_output.size()


		if coverage is not None:
			coverage = coverage.view(-1).unsqueeze(1)
			enc_outputs += self.linear_cover(coverage).view_as(enc_outputs)
			enc_outputs = torch.tanh(enc_outputs)


		# compute attention scores
		align = self.score(dec_output, enc_outputs)

		if src_lengths is not None:
			mask = self.sequence_mask(src_lengths, max_len=align.size(-1))
			mask = mask.unsqueeze(1)  # Make it broadcastable.
			if align.size() != mask.size():
				mask = mask.repeat(align.size(0)//mask.size(0), 1, 1) # TODO: double check it
			align.masked_fill_(~mask , -float('inf'))


		# Softmax to normalize attention weights
		attn = F.softmax(align.view(batch*target_l, source_l), -1)
		attn = attn.view(batch, target_l, source_l)


		# each context vector c_t is the weighted average
		# over all the source hidden states
		context = torch.bmm(attn, enc_outputs)


		# concatenate
		concat_context = torch.cat([context, dec_output], 2).view(batch*target_l, dim*2)
		output = self.linear_out(concat_context).view(batch, target_l, dim)


		if self.attn_type == 'luong_dot':
			output = torch.tanh(output)

		return output, attn, context





	def sequence_mask(self, lengths, max_len=None):
		"""
		Creates a boolean mask from sequence lengths.
		"""
		batch_size = lengths.numel()
		max_len = max_len or lengths.max()
		return (torch.arange(0, max_len)
				.type_as(lengths)
				.repeat(batch_size, 1)
				.lt(lengths.unsqueeze(1)))

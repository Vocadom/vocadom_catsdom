import random
import warnings

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

from .attention import Attention
from .pointer_generator import PointerGenerator



device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class DecoderRNN(nn.Module):



	def __init__(self, vocab_size, embedding_dim, hidden_size, pad_id,
				 sos_id, eos_id, unk_id, max_length, n_layers=1, bidirectional=False,
				 dropout=0, rnn_type='GRU', use_attention=True, embedding=None,
				 update_embedding=True, pointer_gen=False, coverage=False,
				 attn_type='bahdanau'):

		super(DecoderRNN, self).__init__()

		self.vocab_size = vocab_size
		self.embedding_dim = embedding_dim
		self.hidden_size = hidden_size
		self.n_layers = n_layers
		self.bidirectional = bidirectional
		self.dropout = dropout if self.n_layers > 1 else 0
		self.use_attention = use_attention
		self.pad_id = pad_id
		self.sos_id = sos_id
		self.eos_id = eos_id
		self.unk_id = unk_id
		self.max_length = max_length
		self.rnn_type = rnn_type
		self.coverage = coverage
		self.pointer_gen = pointer_gen



		self.embedding = nn.Embedding(self.vocab_size, self.embedding_dim, padding_idx=self.pad_id)
		if embedding is not None:
			self.embedding.weight = nn.Parameter(embedding)
		self.embedding.weight.requires_grad = update_embedding

		self.rnn = getattr(nn, self.rnn_type)(embedding_dim, self.hidden_size, n_layers, batch_first=True, dropout=self.dropout)
		self.input_dropout = nn.Dropout(p=self.dropout)

		if use_attention:
			self.attention = Attention(self.hidden_size, coverage=coverage, attn_type=attn_type)

		if pointer_gen:
			self.pointer_generator = PointerGenerator(self.hidden_size, self.embedding_dim, self.vocab_size)


		self.out = nn.Linear(self.hidden_size, self.vocab_size)




	def forward_step(self, trg_seq, hidden, encoder_outputs, src_lengths, coverage=None, pg_src_batches=None, max_oovs=0):

		batch_size = trg_seq.size(0)
		trg_size = trg_seq.size(1)

		embedded = self.embedding(trg_seq)
		embedded = self.input_dropout(embedded)

		# Run the forward pass of the RNN.
		outputs, hidden = self.rnn(embedded, hidden)

		# Calculate the attention.
		if self.use_attention:
			outputs, attn_weights, context_v = self.attention(
				outputs.contiguous(),
				encoder_outputs,
				src_lengths=src_lengths,
				coverage = coverage
			)


		outputs = self.out(outputs.contiguous().view(-1, self.hidden_size))

		p_gen = None
		
		if self.pointer_gen:
	
			outputs = F.softmax(outputs, dim=1)

			if isinstance(hidden, tuple):
				hidden_ = hidden[0] #LSTM
			else:
				hidden_ = hidden #GRU

			outputs, p_gen = self.pointer_generator(context_v, hidden_, embedded, outputs, attn_weights, pg_src_batches, max_oovs)

			outputs = torch.log(outputs).view(batch_size, trg_size, -1)
		
		else:
			outputs = F.log_softmax(outputs, dim=1).view(batch_size, trg_size, -1)

		return outputs, hidden, attn_weights, coverage, p_gen





	def forward(self, trg_seqs=None, encoder_hidden=None, encoder_outputs=None, src_lengths=None,
				teacher_forcing_ratio=0, coverage=None, pg_src_batches=None, max_oovs=0, sampling=False):

		src_size = encoder_outputs.size(1)
		batch_size  = encoder_outputs.size(0)

		if trg_seqs is None:
			if teacher_forcing_ratio > 0:
				teacher_forcing_ratio = 0
				warnings.warn("Teacher forcing ratio was set back to 0 since no target sequence was provided.")

			trg_seqs = torch.LongTensor([self.sos_id] * batch_size).view(batch_size, 1).to(device)
			max_length = self.max_length
		else:
			max_length = trg_seqs.size(1)


		hidden = self._init_state(encoder_hidden)
		use_teacher_forcing = True if random.random() < teacher_forcing_ratio else False


		outputs = torch.zeros(batch_size, max_length, self.vocab_size + max_oovs).to(device)
		attn_weights = torch.zeros(batch_size, max_length, src_size).to(device)
		coverages = torch.zeros(batch_size, max_length, src_size).to(device)
		p_gens = torch.zeros(batch_size, max_length).to(device)


		if use_teacher_forcing and not (self.coverage or self.pointer_gen):
			
			decoder_input = trg_seqs
			outputs, hidden, attn_weights, coverages, p_gens = self.forward_step(decoder_input, hidden, encoder_outputs, src_lengths, coverage)

		elif use_teacher_forcing:

			for di in range(max_length):
				decoder_input = trg_seqs[:, di].unsqueeze(1) # we feed the groud truth tokens as the decoder input
				step_outputs, hidden, step_attn, coverage, p_gen = self.forward_step(decoder_input, hidden, encoder_outputs, src_lengths, coverage, pg_src_batches, max_oovs)
				outputs[:, di] = step_outputs.squeeze(1)
				attn_weights[:, di] = step_attn.squeeze(1)

				if coverage is not None:
					coverages[:, di] = coverage.squeeze(1)

					# Update the coverage attention.
					coverage = coverage + step_attn.squeeze(1)
				
				if p_gen is not None:
					p_gens[:, di] = p_gen.squeeze(1)

		else:

			decoder_input = trg_seqs[:, 0].unsqueeze(1) # we start by feeding the BOS token

			for di in range(max_length):
				step_outputs, hidden, step_attn, coverage, p_gen = self.forward_step(decoder_input, hidden, encoder_outputs, src_lengths, coverage, pg_src_batches, max_oovs)
				

				if not sampling:
					decoder_input = step_outputs.topk(1)[1].squeeze(1).detach()
				else:
					decoder_input = torch.multinomial(step_outputs, 1).squeeze(1).detach()


				decoder_input[decoder_input >= self.vocab_size] = self.unk_id

				outputs[:, di] = step_outputs.squeeze(1)
				attn_weights[:, di] = step_attn.squeeze(1)

				if coverage is not None:
					coverages[:, di] = coverage.squeeze(1)

					# Update the coverage attention.
					coverage = coverage + step_attn.squeeze(1)
				
				if p_gen is not None:
					p_gens[:, di] = p_gen.squeeze(1)


		return outputs, hidden, attn_weights, coverages, p_gens




	def _init_state(self, encoder_hidden):
		""" Initialize the decoder hidden state. """

		def _cat_directions(h):
			""" If the encoder is bidirectional, do the following transformation.
				(#directions * #layers, #batch, hidden_size) -> (#layers, #batch, #directions * hidden_size)
			"""
			if self.bidirectional:
				h = torch.cat([h[0:h.size(0):2], h[1:h.size(0):2]], 2)
			return h


		if encoder_hidden is None:
			return None
		if isinstance(encoder_hidden, tuple):
			encoder_hidden = tuple([_cat_directions(h) for h in encoder_hidden])
		else:
			encoder_hidden = _cat_directions(encoder_hidden)
		return encoder_hidden

import os
from delexicalize import delex, relex
import pickle

input_file = sys.argv[1]
outout_file = sys.argv[2]
delex_dic_file = sys.argv[3]


def lexicalize(input_file, outout_file, delex_dic_file):

	with open(delex_dic_file, 'r') as fd:
		delex_dic = pickle.load(fd)

	with open(input_file) as fs, open(outout_file, 'w') as fd:
		for line_i, line in enumerate(fs):
			line = line.strip()
			decoded_sentence = relex(delex_dic[line_i], line) 
			fd.write(decoded_sentence+'\n')

lexicalize(input_file, outout_file, delex_dic_file)

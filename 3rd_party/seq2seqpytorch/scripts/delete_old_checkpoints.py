import os
import glob
import time
import sys


while True:

	checkpoints = glob.glob('../checkpoints/stacked_seq2seq*')
	checkpoints_sorted = sorted(checkpoints, key=os.path.getctime, reverse=True)
	print('List of checkpoints', checkpoints_sorted)
	
	if len(checkpoints_sorted) > 5:
		for checkpoint in checkpoints_sorted[5:]:
			os.remove(checkpoint)
			print('removed: ', checkpoint)


	time.sleep(60*2)

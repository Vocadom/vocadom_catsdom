from __future__ import unicode_literals, print_function, division
import os
import logging
import time
import math

import matplotlib.pyplot as plt
plt.switch_backend('agg')
import matplotlib.ticker as ticker

import torch
import torch.nn as nn
from torch import optim
import torchtext


from utils import attention_viz, decode_sentence
from utils.data_util import tokenizer, detokenizer
from utils import  checkpoint_util
from evaluator import Evaluator

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
logger = logging.getLogger(__name__)


class Trainer:

	def __init__(self, opts, model, train_dataset, val_dataset, experiment_dir, checkpoint_dir):
		
		self.opts = opts
		self.model = model

		self.optimizer = optim.Adam(model.parameters(), lr=opts.lr, weight_decay=opts.weight_decay)
		self.lr_scheduler = optim.lr_scheduler.ReduceLROnPlateau(self.optimizer, mode='min', factor=opts.lr_scheduler_factor, patience=opts.lr_scheduler_patience, verbose=False)
		self.criterion = nn.NLLLoss(ignore_index=opts.PAD_id)

		self.train_dataset = train_dataset
		self.val_dataset = val_dataset

		self.experiment_dir = experiment_dir
		self.checkpoint_dir = checkpoint_dir
		self.evaluator = Evaluator(self.opts, self.criterion, self.val_dataset)



	def train_batch(self, src_batches, src_lengths, trg1_batches, trg2_batches, pg_src_batches=None, pg_trg_batches=None, max_oovs=0):

		if self.opts.pointer_gen:
			trg2_batches = pg_trg_batches

		
		decoder_outputs, decoder_hidden, attn_weights, coverages, p_gens = \
			self.model(src_batches, src_lengths, trg1_batches, pg_src_batches, max_oovs, self.opts.teacher_forcing_ratio)
	

		loss = self.criterion(decoder_outputs.contiguous().view(-1, decoder_outputs.size(2)), trg2_batches.contiguous().view(-1))

		if self.opts.coverage:
			coverage_loss_wt = 1.0 #TODO: put this in opts
			coverage_loss = coverage_loss_wt * torch.mean(torch.mean(torch.sum(torch.min(attn_weights, coverages),2),1))
			loss += coverage_loss


		# Backward propagation
		self.model.zero_grad()
		loss.backward()
		clip = nn.utils.clip_grad_norm_(self.model.parameters(), self.opts.clip_grad)
		self.optimizer.step()

		loss = loss.data.item()


		return loss, clip



	def train(self, mode):


		if mode == 'resume':
			if self.checkpoint_dir:
				# Load checkpoint
				checkpoint = checkpoint_util.load_checkpoint(checkpoint_dir=self.checkpoint_dir)

				self.model.load_state_dict(checkpoint['model_state_dict'], strict=False)
				if not self.opts.coverage: self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
				self.lr_scheduler.load_state_dict(checkpoint['lr_scheduler_state_dict'])
				global_step = checkpoint['global_step']
				train_loss_history = checkpoint['train_loss_history']
				val_loss_history = checkpoint['val_loss_history']
				start_time = checkpoint['start_time']
				clip_history = checkpoint['clip_history']

		else:
			global_step = 1
			train_loss_history = []
			val_loss_history = []
			start_time = time.time()
			clip_history = []


		batch_iterator = torchtext.data.BucketIterator(dataset=self.train_dataset.dataset, batch_size=self.opts.batch_size, sort=False, shuffle=True, 
														sort_within_batch=True,  sort_key=lambda x: len(x.src), device=device, repeat=True)
		batch_generator = batch_iterator.__iter__()

		total_batches = len(batch_iterator)

		for iter in range(global_step, self.opts.total_nb_iter + 1):

			batch = next(batch_generator)
			epoch = (iter//total_batches)+1 # batch_iterator.epoch+1 
			epoch_iter = batch_iterator._iterations_this_epoch

			src_batches, src_lengths = batch.src
			trg1_batches = batch.trg1
			trg2_batches = batch.trg2
			src_raw_batches = batch.src_raw
			trg_raw_batches = batch.trg_raw

			if self.opts.pointer_gen:
				pg_src_batches, pg_trg_batches, max_oovs, oovs = self.train_dataset.pg_batch(src_batches, src_raw_batches, trg2_batches, trg_raw_batches)
			else:
				pg_src_batches, pg_trg_batches, max_oovs = None, None, 0


			loss, clip = self.train_batch(src_batches,
							 	     	  src_lengths,
							 	    	  trg1_batches,
							 	    	  trg2_batches,
							 	    	  pg_src_batches, 
							 	     	  pg_trg_batches, 
							 	   		  max_oovs)

			train_loss_history.append(loss)
			clip_history.append(clip)




			print('Epoch: {}    iteration: {}    epoch iteration: {}/{},    batch loss: {}'.format(epoch, iter, epoch_iter, total_batches, loss), end="\r")

			# print loss
			if (iter != global_step) and (iter % self.opts.print_loss_every == 0):
				loss_avg = sum(train_loss_history[-self.opts.print_loss_every:])/len(train_loss_history[-self.opts.print_loss_every:])
				clip_avg = sum(clip_history[-self.opts.print_loss_every:])/len(clip_history[-self.opts.print_loss_every:])
				self.print_loss(iter, self.opts.total_nb_iter, start_time, loss_avg, clip_avg)



			# plot loss
			if (iter != global_step) and (iter % self.opts.plot_loss_every == 0):
				train_points = [sum(train_loss_history[i:i+self.opts.plot_loss_every])/len(train_loss_history[i:i+self.opts.plot_loss_every]) for i in range(0,len(train_loss_history),self.opts.plot_loss_every)]
				val_points = val_loss_history
				file_name = os.path.join(self.experiment_dir, '%s.png' % self.opts.experiment_name)
				self.show_loss_plot(train_points, val_points, file_name)


			# evaluate model on val data
			# if (iter != global_step) and (epoch_iter % total_batches == 0):
			if (iter != global_step) and (iter % self.opts.evaluate_model_every == 0):

				
				# get validation loss
				val_loss = self.evaluator.evaluate(self.model)

				val_loss_msg = 'Epoch: %d    Validation loss: %.4f' % (epoch, val_loss)
				print(val_loss_msg)
				logger.info(val_loss_msg)
				val_loss_history.append(val_loss)

				# apply learning rate scheduler (decay)
				if iter >= self.opts.lr_scheduler_start_at:
					self.lr_scheduler.step(val_loss)
				self.get_learning_rate(self.optimizer)



			# save model and evaluate
			if (iter != global_step) and (iter % self.opts.save_model_every == 0):
				loss_avg = sum(train_loss_history[-self.opts.save_model_every:])/len(train_loss_history[-self.opts.save_model_every:])
				global_step = iter
				checkpoint_util.save_checkpoint(self.checkpoint_dir, 
												self.opts, 
												self.opts.experiment_name, 
												self.model, 
												self.optimizer, 
												self.lr_scheduler, 
												loss_avg, 
												global_step, 
												train_loss_history, 
												val_loss_history,
												start_time, 
												clip_history)




	def get_learning_rate(self, optimizer):
		for param_group in optimizer.param_groups:
			lr_msg = 'Current learning rate is: {}'.format(param_group['lr'])
			print(lr_msg)
			logger.info(lr_msg)




	def print_loss(self, iter, total_nb_iter, start_time, loss_avg, clip_avg):
		print_summary = 'ET: %s    Progress: (%d %d%%)    Loss: %.4f    Grad norm: %.4f' % (self.time_since(start_time, iter/total_nb_iter), iter, (iter/total_nb_iter)*100, loss_avg, clip_avg)
		print(print_summary)
		logger.info(print_summary)




	def show_loss_plot(self, train_loss, val_loss, file_name):
		
		plt.figure()
		fig, ax = plt.subplots()
		loc = ticker.MultipleLocator(base=0.2)
		ax.yaxis.set_major_locator(loc)
		
		plt.plot(train_loss, label='Train loss')
		plt.plot(val_loss, label='Validation loss')
		plt.legend(loc='upper right')

		fig.savefig(file_name, dpi=fig.dpi)

		plt.close('all')




	def time_since(self, since, percent):

		def as_minutes(s):
			m = math.floor(s / 60)
			s -= m * 60
			return '%dm %ds' % (m, s)

		now = time.time()
		s = now - since
		es = s / (percent)
		rs = es - s
		return '%s (- %s)' % (as_minutes(s), as_minutes(rs))


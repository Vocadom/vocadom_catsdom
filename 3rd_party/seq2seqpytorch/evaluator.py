from __future__ import unicode_literals, print_function, division
import logging
import os

import torch
import torch.nn as nn
import torchtext

from utils import attention_viz, decode_sentence
from utils.data_util import tokenizer, detokenizer

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
logger = logging.getLogger(__name__)


class Evaluator:

	def __init__(self, opts, criterion, val_dataset):
		
		self.opts = opts
		self.criterion = criterion
		self.val_dataset = val_dataset


	def evaluate_batch(self, model, src_batches, src_lengths, trg1_batches, trg2_batches, pg_src_batches=None, pg_trg_batches=None, max_oovs=0):

		with torch.no_grad():

			if self.opts.pointer_gen:
				trg2_batches = pg_trg_batches



			decoder_outputs, decoder_hidden, attn_weights, coverages, p_gens = \
				model(src_batches, src_lengths, trg1_batches, pg_src_batches, max_oovs, teacher_forcing_ratio=self.opts.teacher_forcing_ratio)
			
			loss = self.criterion(decoder_outputs.contiguous().view(-1, decoder_outputs.size(2)), trg2_batches.contiguous().view(-1))
			
			if self.opts.coverage:
				coverage_loss_wt = 1.0
				coverage_loss = coverage_loss_wt * torch.mean(torch.mean(torch.sum(torch.min(attn_weights, coverages),2),1))
				loss += coverage_loss

			loss = loss.data.item()


		return decoder_outputs, attn_weights, loss




	def evaluate(self, model):
		
		total_loss = 0
		loss_norm_term = 0


		batch_iterator = torchtext.data.BucketIterator(dataset=self.val_dataset.dataset, batch_size=self.opts.batch_size, sort=True, 
													   sort_key=lambda x: len(x.src), device=device, train=False)

		for batch in batch_iterator:

			src_batches, src_lengths = batch.src
			trg1_batches = batch.trg1
			trg2_batches = batch.trg2
			src_raw_batches = batch.src_raw
			trg_raw_batches = batch.trg_raw

			if self.opts.pointer_gen:
				pg_src_batches, pg_trg_batches, max_oovs, oovs = self.val_dataset.pg_batch(src_batches, src_raw_batches, trg2_batches, trg_raw_batches)
			else:
				pg_src_batches, pg_trg_batches, max_oovs, oovs = None, None, 0, None



			decoder_outputs, attn_weights, loss = self.evaluate_batch(model, src_batches, src_lengths, trg1_batches, trg2_batches, pg_src_batches, pg_trg_batches, max_oovs)

			total_loss += loss
			loss_norm_term +=1

		total_loss /= loss_norm_term

		return total_loss

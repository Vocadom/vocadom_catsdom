
import torch
import numpy as np
from .data_util import tokenizer, detokenizer


def decode(decoder_outputs, attn_weights, source_sentences, target_lang, UNK_rep, oovs, beam_size=1):

	UNK_id = target_lang.UNK_id
	EOS_id = target_lang.EOS_id
	PAD_id = target_lang.PAD_id

	def _decode_token(token_i, attention, source_sentence, oov=None):

		if token_i == PAD_id:
			return ''

		if token_i == UNK_id and UNK_rep:
			score, idx = attention.max(0)
			if idx.item() < len(source_sentence):
				token = source_sentence[idx.item()]
			else:
				token = target_lang.index2word[token_i]
		else:
			try:
				token = target_lang.index2word[token_i]
			except:
				article_oov_idx = token_i - target_lang.n_words
				token = oov[article_oov_idx]

		return token



	'''
	decoder_outputs = decoder_outputs.topk(1)[1]
	decoded_words = [[_decode_token(decoder_outputs[si][di].item(), attn_weights[si][di], source_sentences[si],  oovs[si] if oovs else None) for di, dec in enumerate(sequence)] for si, sequence in enumerate(decoder_outputs)]
	'''

	lengths = np.array([decoder_outputs.size(1)] * decoder_outputs.size(0))
	decoded_symbols = []

	for di in range(decoder_outputs.size(1)):

		if beam_size > 1:
			symbols = decoder_outputs[:, di]
		else:
			step_output = decoder_outputs[:, di, :]
			symbols = step_output.topk(1)[1]

		decoded_symbols.append(symbols)


		eos_batches = symbols.data.eq(EOS_id)
		if eos_batches.dim() > 0:
			eos_batches = eos_batches.cpu().view(-1).numpy()
			update_idx = ((lengths > di) & eos_batches) != 0
			lengths[update_idx] = len(decoded_symbols)

	decoded_words = [[_decode_token(decoded_symbols[di][bi].item(), attn_weights[bi][di], tokenizer(source_sentences[bi]), oovs[bi] if oovs else None) for di in range(length)] for bi, length in enumerate(lengths)]


	return decoded_words


'''
def get_sentence(seq):
	if EOS_id in seq: return seq[:seq.index(EOS_id)+1]
	else: return seq
'''		
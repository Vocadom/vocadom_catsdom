import json
from .attribute_dic import AttrDict

class Config:

	def __init__(self, file_name):
		
		self.file_name = file_name
		self.opts = AttrDict()

	def load(self):

		with open(self.file_name) as json_data_file:
			config = json.load(json_data_file)

		self.opts.update(config)

		return self.opts

	def summary(self):

		output_string = ''
		output_string += '='*100 + '\n'
		output_string += 'Hyperparameters:' + '\n'
		for k,v in self.opts.items():
			output_string += '- {}: {}'.format(k, v) + '\n'
		output_string += '='*100 + '\n'

		return output_string

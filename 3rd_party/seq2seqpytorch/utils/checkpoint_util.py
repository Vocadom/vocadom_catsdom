import os
import glob

import torch



def load_checkpoint(checkpoint_dir, checkpoint_file=None, verbose=True):

	if checkpoint_file is None:
		#Load the latest checkpoint (also best wrt to train loss)
		list_of_checkpoints = glob.glob('{}/*'.format(checkpoint_dir))
		checkpoint_file = max(list_of_checkpoints, key=os.path.getctime)
	else:
		checkpoint_file = os.path.join(checkpoint_dir, checkpoint_file)

	checkpoint = torch.load(checkpoint_file, map_location=lambda storage, loc: storage)

	if verbose:
		print('='*100)
		print('Checkpoint {} was loaded successfully...'.format(checkpoint_file))
		print('='*100)

	return checkpoint


def save_checkpoint(checkpoint_dir, opts, experiment_name, model, optimizer, lr_scheduler, loss_avg, global_step, train_loss_history, val_loss_history=None, start_time=None, clip_history=None, verbose=True):
	checkpoint = {
		'opts': opts,
		'global_step': global_step,
		'train_loss_history': train_loss_history,
		'val_loss_history': val_loss_history,
		'model_state_dict': model.state_dict(),
		'optimizer_state_dict': optimizer.state_dict(),
		'lr_scheduler_state_dict': lr_scheduler.state_dict(),
		'start_time': start_time,
		'clip_history': clip_history,
	}

	checkpoint_file = '%s/%s_loss_%.2f_step_%d.pt' % (checkpoint_dir, experiment_name, loss_avg, global_step)

	torch.save(checkpoint, checkpoint_file)
	
	if verbose:
		print('='*100)
		print('Checkpoint {}, was saved successfully...'.format(checkpoint_file))
		print('='*100)

	return checkpoint_file

import os
import numpy as np

import matplotlib.pyplot as plt
plt.switch_backend('agg')
import matplotlib.ticker as ticker

def show_attention(input_sentence, output_sentence, attentions, file_name):

	attentions = attentions.data.cpu().numpy()


	#dumpt attention to txt file
	np.savetxt(file_name+'_dump', attentions, delimiter='\t')




	#dumpt attention to image file
	width = 40
	height = 40

	font_size_x = 16
	font_size_y = 16

	# Set up figure with colorbar
	fig = plt.figure(figsize=(width, height))
	ax = fig.add_subplot(111)
	cax = ax.matshow(attentions, cmap='bone')
	cb = fig.colorbar(cax)

	# Set up axes
	ax.set_xticklabels([''] + input_sentence + [''], rotation=90)
	ax.set_yticklabels([''] + output_sentence)

	# Show label at every tick
	ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
	ax.yaxis.set_major_locator(ticker.MultipleLocator(1))
	
	for item in ([ax.title, ax.yaxis.label] + ax.get_yticklabels()):
		item.set_fontsize(font_size_y)

	for item in ([ax.xaxis.label] + ax.get_xticklabels()):
		item.set_fontsize(font_size_x)


	cb.remove()
	plt.close()
	fig.savefig(file_name, dpi=fig.dpi, bbox_inches='tight')



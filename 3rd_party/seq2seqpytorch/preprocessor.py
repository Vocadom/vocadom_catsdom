import os
from utils import data_util
import argparse



# http://statmt.org/wmt14/translation-task.html#download

parser = argparse.ArgumentParser(description='Preprocessor')
parser.add_argument('--train_file', help='train file name for saving', required=True)
parser.add_argument('--val_file', help='val file name for saving', required=True)
parser.add_argument('--src_train', help='train source file', required=True)
parser.add_argument('--trg_train', help='train target file', required=True)
parser.add_argument('--src_val', help='validation source file', required=False)
parser.add_argument('--trg_val', help='validation target file', required=False)
parser.add_argument('--save_path', help='location to store files', required=True)
parser.add_argument('--src_max_length', type=int, help='source max length of sequences', required=True)
parser.add_argument('--trg_max_length', type=int, help='target max length of sequences', required=True)

args = parser.parse_args()

train_file = args.train_file
val_file = args.val_file
src_train = args.src_train
trg_train = args.trg_train
src_val = args.src_val
trg_val = args.trg_val
src_max_length = args.src_max_length
trg_max_length = args.trg_max_length

train_save = os.path.join(args.save_path, train_file)
val_save = os.path.join(args.save_path, val_file)

data_util.prepare_data(src_train, trg_train, src_val, trg_val, train_save, val_save, src_max_length, trg_max_length)




'''
python preprocessor.py --train_file train_wmt --val_file val_wmt --src_train ../stacked_seq2seq_data/wmt14/WMT14.fr-en.fr --trg_train ../stacked_seq2seq_data/wmt14/WMT14.fr-en.en --src_val ../stacked_seq2seq_data/wmt14/ntst1213.fr-en.fr --trg_val ../stacked_seq2seq_data/wmt14/ntst14.fr-en.en --save_path data/ --max_length 50
python preprocessor.py --train_file train_torchtext --val_file val_torchtext --src_train ../stacked_seq2seq_data/data/train_french --trg_train ../stacked_seq2seq_data/data/train_english --src_val ../stacked_seq2seq_data/data/val_french --trg_val ../stacked_seq2seq_data/data/val_english --save_path data/ --max_length 50
'''
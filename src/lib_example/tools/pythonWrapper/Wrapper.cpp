/*
 * @Author: Lucas Labadens
 */

#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <regex>
#include <list>
#include <thread>         // std::thread
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "Wrapper.hpp"

#define PYRUN "python "

//true if a python script is running
bool pythonRunning;

/**
 * @brief printOutputPyModule Function to print the output of the py module wrapped
 * @param fifo Fifo to communicate with the python module
 */
void printOutputPyModule(char *fifo)
{
    std::ifstream file{fifo};
    std::string line;
    while(pythonRunning)
    {
	std::getline(file, line);
	if(line != "")
	{
	    std::cout << line << std::endl;;
	}
    }
    file.close();
}

/**
 * @brief runPyModule Function used to run python Module
 * @param pyModule pyModule name of python module
 * @param args args list of argument to run python module with
 * @return Return string of printed result by python module
 */
std::string runPyModule(const char *pyModule, std::list<std::string> args)
{
    std::string pyArgs = "";
    if(args.size() > 0)
    {
	pyArgs = processArgs(args);
    }
  
    char buffer[128];
    char pyCmd[strlen(pyModule) + strlen(PYRUN) + strlen(pyArgs.data())+ 1 ];

    std::string out = ""; // use to store result of python module

    //Create python command
    memcpy(pyCmd,PYRUN,strlen(PYRUN)+1);
    strcpy(pyCmd,PYRUN);
    strcat(pyCmd,pyModule);
    strcat(pyCmd,pyArgs.data());

    //Create Fifo to print python output
    char fifoName[1500];
    strcpy(fifoName, pyModule);
    strcat(fifoName,".out");

    mkfifo(fifoName, 0777);

    pythonRunning = true;
    //Run thread that print python output through Fifo
    std::thread  printOut(printOutputPyModule,fifoName);

    //Run python module
    FILE *cmd = popen(pyCmd,"r");

    // Read result of module when it finish
    if(cmd)
    {
	while(fgets(buffer,sizeof buffer,cmd) != nullptr)
	{
	    out += buffer;
	}
	pclose(cmd);
    }
    pythonRunning = false;

    printOut.join();
    return out;
}


/**
 * @brief processArgs Function used concate arguments in one string
 * @param args args list of argument
 * @return string of concate arguments
 */
std::string processArgs(std::list<std::string> args)
{
    std::string result = "";
    for(std::string s : args)
    {
	result += " "+s;
    }
    return result;
}


import inspect

global fileOut

def  init_log():
    global fileOut

    #use to communicate with c++ wrapper
    fifoName = inspect.stack()[1][1] + ".out"
    print("fifoName",fifoName)
    fileOut = open( fifoName, 'w' )
    return fileOut

def close_log():
    fileOut.close()

def Print(*args):
    print(*args,file=fileOut)
    fileOut.flush()


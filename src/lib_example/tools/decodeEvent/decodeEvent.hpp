#ifndef DECODEEVENT_HPP
#define DECODEEVENT_HPP

#include <pthread.h>                // Using for mutex
#include <cstdint>                  // Using for uint8_t uint16_t
#include <string>

#define MAX_PHN 15

/**
 * @brief The EvtType enum Type of Event End of file or Keyword Detected
 */
enum EvtType
{
    Ef,
    Ep,
    None
};

/**
 * @brief The Event struct Event base struct
 */
struct Event
{
    virtual ~Event() = default;
    Event()
    {
	type  = None;
	micro = 0;
	deb   = 0.;
	fin   = 0. ;
    };
    EvtType type; //Ep ou Ef
    int  micro;
    float deb;
    float fin;
};

/**
 * @brief The EventEp struct Keyword detected event
 */
struct EventEp : public Event
{
    EventEp(){type = Ep;};
    int nbPhon;
    double phonTime[MAX_PHN];
    char   phonVal[MAX_PHN];
};

/**
 * @brief The EventEf struct End of Sound detected event
 */
struct EventEf : public Event
{
    EventEf(){type = Ef;};
    double endOfVoice;
};

/* ====================
	Prototypes
   ==================== */
Event* decodeEvent(char *rcv,int nbBytes);
std::string EventToString(Event& e);
#endif

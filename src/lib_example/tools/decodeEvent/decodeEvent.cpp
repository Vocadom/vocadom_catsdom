/*
 * @Author: Clémence VIAL 
 * @Date: 2020-01-08 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2020-01-09 11:08:28 
 */

#include "decodeEvent.hpp"
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <iomanip>
#include <sys/time.h>

/**
 * @brief return type linked to string
 * @param name char representing Event type
 * @return EvtType the event type struct
 **/
EvtType getTypeName(char *name)
{
    if(strcmp(name,"Ef") == 0)
	return Ef;
    else if(strcmp(name,"Ep") == 0)
	return Ep;
    else
	return None;
}

/**
 * @brief decodeEfEvent
 * @param rcv the frames received
 * @return EventEf structure
 */
EventEf* decodeEfEvent(char *rcv)
{
    EventEf *evt = new EventEf;

    return evt;

}
/**
 * @brief decodeEpEvent decode Ep type event
 * @param rcv the frames received
 * @return EventEP structure
 */
EventEp* decodeEpEvent(char *rcv)
{
    EventEp *evt = new EventEp;

    char decode[1000];

    int     count = 0;

    memcpy(decode,rcv,strlen(rcv));
    char   *split = strtok(decode, " ");

    while(split != NULL)
    {
	//std::cout << split << std::endl;
	switch(count){
	    case 0:
		evt->micro = atoi(split);
		break;
	    case 1:
		evt->deb = atof(split);
		break;
	    case 2:
		evt->fin = atof(split);
		break;
	    case 3:
		break;
	    default:
		evt->phonTime[count - 4] = atof(split);
		split = strtok(NULL, " ");
		evt->phonVal[count - 4] = *split;
		break;
	}
	count ++;
	split = strtok(NULL, " ");
    }
    evt->nbPhon = count - 4;

    return evt;
}

/**
 * @brief decodeEvent Translate char frame reprensting event into EventStructure
 * @param rcv frame received
 * @param nbBytes the frames length
 * @return Event struct, Ep or Ef according to the event type
 */
Event* decodeEvent(char *rcv,int nbBytes)
{
    if(nbBytes > 2)
    {
	char typeName[3];
	memcpy(typeName,rcv,2);
	typeName[2] = '\0';

    // get the type of the event and call the correct function.
	EvtType type = getTypeName(typeName);
	if(type == Ep)
	{
	    return decodeEpEvent(rcv + 3);
	}
	else if(type == Ef)
	{
	    return decodeEfEvent(rcv + 2);
	}
	else
	{
	    return new Event();
	}
    }
    else
	return new Event();

}

std::string EventEfToString(EventEf& e)
{
    std::string str = "Ef";
    return str;
}

/**
 * @brief EventEpToString get string from EventEp
 * @param e the EventEp to convert
 * @return string
 */
std::string EventEpToString(EventEp& e)
{
    std::stringstream ss;
    ss << e.micro << " " << std::fixed << std::setprecision(2) << e.deb << " "
       << std::fixed << std::setprecision(2) <<  e.fin;

    for(int i = 0; i < e.nbPhon; i++)
    {

	ss << " " << std::string(1,e.phonVal[i]) << "-" <<std::fixed << std::setprecision(2) << e.phonTime[i] ;
    }

    return ss.str();
}

/**
 * @brief EventToString get string from EventEf
 * @param e the EventEf to convert
 * @return string
 */
std::string EventToString(Event& e)
{
    std::string str = "";

    switch(e.type)
    {
	case Ep:
	    str += EventEpToString(static_cast<EventEp&>(e));
	    break;
	case Ef:
	    str += EventEfToString(static_cast<EventEf&>(e));
	    break;
	case None:
	    str += "None";
	    break;
    }
    return str;
}

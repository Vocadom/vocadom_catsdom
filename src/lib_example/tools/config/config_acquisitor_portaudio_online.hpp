/*
     Copyright (C) 2012-2016 Laboratoire d'Informatique de Grenoble (www.liglab.fr),
     Centre National de la Recherche Scientifique (www.cnrs.fr).
     The initial authors of the original code are Michel Vacher, François Portet 
     and Frédéric Aman.
     Contributors: Michel Vacher, François Portet, Frédéric Aman, Fabien Eloy, 
     William Duclot, Cheick Mahady Sissoko, Dan Istrate, Hubert Glasson, Stéphane 
     Chaillol and Alain Dufaux.
     
     This file is part of CirdoX.

     CirdoX is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     CirdoX is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CirdoX. If not, see <http://www.gnu.org/licenses/>.

     CirdoX is a software project initiated by the French Centre National 
     de la Recherche Scientifique (CNRS) and the Laboratoire d'Informatique de 
     Grenoble  (LIG) - France (https://www.liglab.fr/).
     This work was supported by the French funding agencies ANR and CNSA through 
     the CIRDO project (ANR-2010-TECS-012).
     CirdoX is a modular software able to analyse online the audio environment, 
     to extract the uttered sentences and then to process them thanks to an ASR stage 
     followed by a filtering stage.

     Contact Frédéric Aman <frederic.aman@gmail.com>, Michel Vacher
     <michel.vacher@imag.fr> or François Portet <francois.portet@imag.fr>
     for more information about the licence or the use of CirdoX.
*/

/*******************************************************************************
*	Fichier config_acquisitor_portaudio_online.h 
*
*	Frédéric Aman 	09/2012
*
*	Définition des section et variables du fichier .cfg
*******************************************************************************/

#ifndef CONFIG_ACQUISITOR_PORTAUDIO_ONLINE_HPP
#define CONFIG_ACQUISITOR_PORTAUDIO_ONLINE_HPP

#define CFG_SECTION_ACQ			"ACQ"
#define CFG_BOARD			"$BOARD"
#define CFG_PORT			"$PORT"
#define CFG_CHANNEL			"$CHANNEL"
#define CFG_CHANNEL_MAX_PER_PORT	"$CHANNEL_MAX_PER_PORT"
#define CFG_FRAMES_PER_BUFFER 		"$FRAMES_PER_BUFFER"
#define CFG_SAMPLE_RATE 		"$SAMPLE_RATE"
#define CFG_BIT_DEPTH       "$BIT_DEPTH"


/* types ==================================================================== */

/* structures =============================================================== */
typedef struct {
	char board[1000];
	int port;
	int channel;
	int channel_max_per_port;
	int frames_per_buffer;
	int sample_rate;
     int bit_depth;
}configuration_acquisitor;

/* variables globales ======================================================= */


/* prototypes =============================================================== */
void load_configuration_portaudio_acquisitor(configuration_acquisitor *config, char path[]);

#endif

/* eof ====================================================================== */

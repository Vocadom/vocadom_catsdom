/*
     Copyright (C) 2012-2016 Laboratoire d'Informatique de Grenoble (www.liglab.fr),
     Centre National de la Recherche Scientifique (www.cnrs.fr).
     The initial authors of the original code are Michel Vacher, François Portet 
     and Frédéric Aman.
     Contributors: Michel Vacher, François Portet, Frédéric Aman, Fabien Eloy, 
     William Duclot, Cheick Mahady Sissoko, Dan Istrate, Hubert Glasson, Stéphane 
     Chaillol and Alain Dufaux.
     
     This file is part of CirdoX.

     CirdoX is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     CirdoX is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CirdoX. If not, see <http://www.gnu.org/licenses/>.

     CirdoX is a software project initiated by the French Centre National 
     de la Recherche Scientifique (CNRS) and the Laboratoire d'Informatique de 
     Grenoble  (LIG) - France (https://www.liglab.fr/).
     This work was supported by the French funding agencies ANR and CNSA through 
     the CIRDO project (ANR-2010-TECS-012).
     CirdoX is a modular software able to analyse online the audio environment, 
     to extract the uttered sentences and then to process them thanks to an ASR stage 
     followed by a filtering stage.

     Contact Frédéric Aman <frederic.aman@gmail.com>, Michel Vacher
     <michel.vacher@imag.fr> or François Portet <francois.portet@imag.fr>
     for more information about the licence or the use of CirdoX.
*/

/*******************************************************************************
*
*	Fichier config_acquisitor_portaudio_online.c
* 
* 	Fonctions to load and save the configuration file
*
*	F.Aman	09/2012
*
*******************************************************************************/

/* include ================================================================== */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <glib.h>

#include "config_acquisitor_portaudio_online.hpp"

/*******************************************************************************
*
* FONCTION LOAD CONFIGURATION
*
*******************************************************************************/

void load_configuration_portaudio_acquisitor(configuration_acquisitor *config, char path[])
{
	char Trace[1024] = {'\0'};
	GKeyFile *kfileconfig;
	GError *p_error = NULL;

	// Créer un nouveau GkeyFile
	kfileconfig = g_key_file_new();

	// Charger un fichier existant
	if (!g_key_file_load_from_file(kfileconfig, path, G_KEY_FILE_KEEP_COMMENTS, &p_error))
	{
		// Erreur sortie de programme
		g_warning("Impossible to load config file : %s\n", p_error->message);
		g_error_free(p_error);
		g_key_file_free(kfileconfig);
		exit(EXIT_FAILURE);
	}

	// Lecture données GkeyFile
	strcpy(config->board, g_key_file_get_string(kfileconfig, CFG_SECTION_ACQ, CFG_BOARD, NULL));
	config->port = g_key_file_get_integer(kfileconfig, CFG_SECTION_ACQ, CFG_PORT, NULL);
	config->channel = g_key_file_get_integer(kfileconfig, CFG_SECTION_ACQ, CFG_CHANNEL, NULL);
	config->channel_max_per_port = g_key_file_get_integer(kfileconfig, CFG_SECTION_ACQ, CFG_CHANNEL_MAX_PER_PORT, NULL);
	config->frames_per_buffer = g_key_file_get_integer(kfileconfig, CFG_SECTION_ACQ, CFG_FRAMES_PER_BUFFER, NULL);
	config->sample_rate = g_key_file_get_integer(kfileconfig, CFG_SECTION_ACQ, CFG_SAMPLE_RATE, NULL);
	config->bit_depth = g_key_file_get_integer(kfileconfig, CFG_SECTION_ACQ, CFG_BIT_DEPTH, NULL);

}

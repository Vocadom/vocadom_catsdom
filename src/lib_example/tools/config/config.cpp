/*
 * @Author: Souleymane CONTE 
 * @Date: 2019-07-29 17:04:28 
 * @Last Modified by: Souleymane CONTE 
 * @Last Modified time: 2019-07-29 17:04:28 
 */

#include <iostream>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include "../soundSync/SoundSync.hpp"

int getParamInt(std::string file,std::string clientType, std::string key){
    
    int value = -1;
    boost::property_tree::ptree ptree;
    
    if (!boost::filesystem::exists(file))
    {
        throw std::logic_error("Configuration file must exist");
    }
    read_xml(file, ptree, boost::property_tree::xml_parser::trim_whitespace);
    BOOST_FOREACH (const boost::property_tree::ptree::value_type &v, ptree.get_child("VocadomConfiguration"))
    {
        const boost::property_tree::ptree &attributes = v.second.get_child("<xmlattr>");
        BOOST_FOREACH (const boost::property_tree::ptree::value_type &at, attributes)
        {
            if (at.second.data() == clientType)
            {
                BOOST_FOREACH (const boost::property_tree::ptree::value_type &vs, v.second)
                {
                    if (vs.first == key)
                    {
                        value =  stoi(vs.second.data());
                    }
                }
            }
        }
    }

    return value;
}

int getParamString(std::string file, std::string clientType, std::string key, std::string &value){
    
    boost::property_tree::ptree ptree;
    
    if (!boost::filesystem::exists(file))
    {
        throw std::logic_error("Configuration file must exist");
    }
    read_xml(file, ptree, boost::property_tree::xml_parser::trim_whitespace);
    BOOST_FOREACH (const boost::property_tree::ptree::value_type &v, ptree.get_child("VocadomConfiguration"))
    {
        const boost::property_tree::ptree &attributes = v.second.get_child("<xmlattr>");
        BOOST_FOREACH (const boost::property_tree::ptree::value_type &at, attributes)
        {
            if (at.second.data() == clientType)
            {
                BOOST_FOREACH (const boost::property_tree::ptree::value_type &vs, v.second)
                {
                    if (vs.first == key)
                    {
                        value =  vs.second.data();
                        return 1;
                    }
                }
            }
        }
    }

    return -1;
}

extern "C" int GetConfig(std::string file, std::string clientType, std::string &input, std::string &output)
{
    bool isInputExist = false, isOutputExist = false;
    std::string keyIn = "Input";
    std::string keyOut = "Output";

    if(getParamString(file,clientType, keyIn, input) == 1)
        isInputExist = true;
    if(getParamString(file,clientType,keyOut, output) == 1)
        isOutputExist = true;

    if( isInputExist && isOutputExist)
        return 1;
    else
        return -1;  
}


extern "C" void GetFileList(std::vector<std::string>& fileList,const std::string& path, const std::string& ext)
{
    if (!path.empty())
    {
        boost::filesystem::path apk_path(path);
        boost::filesystem::recursive_directory_iterator end;

        for (boost::filesystem::recursive_directory_iterator i(apk_path); i != end; ++i)
        {
            const boost::filesystem::path cp = (*i);
            if(cp.extension() == ext){
                fileList.push_back(cp.string());
            }
        }
    }
}

extern "C" int GetChunkLen(std::string file,std::string clientType, int &chunklen)
{
    std::string key = "ChunkLen";
    chunklen = getParamInt(file,clientType, key);
    if(chunklen == -1 )
        return -1;
    return 1;
}

extern "C" int GetPort(std::string file,std::string clientType, std::string namePort, int &portServer)
{
    portServer = getParamInt(file,clientType, namePort);
    if(portServer == -1 )
        return -1;
    return 1;
}

extern "C" int GetPorts(std::string file,std::string clientType, std::string namePort, int* portServer, int number_kaldi)
{
    for(int i = 0; i < number_kaldi; i++ )
    {
        portServer[i] = getParamInt(file,clientType, namePort + std::to_string(i));
        if(portServer[i] == -1 )
            return -1;
    }
    return 1;
}

extern "C" int GetAdress(std::string file, std::string clientType, std::string key, std::string &value)
{
    return getParamString(file, clientType, key, value);
}

extern "C" int GetScriptKaldi(std::string file,std::string clientType, std::string &script)
{
    std::string key = "ScriptKaldi";
    return getParamString(file, clientType, key, script);
}

extern "C" int GetSubscribers(std::string file, std::string clientType, std::vector<std::string> &subscribers, int nbr_subscribers)
{
    std::string key = "Subscriber";
    std::string subscriber;
    for(int i = 0; i < nbr_subscribers; i++ )
    {
        if(getParamString(file,clientType, key + std::to_string(i), subscriber) == 1 )
            subscribers.push_back(subscriber);
        else 
            return -1;
    }
    return 1;
}
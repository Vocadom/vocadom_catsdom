/*
 * @Author: Souleymane CONTE 
 * @Date: 2019-07-29 17:04:28 
 * @Last Modified by: Souleymane CONTE 
 * @Last Modified time: 2019-07-29 17:04:28 
 */

#ifndef CONFIG_HPP
#define CONFIG_HPP


#include "../soundSync/SoundSync.hpp"
#include <vector>

extern "C" int GetConfig(std::string file, std::string clientType, std::string &input, std::string &output);
extern "C" void GetFileList(std::vector<std::string>& fileList, const std::string& path, const std::string& ext);
extern "C" int GetChunkLen(std::string file, std::string clientType, int &chunklen);
extern "C" int GetMicroNumber(std::string file,std::string clientType, int &microNum);
extern "C" int GetPort(std::string file,std::string clientType, std::string namePort, int &portServer);
extern "C" int GetPorts(std::string file,std::string clientType, std::string namePort, int *portServer, int number_kaldi);
extern "C" int GetAdress(std::string file, std::string clientType, std::string key, std::string &value);
extern "C" int GetSoundInfo(std::string file, std::string clientType, SoundInfo &info);
extern "C" int GetScriptKaldi(std::string file,std::string clientType, std::string &script);
extern "C" int GetSubscribers(std::string file, std::string clientType, std::vector<std::string> &subscribers, int nbr_subscribers);
#endif //CONFIG_HPP
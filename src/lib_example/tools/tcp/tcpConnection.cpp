
/*
 * @Author: Clémence VIAL 
 * @Date: 2019-11-23 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2019-11-25 11:08:28 
 */

#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <sys/wait.h>                                   // using for signal(SIGPIPE, SIG_IGN);

#define SA struct sockaddr                                  // server's struct contains its informations



/**
 * @brief connect_tcp_client Function used to create a tcp server
 * @param port_server
 * @param adresse_server
 * @return Return socket client tcp
 */
int connect_tcp_client(int port_server, std::string adresse_server )
{
 
    int ret, sock;                                                        
    struct sockaddr_in server;                                      // informations about the server

    signal(SIGPIPE, SIG_IGN);                           // Disabling SIGPIPE whitch makes the the client crash
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);  // Socket client TCP creation

    if(0 > sock)
    {
        std::cout << "Failed to create socket client.. "  << strerror(errno) << std::endl;
        close(sock);
        return -1;
    }
                                                                    // Connection settings to the server
    server.sin_addr.s_addr = inet_addr(adresse_server.c_str());                // server adresse
    server.sin_family = AF_INET;                                    // TCP/IP
    server.sin_port = htons(port_server);                       // server port

    ret = connect(sock, (struct sockaddr*)&server, sizeof(server) );  // Connection to the serveur
    if (0 > ret)
    {
        std::cout << "Failed to connect to server on port " << port_server <<  "... error : "<< strerror(errno) << std::endl;
        close(sock);
        return -1;
    }

    return sock;
}

/**
 * @brief create_tcp_server Function used to create a tcp server
 * @param port_server
 * @param adresse_server Return socket server tcp
 * @return
 */
int create_tcp_server(int port_server, std::string adresse_server )
{
 
    struct sockaddr_in servaddr;                        // Informations about server
    int enable = 1;                                     // enable SO_REUSEADDR
    signal(SIGPIPE, SIG_IGN);                           // Disabling SIGPIPE whitch makes the server crash
    int sock_server_fd;

    sock_server_fd = socket(AF_INET, SOCK_STREAM, 0);     // socket create and verification 

    if (sock_server_fd == -1) 
    { 
        std::cout << "socket creation failed...\n" << std::endl; 
        return -1; 
    } 
         
    servaddr.sin_family = AF_INET;                      // TCP/IP
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);       // server adresse
    servaddr.sin_port = htons(port_server);              // server port 

    
    if (setsockopt(sock_server_fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
    {
        std::cout << "setsockopt(SO_REUSEADDR) failed" << std::endl; 
        return -1;
    }

    // Binding newly created socket to given IP and verification 
    if ((bind(sock_server_fd, (SA*)&servaddr, sizeof(servaddr))) != 0) 
    { 
        std::cout << "socket bind failed..." << std::endl; 
        return -1;
    } 
        
    if ((listen(sock_server_fd, 5)) != 0)                 // Now server is ready to listen and verification 
    { 
        std::cout << "Listen failed..." << std::endl; 
        return -1;
    } 

    return sock_server_fd;
}
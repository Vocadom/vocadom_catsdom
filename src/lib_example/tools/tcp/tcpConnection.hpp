/*
 * @Author: Clémence VIAL 
 * @Date: 2019-11-23 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2019-11-25 11:08:28 
 */

#ifndef TCPCONNECTION_HPP
#define TCPCONNECTION_HPP

#include <iostream>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#define SA struct sockaddr                                  // server's struct contains its informations


/* ====================
	Prototypes
   ==================== */ 
int connect_tcp_client(int port_server, std::string adresse_server );
int create_tcp_server(int port_server, std::string adresse_server );

#endif //TCPCONNECTION_HPP
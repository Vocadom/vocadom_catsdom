#ifndef SOUNDSYNC_HPP
#define SOUNDSYNC_HPP

#include <pthread.h>                // Using for mutex
#include <cstdint>                  // Using for uint8_t uint16_t

/* ====================
	Constants
   ==================== */
#define MIN_SIZE_HEADER_WAV               44
#define MAX_SIZE_HEADER_WAV		  80		// first sample in WAV file
#define WAV_FMT_POS                       20
#define WAV_FMT_EXT                   0xfffe           // Extended format in little endian 
#define WAV_FORMAT_MIN_SIZE               24
#define WAV_ID_SIZE                       12

#define NBR_BYTES_PER_SEND		2048	// TCP send buffer size
#define NBR_BYTES_PER_RECV		320	// TCP recev buffer size (unused)
#define SIZE_SOUND_STAMP		  8		  // stamp struct size in bytes
#define	ONE_BYTES			        1     // Using if sample is one byte
#define TWO_BYTES			        2     // Using if sample is two bytes
#define FOUR_BYTES			      4     // Using if sample is four bytes

/* ====================
	Typedefs
   ==================== */  
typedef struct {						        // Sound sampling information
    uint16_t numChannels;
    uint16_t sampleRate;
    uint16_t bitDepth;
} SoundInfo, *pSoundInfo;

typedef struct {
	uint8_t marker[4] = {0};    // shouls alway take saturation value 
	unsigned int value;						            // current tick value
} stamp_t, *pstamp_t;

/* ====================
	Extern Globals
   ==================== */  
   extern pthread_mutex_t lockFdOut;	// client array protection
   
/* ====================
	Prototypes
   ==================== */ 
int16_t twoBytesToInt16 (uint8_t* , int );
int32_t fourBytesToInt32 (uint8_t* , int );
int init_SoundSync(int, SoundInfo* );  // connect to WAV files
int start_SoundSync(const SoundInfo* , int, const int*, const int* );  // add stamp & dispatch - 1 in, multi out
int start_SoundDeSync(const SoundInfo*, int, int, unsigned int& ); // remove stamp & store 1rst stamp value - 1 in, 1 out

#endif 

from struct import pack,unpack

def getUnstampData(data, stampChar, nbChannel):
    previousPosition = 0
    unstampData = ()
    t = '<' +str(len(data)//2)+ 'h'

    #get a tuples of little endian short instead of bytes
    dataTuples = unpack(t,data)

    #get list of stamps position
    positions = [i for i in range(len(dataTuples)-1) if dataTuples[i] == stampChar]
    #file has no stamp
    if len(positions) == 0:
        return data, -1

    #get first stamp, convert to int from two short in litlle endian
    firstStamp = unpack('<I',pack('<2h',*dataTuples[positions[0]+2:positions[0]+4]))

    #get data without stamp
    for pos in positions:
        if pos % ((160 * nbChannel) + 4) == 0:
            unstampData     += dataTuples[previousPosition:pos]
            previousPosition = pos + 4

    unstampData += dataTuples[previousPosition:]

    t = '<' + str(len(unstampData)) + 'h'

    #return bytes of data without stamp
    return pack(t,*unstampData), firstStamp[0]

#stamp Data
def getStampData(data, firstStamp, nbChannel, stampChar):
    stampData = ()
    t = '<' + str(len(data) // 2) + 'h'

    dataTuples = unpack(t, data)
    stampCount = 0

    samplePerStamp = nbChannel * 160
    nbStamp = len(dataTuples) // (nbChannel * 160)
    previous = 0

    for i in range(nbStamp):
        #set two first short corresponding to stamp mark
        stampData += unpack('<2h', pack('<2h', stampChar, 0))
        #set value of stampCount in int and write it as two show short
        stampData += unpack('<2h', pack('<I', firstStamp + stampCount))
        stampData += dataTuples[previous:previous + samplePerStamp]
        previous  += samplePerStamp
        #increase value of stamp
        stampCount += 1

    t = '<' + str(len(stampData)) + 'h'
    return pack(t, *stampData)

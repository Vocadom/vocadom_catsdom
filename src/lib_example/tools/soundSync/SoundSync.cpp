/*
 * @Author: Clémence VIAL 
 * @Date: 2020-01-08 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2020-01-09 11:08:28 
 */

#include "SoundSync.hpp"
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <sys/time.h>

/* ====================
    Globals
   ==================== */  
pthread_mutex_t lockFdOut = PTHREAD_MUTEX_INITIALIZER;

/**
 * @brief Convert 4 bytes
 * @param source
 * @param startIndex
 * @return int32_t
 */
int32_t fourBytesToInt32 (uint8_t *source, int startIndex)
{
    return ((source[startIndex + 3] << 24) | (source[startIndex + 2] << 16) | (source[startIndex + 1] << 8) | source[startIndex]);
        
    // optimal
    //return (*((int32_t*)(source + startIndex));
}

/**
 * @brief Convert 2 bytes
 * @param source
 * @param startIndex
 * @return int16_t
 */
int16_t twoBytesToInt16 (uint8_t *source, int startIndex)
{
    return ((source[startIndex + 1] << 8) | source[startIndex]);
}

/**
 * @brief init_SoundSync Initialize when input is a WAV file
 * @param fdin existing file descriptor
 * @param currentSoundInfo sound characteristics
 * @return int
 */
int init_SoundSync(int fdin, SoundInfo *currentSoundInfo)
{
    uint8_t header[MAX_SIZE_HEADER_WAV];
    int bytesRead;

    int sizeHeader = MIN_SIZE_HEADER_WAV;
    bytesRead = read(fdin, (char *)header, MIN_SIZE_HEADER_WAV);

    uint16_t wavFmt = (uint16_t)twoBytesToInt16 (header,WAV_FMT_POS);

    if( wavFmt == WAV_FMT_EXT)
    {

	bytesRead = read(fdin, (char *)header+bytesRead,MAX_SIZE_HEADER_WAV-MIN_SIZE_HEADER_WAV);
    }
    else
    {
	bytesRead =  read(fdin, (char *)header+bytesRead, MIN_SIZE_HEADER_WAV - bytesRead);
    }
    sizeHeader = bytesRead;

    if(bytesRead == sizeHeader){
        currentSoundInfo->numChannels = (uint16_t) twoBytesToInt16 (header, 22);
        currentSoundInfo->sampleRate = (uint16_t) fourBytesToInt32 (header, 24);
        currentSoundInfo->bitDepth = (uint16_t) twoBytesToInt16 (header, 34);
        return 1;       
    }
    else
        return -1;

}

/**
 * @brief start_SoundSync
 * @param info Process Sound pSamples to add TimeStamp
 * @param fdin input stream
 * @param fdout array of output streams
 * @param nbSock size of fdout
 * @return
 */
int start_SoundSync(const SoundInfo *info, int fdin, const int *fdout, const int *nbSock )
{
    
    uint8_t outBuffer[NBR_BYTES_PER_SEND];      // buffer dispatched on fdout streams
      
    stamp_t stamp;                              // stamp Sound
    
    uint8_t *pSample = NULL;                    // input buffer sample by sample
    int nbSamplePer10ms;                        // samples in 10ms of sound
    int nbSampleOut;                            // samples in NBR_BYTES_PER_SEND (outBuffer)    
    int nbBytesIn;                              // data's number read from fdin
    int nbBytesPerSample;                       // sample size 
    int bytesAvailable = 0;                     // bytes available in output buffer             
    int outByteCount = 0;                       // up to NBR_BYTES_PER_SEND
    int currentSample = 0;                      // sample from input                        

    nbSamplePer10ms = (info->sampleRate / 100) * info->numChannels; // samples per  1/100e of a second
    nbBytesPerSample = info->bitDepth / 8;       
    nbSampleOut = NBR_BYTES_PER_SEND / nbBytesPerSample;

    pSample = new uint8_t[nbBytesPerSample];
    if (NULL == pSample) 
        return -1; 
    
    stamp.value = 0;                             // init stamp's value
    // init stamp's marker which its value is the value saturation
    switch (nbBytesPerSample)                     
    {
        case ONE_BYTES:                         // 0x7F
            stamp.marker[0] = 0x7F;
            break;

        case TWO_BYTES:                         // 0x7FFF
            stamp.marker[0] = 0xFF;
            stamp.marker[1] = 0x7F;
            break;

        case FOUR_BYTES:                        // 0x7FFFFFFF
            stamp.marker[0] = 0xFF;
            stamp.marker[1] = 0xFF;
            stamp.marker[2] = 0xFF;
            stamp.marker[3] = 0x7F;
            break;

        default:
            break; 
    }

    while(1) 
    {
        nbBytesIn = 0;
        do
	{
	  nbBytesIn += read(fdin, pSample + nbBytesIn, nbBytesPerSample - nbBytesIn);
	}while(nbBytesIn < nbBytesPerSample);
	
        if(0 >= nbBytesIn)
            break;

        pthread_mutex_lock(&lockFdOut);
        if(*nbSock <= 0)
        {
            pthread_mutex_unlock(&lockFdOut);
            break;
        }
        pthread_mutex_unlock(&lockFdOut);

        // decrement pSample value if saturation (portable code)
        switch (nbBytesPerSample)
        {
            case ONE_BYTES:
                if(0x7F == pSample[0])
                    pSample[0] = 0x7E;      // (char)(*pSample)--;
                break;
                
            case TWO_BYTES: // little endian
                if( 0xFF == pSample[0] && 0x7F == pSample[1])    
                    pSample[0] = 0xFE;      // (short)(*pSample)--;
                break;
            
            case FOUR_BYTES: // little endian
                if( 0xFF == pSample[0] &&  0xFF == pSample[1] && 
                    0xFF == pSample[2] &&  0x7F == pSample[3] )                
                
                    pSample[0] = 0xFE;      // (int)(*pSample)--;
                break;
                
            default:
                break;
        }

        //  Every 10ms stamp's number is added to the sound RAW 
        if(0 == (currentSample % (nbSamplePer10ms + (SIZE_SOUND_STAMP / nbBytesPerSample))))
        {
            // Test bytes available in output buffer 
            // If there isn't enough bytes available stamp is cut in half
            if((NBR_BYTES_PER_SEND - SIZE_SOUND_STAMP) <= outByteCount)     
            {
                bytesAvailable = NBR_BYTES_PER_SEND - outByteCount;   
                memcpy(&outBuffer[outByteCount], &stamp, bytesAvailable);                                 

                pthread_mutex_lock(&lockFdOut);
                for(int i = 0; i < *nbSock ; i++)
                    write(fdout[i], outBuffer, sizeof(outBuffer));
                pthread_mutex_unlock(&lockFdOut);

                memcpy(outBuffer, (&stamp)+bytesAvailable, (SIZE_SOUND_STAMP - bytesAvailable));

            }
            else
            
                memcpy(&outBuffer[outByteCount], &stamp, SIZE_SOUND_STAMP);

            currentSample += (SIZE_SOUND_STAMP/ nbBytesPerSample);
        	outByteCount = (currentSample % nbSampleOut) * nbBytesPerSample;
        	stamp.value++;

        }

        for(int i = 0; i < nbBytesPerSample; i++)                        // sample received is added to output buffer
            outBuffer[outByteCount+i] = pSample[i];

        currentSample++;
        outByteCount = (currentSample % nbSampleOut) * nbBytesPerSample;

        
        if(0 == (currentSample % nbSampleOut))                          // outbuffer is sent when currentSample reach the limit
        {
            pthread_mutex_lock(&lockFdOut);
            for(int i = 0; i < *nbSock ; i++)
                write(fdout[i], outBuffer, sizeof(outBuffer) );
            pthread_mutex_unlock(&lockFdOut);
            
        }
    }

    if(0 != (currentSample % nbSampleOut))
    {
        pthread_mutex_lock(&lockFdOut);
        for(int i = 0; i < *nbSock ; i++)
            write(fdout[i], outBuffer, outByteCount);            // samples remaining in outBuffer are sent
        pthread_mutex_unlock(&lockFdOut);
    }

    delete[] pSample;

    return nbBytesIn;

}

/**
 * @brief start_SoundDeSync Process Sound pSamples to add TimeStamp
 * @param info sound characteristics
 * @param fdin input stream
 * @param fdout output stream
 * @param firstStamp first stamp detected in fdin
 * @return
 */
int start_SoundDeSync(const SoundInfo *info, int fdin, int fdout, unsigned int &firstStamp)
{    
    uint8_t inBuffer[NBR_BYTES_PER_RECV*16 + SIZE_SOUND_STAMP];           // input buffer from fdin
    uint8_t outBuffer[NBR_BYTES_PER_RECV*16];          // buffer sent to fdout
    
    int nbBytesIn;                                  // data's number readen
    int nbBytesOutput;                              // data's number written
    int nbBytesToWrite = 0;                         // data's number to write
    int bytesAvailable = 0;                         // bytes available in input buffer

    bool detectFirstStamp = false;                  // first stamp's detection
    bool detectStamp = true;                        // stamp's detection
    stamp_t stamp;                                  // stamp detected

    uint8_t *pSample = NULL;                        // input buffer sample by sample
    int nbBytesPerSample = 0;                       // sample size 
    int nbSampleIn = 0;                             // samples in NBR_BYTES_PER_RECV (inBuffer)    

    nbBytesPerSample = info->bitDepth / 8;
    nbSampleIn = NBR_BYTES_PER_RECV / nbBytesPerSample;

    pSample = new uint8_t[nbBytesPerSample];
    if (NULL == pSample) 
        return -1; 

    int p = 0;

    int bytesToRead = NBR_BYTES_PER_RECV*16 + SIZE_SOUND_STAMP;

    bool calibrate_on_stamp  = false;
    short previous_ff        = 0;

    //read until we are on an stamp
    while(!calibrate_on_stamp)
    {
        nbBytesIn = read(fdin, inBuffer + previous_ff, bytesToRead - previous_ff);

        for(int i=0; i<nbBytesIn-1; i++)
        {

            if((char)inBuffer[i] == '\xFF' && (char)inBuffer[i+1]=='\x7F')
            {
                //remove all part before the first stamp
                memcpy(inBuffer,inBuffer+i,nbBytesIn-i);

                nbBytesIn          = nbBytesIn-i;
                calibrate_on_stamp = true;

                break;
            }
        }
        //check if last value could be a stamp at next reading
        if((char)inBuffer[nbBytesIn - 1] == '\xFF')
        {
            inBuffer[0] == inBuffer[nbBytesIn - 1];
            previous_ff = 1;
        }
        else
        {
            previous_ff = 0;
        }
    }

	
    // read data from fdin and write to fdout
    while(1) 
    {
        //if it's the first data received keep the previous
        //data from the first stamp detected
        if(!calibrate_on_stamp)
            nbBytesIn = 0;
        else
            calibrate_on_stamp = false;

	while(nbBytesIn < bytesToRead)
	{
	    int has_been_read = read(fdin, inBuffer+nbBytesIn, bytesToRead -nbBytesIn);
	    if(has_been_read < 0)
	    {
		nbBytesIn = 0;
		break;
	    }
	    nbBytesIn += has_been_read;
	}

        if(0 >= nbBytesIn)
            break;

        for(int i = 0; i < nbBytesIn; i += nbBytesPerSample)
        {
            detectStamp = true ;
            
            switch(nbBytesPerSample)
            {                                  
                case ONE_BYTES:
                    if( '\x7F' != inBuffer[i] )                                 // Test if inBuffer's value isn't the value's saturation (0x7F)                  
                    {
                        outBuffer[nbBytesToWrite] = inBuffer[0];                             // value is added to outBuffer
                        nbBytesToWrite++;
                         detectStamp = false;
                    }
                    break;

                case TWO_BYTES:
                    if( ('\xFF' != inBuffer[i]) && ('\x7F' != inBuffer[i+1]) )    // if inBuffer's value isn't the value's saturation 
                    
                    {
                        outBuffer[nbBytesToWrite] = inBuffer[i];                               // value is added to outBuffer
                        outBuffer[nbBytesToWrite+1] = inBuffer[i+1];                 
                        nbBytesToWrite+=2; 
                        detectStamp = false;  
                    }
                    break;

                case FOUR_BYTES:
                    if( '\xFF' != inBuffer[i] && '\xFF' != inBuffer[i+1] 
                     && '\xFF' != inBuffer[i+2] && '\x7F' != inBuffer[i+3] )     
                    {
                        for(int j=0 ; j <nbBytesPerSample ; j++)
                            outBuffer[nbBytesToWrite+j] = inBuffer[j+i];
                                      
                        nbBytesToWrite+=4;    
                        detectStamp = false;                        
                    }    
                    break;

                default:
                    break;
            }

            if(detectStamp)  
            {                                                             // stamp marker (value's staturation) is removed and first stamp is detected
                bytesAvailable = nbBytesIn - i;

                if(SIZE_SOUND_STAMP <= bytesAvailable)
                {
                    memcpy((uint8_t *) &stamp, &inBuffer[i], SIZE_SOUND_STAMP);
                    i += SIZE_SOUND_STAMP - 2;  
                }
                else
                {
                    memcpy((uint8_t *) &stamp, &inBuffer[i], bytesAvailable);
                    read(fdin, (uint8_t *) (&stamp) + bytesAvailable, SIZE_SOUND_STAMP - bytesAvailable );
                    i = nbBytesIn;  
                }

                if(!detectFirstStamp)                                     // first stamp's detection
                {  
                    firstStamp = stamp.value;
                    detectFirstStamp = true;
                }

            } 
        }

        if(0 < nbBytesToWrite)                                           // outBuffer sending
        {
            nbBytesOutput = write(fdout, outBuffer, nbBytesToWrite);
            p+=nbBytesToWrite;
            if(0 >= nbBytesOutput )
                break;
        }

        nbBytesToWrite = 0; 
        
    }

    delete[] pSample;

    return nbBytesIn;
}

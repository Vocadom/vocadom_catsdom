#include "tcpConnection.hpp"
#include "config.hpp"
#include "recoLocution.hpp"
#include "config_acquisitor_portaudio_online.hpp"
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>

#define CHUNK 8000 // correspond to 0.5 seconds
#define KALDI_SCRIPT "LOCU"
const std::string file = "VocadomConf.xml";

static pthread_cond_t condition = PTHREAD_COND_INITIALIZER;                // condition use to synchronize the connection with servers
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;   
//socket server
static int sock_locution;

//stampStream server port 
static int port_locution;
static std::string  adress_locution;

static int port_nlu;
static std::string adress_nlu;

static int first_stamp;

/* ----------------------------------------------------------------------
    Thread : Connection to VAD
   ---------------------------------------------------------------------- */
void* run_kaldi(void*)
{
    system(KALDI_SCRIPT);
}

void* server_locution(void *)
{
    int buffer[CHUNK];
    bool connection = false;

    while(1)
    {
	std::cout << "dans serveur loc début" << std::endl;
	int sock_vad = accept(sock_locution, NULL, NULL);
	if(sock_vad == -1)
	{
	    perror("sock VAD");
	    pthread_exit(NULL);	
	}
        std::cout << "dans serveur loc après création socket vad" << std::endl;
	connection = true;
	while(connection)
	{
	    int nb_read = 0;

	    while(nb_read < CHUNK)
	    {
		int ret = read(sock_vad, buffer + nb_read, CHUNK - nb_read);
		if(ret <= 0)
		{
		    std::cout << "VAD disconnected" << std::endl;
		    connection = false;
		    break;
		}
		nb_read += ret;
	    }
	    std::cout << "dans serveur loc avant write à socket NLU" << std::endl; 
	    std::cout << "nlu : port" << port_nlu << " address" << adress_nlu << std::endl;
	    int sock_NLU = connect_tcp_client(port_nlu, adress_nlu);
	    write(sock_NLU,"1",1);
	    std::cout << "sock nlu" << sock_NLU << std::endl;
	    std::cout << "dans serveur loc après write à socket NLU" << std::endl;    
	    return NULL;
	}
    }
}


int recoLocution_init()
{
    if(GetPort(file, MACHINE, "PortRecoLocution", port_locution) != 1)
    {
        std::cout <<"Failed to get port of RecoLocution " << port_locution << MACHINE << std::endl;
        return -1;
    }

    if(GetAdress(file, MACHINE, "AdressRecoLocution", adress_locution) != 1)
    {
	std::cout << "Failed to get adress of RecoLocution" << std::endl;
    }
    
    if(GetPort(file, MACHINE, "PortNLU", port_nlu) != 1)
    {
        std::cout <<"Failed to get port of NLU " << MACHINE << std::endl;
        return -1;
    }

    if(GetAdress(file, MACHINE, "AdressNLU", adress_nlu) != 1)
    {
	std::cout << "Failed to get adress of NLU" << std::endl;
    }
       
    return 1;
}

int recoLocution_preprocess()
{
    std::cout << "preprocess" << std::endl;
    sock_locution = create_tcp_server(port_locution, adress_locution);
    if(sock_locution < 0)
    {
	return -1;
    }
    return 1;
}
int recoLocution_process()
{
    pthread_t thread_server_locution;
    int  new_thread_server_locution;
    std::cout << "dans process début" << std::endl;
   // if(pthread_create( &thread_server_locution , NULL ,  server_locution , NULL) != 0)
    new_thread_server_locution = pthread_create( &thread_server_locution , NULL ,  server_locution , NULL); 
    if (new_thread_server_locution)
    {
	std::cout << "thread client to stamp stream failed" << std::endl;
	return -1;
    }
    if(pthread_join(thread_server_locution, NULL) != 0)
    {
	perror("pthread_join");
	return -1;    
    }
    std::cout << "dans process fin" << std::endl;
    return 1;
}

int recoLocution_postprocess()
{
    return 1;
}
int recoLocution_finish()
{
    return 1;
}

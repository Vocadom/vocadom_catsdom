#ifndef RECOLOCUTION_HPP
#define RECOLOCUTION_HPP

#define MACHINE "recoLocution" // Module's name
#define PORTKALDI 7000	       //Kadi Server 

/* ====================
	Prototypes
   ==================== */ 
extern "C" int recoLocution_init();
extern "C" int recoLocution_preprocess();
extern "C" int recoLocution_process();
extern "C" int recoLocution_postprocess();
extern "C" int recoLocution_finish();

#endif //RECOLOCUTION_HPP

/* include ================================================================== */
#include <sndfile.h>
#include <sys/ipc.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>										// using for SHRT_MAX
#include <sys/time.h>									// using to know time
#include <portaudio.h>									// using to record audio
#include <fcntl.h>
#include <unistd.h>
#include <iostream>										// using for stdin and stdout
#include <sys/wait.h> 									// using for signal(SIGPIPE, SIG_IGN);
#include <pthread.h>									// using to create thread
#include <sys/socket.h>									// using for tcp server
#include <netinet/in.h>
#include <arpa/inet.h>

#include "SoundSync.hpp"
#include "config.hpp"
#include "tcpConnection.hpp"
#include "config_acquisitor_portaudio_online.hpp"
#include "include/stampAudioOnline.hpp"
#include "include/record_play_manager.h"
#include "include/config.h"
#include "include/acquisition_multi_channels.h"

static std::string inputFolder; 
static std::string outputFolder;
static std::string file = "VocadomConf.xml";

/***** Portaudio ***************************************/
static configuration_acquisitor *Cfg_acquisitor; 		// structure pour la configuration de l'acquisition
static PaStream *stream;
static SAMPLE **buffOutput; 							// buffer résultat de FRAMES_PER_BUFFER échantillons à fréquence = SAMPLE_RATE
static int nb_channels;									// channel's number
static int frames_per_buffer;							// buffer input size per channel	
static int sample_rate;	
static int channel_max_per_port;
static int port_audio;
static int bit_depth;
static double ratio;

/***** File wav record ********************************/
static file_channel *f_channel = NULL; 					// structure for files management			
static struct timeval ini_timestamp;
static 	char f_name[200];

/***** TCP server *************************************/
static int portServer = 0;								// server's port
static std::string adressServer;
static int sockserverfd = 0;							// socket server
static int nbSock=0;                                           // socket client's number open
static int sockclientTab[NBR_CLIENTS];                         // Array of sockets clients
static pthread_cond_t condition = PTHREAD_COND_INITIALIZER;    // mutex condition to waiting new client connexion
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static bool begin_to_send = false;								// boolean to know when the server has clients to send samples 
static int pipe_fd[2];												// pipe's file descriptor :  fd[0] for reading fd[1] for writting

/****For OpenHab *************************************/
std::string MStart;
static std::string MinoucheInfo = "script/MinoucheId.sh";

/* ----------------------------------------------------------------------
    Function : samples acquisitions
    		   save sample in wav file 
    		   send sample in the pipe 
   ---------------------------------------------------------------------- */
/**
 * @brief callback_acquisition samples acquisitions
                               save sample in wav file
                               send sample in the pipe
 * @param buffer wav data
 */
void callback_acquisition(const SAMPLE **buffer)
{
	int j, count = 0;
	short int buffer_short_int[frames_per_buffer * nb_channels];
	
	if(begin_to_send)
	{
		for (int i = 0; i < nb_channels; i++)
		{
		    // writting sample to file wav
		    update_wav_float(buffer[i], f_channel[i].name_wav, frames_per_buffer, SAMPLE_RATE, bit_depth, f_channel[i].file_wav);
			
            // Scales data float to short int
			for (j = 0; j < frames_per_buffer; j++)
			{ 
				buffer_short_int[i + (j * nb_channels)] = buffer[i][j] * SHRT_MAX;
				count++;
			}    
		}
		// send sample to the pipe
		write(pipe_fd[1], buffer_short_int, sizeof(buffer_short_int));
	}
}

/**
 * @brief convert_sample_rate select sample in input buffer
 * @param buffInput wav data
 */
void convert_sample_rate(SAMPLE **buffInput)
{
	int i, j;
	SAMPLE **buffer = buffInput + channel_max_per_port * (port_audio - 1);

	for (i = 0; i < nb_channels; i++)
		for (j = 0; j < frames_per_buffer; j++)
		{
			buffOutput[i][j] = buffer[i][j * (int)ratio];
		}
}

/**
 * @brief recordCallback samples acquisitions
 * @param inputBuffer
 * @param outputBuffer
 * @param samplesPerBuffer
 * @param timeInfo
 * @param statusFlags
 * @param userData
 * @return
 */
static int recordCallback(const void *inputBuffer, void *outputBuffer, unsigned long samplesPerBuffer, const PaStreamCallbackTimeInfo *timeInfo,
    PaStreamCallbackFlags statusFlags, void *userData)
{
	convert_sample_rate((SAMPLE **)inputBuffer);
	callback_acquisition((const SAMPLE **)buffOutput);

	return paContinue;
}

/**
 * @brief sendToClient Thread : Read pipe
                       Adding stamp to the data readen
                       Send the data to socket clients
 * @return
 */
void *sendToClient(void*)
{  
    int err;                                        // return of start_SoundSync 
    short int buffer[frames_per_buffer * nb_channels];
    SoundInfo currentSoundInfo;                      // Information about Sound (numChannels, frequence, bitDepth)

    // param in struct SoundInfo
    currentSoundInfo.numChannels = nb_channels;
    currentSoundInfo.sampleRate = SAMPLE_RATE;
    currentSoundInfo.bitDepth = bit_depth;

    while(1)
    {      
        begin_to_send = true;

        std::cout << "Sending... : " << std::endl;

        // Start_soundSync stamp the sound and send to the clients 
        err = start_SoundSync(&currentSoundInfo, pipe_fd[0], sockclientTab, &nbSock);   

	begin_to_send = false;
	char buffer[4096];

	//set pipe non blocking to flush it
	fcntl(pipe_fd[0], F_SETFL, O_NONBLOCK);

	while(read(pipe_fd[0],buffer,4096) > 0);

	//reset pipe to blocking mode
	fcntl(pipe_fd[0], F_SETFL, 0);

        if(err <= 0 )
            std::cout << "Failed to read data :" << strerror(errno) << std::endl;
        
        std::cout << "Waiting client...\n"  << std::endl;
        
        pthread_mutex_lock(&mutex);                 // mutex condition to waiting new clients
        pthread_cond_wait (&condition, &mutex); 
        pthread_mutex_unlock(&mutex); 
    }

    pthread_exit(NULL);
}

/**
 * @brief recvFromClient Thread: Data's reception of one client
                                 Detect if the client is deconnected
                                 If the client is deconnected, it is deleted from socket client Tab
                                 socket client
 * @param sock socket client
 * @return
 */
void *recvFromClient(void *sock)
{   
    if(sock==NULL)
        return 0;    

    char bufferRecv[2];                                 // received buffer from client
    int client =*(int*) sock;                           // client's socket
    int i =0, j=0;

    // While bufferRcv > 0 client is connected
    while(read(client, bufferRecv, sizeof(bufferRecv)) > 0);

    std::cout << "Client " << client << " exit" << std::endl; 
    
    pthread_mutex_lock(&lockFdOut);

    shutdown(client, 2);
    close(client);                                        // client socket is closed

    for(i=0; i<(nbSock-1); i++)                           // client is deleted from socket clients array
    {
        if(sockclientTab[i]==client)
        {
            for(j=i;j<(nbSock-1);j++)
                *(sockclientTab+j)=*(sockclientTab+j+1);
            break;
        }
    }
    nbSock--;

    pthread_mutex_unlock(&lockFdOut);
    pthread_exit(NULL);
}


/**
 * @brief acceptClients Thread : Accept clients connections
 * @return
 */
void *acceptClients(void*)
{
	int new_thread_rcv;                                 // return after thread creation
    int new_thread_send;                                // return after thread creation
    int newsock;                                        // Socket descriptor of the new client
	socklen_t len;                                      // client size
	struct sockaddr_in cli;                             // information about client
    pthread_t thread_rcv[NBR_CLIENTS];
    pthread_t thread_send;
    bool thread_send_exist = false;                     // Using to create only once sending thread

    len = sizeof(cli); 
  	
    while(1)
    {
        // Accept the data packet from client and verification 
        newsock = accept(sockserverfd, (SA*)&cli, &len); 

        if (newsock < 0) 
        { 
            std::cout << "server acccept failed..." << std::endl; 
            break; 
        } 
        
        pthread_mutex_lock(&lockFdOut);             // LockFdOut is a global mutex share with SoundSync.h a
                                                    // Using to access clients array

        if(nbSock >= NBR_CLIENTS)
        {
            std::cout << "There is too much clients. Can't accept the client" << std::endl; 
        }
        else
        {
            std::cout << "Server acccept the client " << newsock << std::endl; 
            
            sockclientTab[nbSock] = newsock;        // Adding new client to clients array

           // Each time a new client is accepted a new thread is created to receive its data 
            new_thread_rcv = pthread_create( &thread_rcv[nbSock] , NULL ,  recvFromClient , (void*) &sockclientTab[nbSock]);
            if(new_thread_rcv)
            {
                std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
                break;
            }

            // Only once thread is created to send WAV file to clients array
            if(!thread_send_exist)
            {
                new_thread_send = pthread_create( &thread_send , NULL ,  sendToClient , NULL);

                if(new_thread_send)
                {
                    std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
                    break;
                }

                thread_send_exist =true;
            }

            nbSock++;
        }
        
        pthread_mutex_unlock(&lockFdOut);           // lockFdOut released

        pthread_mutex_lock (&mutex);                // A mutex condition released to prevent the precense of a new client
    	pthread_cond_signal (&condition); 
    	pthread_mutex_unlock (&mutex); 

    }

    for(int i = 0; i < nbSock; i++)                 // Close for each received thread 
        pthread_exit(&thread_rcv[i]);               

    pthread_exit(&thread_send);                     // Close sending thread

    for(int i = 0 ; i < nbSock ; i++ )              // Close clients
        close(sockclientTab[i]);
     
    close(sockserverfd);                            // Close server


    pthread_exit(NULL);
}

/*********************************************************/

int stampAudioOnline_init()
{
	char Cfg_acquisitor_file[100];
	char *pPath;
	int flag_device = 0;
	PaError err = paNoError;
	PaStreamParameters parameters;
	char name_wav[200];
	char path[100];
	FILE *pFile;
	short bits = 16;
	short stereo = 1;
	int nr_ech;
	int i;

	if(GetConfig(file, MACHINE, inputFolder, outputFolder) == -1)
		return -1;

	// Loading portaudio_acquisitor configuration file
	sprintf(Cfg_acquisitor_file, "%s", inputFolder.c_str());

	Cfg_acquisitor = (configuration_acquisitor *)malloc(sizeof(configuration_acquisitor));
	if (Cfg_acquisitor == NULL)
	{
		fprintf(stderr, "Allocation Impossible <Cfg_acquisitor>\n");
		return -1;
	}

    // Load configuation file 
	load_configuration_portaudio_acquisitor(Cfg_acquisitor, Cfg_acquisitor_file);
    nb_channels = Cfg_acquisitor->channel;
    frames_per_buffer = Cfg_acquisitor->frames_per_buffer;
    sample_rate = Cfg_acquisitor->sample_rate; 
    channel_max_per_port = Cfg_acquisitor->channel_max_per_port;
    port_audio = Cfg_acquisitor->port;
    bit_depth = Cfg_acquisitor->bit_depth;

	// Init PortAudio
	err = Pa_Initialize();
	if (err != paNoError)
		stampAudioOnline_finish(err);

	char Trace[1024] = {'\0'};
	for (i = 0; i < Pa_GetDeviceCount(); i++)
	{
		if (strcmp(Pa_GetDeviceInfo(i)->name, Cfg_acquisitor->board) == 0)
		{
			parameters.device = i;
			flag_device = 1;
			printf("stampAudioOnline_online_init() : Pa_GetDeviceInfo(k) : \n\tname: %s\n\tmaxInputChannels: %d\n\tmaxOutputChannels: %d\n\tdefaultLowInputLatency: %f\n\tdefaultLowOutputLatency: %f\n\tdefaultHighInputLatency: %f\n\tdefaultHighOutputLatency: %f\n\tdefaultSampleRate: %f", 
			Pa_GetDeviceInfo(i)->name, Pa_GetDeviceInfo(i)->maxInputChannels, Pa_GetDeviceInfo(i)->maxOutputChannels, Pa_GetDeviceInfo(i)->defaultLowInputLatency, Pa_GetDeviceInfo(i)->defaultLowOutputLatency, Pa_GetDeviceInfo(i)->defaultHighInputLatency, Pa_GetDeviceInfo(i)->defaultHighOutputLatency, Pa_GetDeviceInfo(i)->defaultSampleRate);
		}
	}

	if (flag_device == 0)
	{
		fprintf(stderr, "$BOARD parameter <%s> in the config file is not recognized.\n", Cfg_acquisitor->board);
		return -1;
	}

	parameters.channelCount = channel_max_per_port * (port_audio - 1) + nb_channels;
	parameters.sampleFormat = PA_SAMPLE_TYPE; //----------------- Non interleaved
	parameters.suggestedLatency = Pa_GetDeviceInfo(parameters.device)->defaultLowInputLatency;
	parameters.hostApiSpecificStreamInfo = NULL;
	
	ratio = (float)sample_rate / (float)SAMPLE_RATE;
	if (ratio != 1.0 && ratio != 2.0 && ratio != 3.0)
	{
		fprintf(stderr, "Sample rate must be 16000, 32000 or 48000");
		return -1;
	}

	//Begin to record stream
	err = Pa_OpenStream(&stream, &parameters, NULL, sample_rate, frames_per_buffer * (int)ratio, paClipOff,	recordCallback,	NULL);
	if (err != paNoError)
		stampAudioOnline_finish(err);

	// Buffer contains acquisition of sound - size : frame_per_buffer * channel
	buffOutput = (SAMPLE **)malloc(nb_channels * sizeof(SAMPLE *));
	for (i = 0; i < nb_channels; i++)
		buffOutput[i] = (SAMPLE *)malloc(frames_per_buffer * sizeof(SAMPLE));


	printf("\nAcquisition online with PortAudio:\n");
	printf("\tCard: \t\t\t%s\n", Pa_GetDeviceInfo(parameters.device)->name);
	printf("\tChannels: \t\t%d\n", parameters.channelCount);
	printf("\tSample rate: \t\t%d\n\n", sample_rate);
	gettimeofday(&ini_timestamp, NULL);
	
	f_channel = (file_channel *)malloc(nb_channels * sizeof(file_channel));
	for (int channel = 0; channel < nb_channels; channel++)
	{
		// Name file wav
		generate_name_wav(name_wav, channel, 0, ini_timestamp);
		strcpy(f_channel[channel].name_wav, name_wav);
		strcat(f_channel[channel].name_wav, ".wav");

		// Path folder output + wav file
		sprintf(path, "%s/", outputFolder.c_str());
		strcat(path, f_channel[channel].name_wav);
		strcpy(f_channel[channel].name_wav, path);
		update_wav_float(NULL, f_channel[channel].name_wav, 0, SAMPLE_RATE, bit_depth, f_channel[channel].file_wav);

		// Retrieves header informations
		pFile = fopen(f_channel[channel].name_wav, "rb");
		if(pFile != NULL)
		{
			int fe = lire_entete_wav(pFile, &bits, &stereo, &nr_ech);
			printf("\t%s\n\tBits: \t\t\t%d\n\tChannels: \t\t%d\n\tSample rate: \t\t%d\n", f_channel[channel].name_wav, bits, stereo, fe);
			fclose(pFile);
		}
		else
			return -1;

		if (channel < nb_channels - 1)
			printf("\n");
	}
	

        std::ifstream ifs(MinoucheInfo.c_str());
        std::string curlCommandAndData;
        MStart="Start";
        curlCommandAndData.assign((std::istreambuf_iterator<char>(ifs)),
			           (std::istreambuf_iterator<char>()));
        curlCommandAndData = curlCommandAndData.substr(0, curlCommandAndData.size()-1);
        curlCommandAndData = curlCommandAndData + "\"" + MStart + "\"" ;
        if(system(NULL))
	    system(curlCommandAndData.c_str());
	return 1;
}

int stampAudioOnline_preprocess()
{
	PaError err = paNoError;
	int retServer =0;
	
	if(pipe(pipe_fd) != 0) 				// init pipe to trasnfer stream from PA to start_soudSync
		return -1;

	err = Pa_StartStream(stream);	// Start to listen stream from port audio
	if (err != paNoError)
	{
		fprintf(stderr, "stampAudioOnline_process() : Error, stream couldn't be started\n");
		return -1;
	}

    // init server's port for sending stream
    if(GetPort(file, MACHINE, "PortStampStream", portServer) == -1)
        portServer = PORT;


    if(GetAdress(file, MACHINE, "AdressStampStream", adressServer) == -1)
        adressServer = ADRESS;

    sockserverfd = create_tcp_server(portServer, adressServer);	// Init tcp server
	if( sockserverfd == -1)
		return -1;

	return 1;
}

int stampAudioOnline_process()
{
	int new_thread_accept;        // return after thread creation
	pthread_t thread_accept;	  // thread to accept client tcp
	PaError err = paNoError;	  // detect error during listenning port audio

	new_thread_accept = pthread_create( &thread_accept , NULL ,  acceptClients , NULL);
    if(new_thread_accept)
    {
        std::cout << "stampAudioOnline_process() : Failed to create thread:" << strerror(errno) << std::endl;
        return -1;
    }

    // Recording audio
	while(1){
	
		err = Pa_IsStreamActive(stream);

		if (err < 0)
		{
			fprintf(stderr, "stampAudioOnline_process() : Error, stream became inactive without having been closed\n");
			return -1;
		}
		else if(err != 1)
		{	// Log
			printf("stampAudioOnline_process() : Fin de la lecture\n");
			break;
		}
    }         
   
    if (pthread_join(thread_accept, NULL) ) 
    {
        perror("pthread_join");
        return -1;
    }

    return 1;  
}

int stampAudioOnline_postprocess()
{
	return 1;
}

int stampAudioOnline_finish(PaError err)
{
	if (err != paNoError)
	{
		fprintf(stderr, "An error occured while using the portaudio stream\n");
		fprintf(stderr, "Error number: %d\n", err);
		fprintf(stderr, "Error message: %s\n", Pa_GetErrorText(err));
	}

	err = Pa_CloseStream(stream);
	Pa_Terminate();

	if (buffOutput)
	{
		int i;
		for (i = 0; i < nb_channels; i++)
			if (buffOutput[i])
				free(buffOutput[i]);
		free(buffOutput);
	}
	if (Cfg_acquisitor)
		free(Cfg_acquisitor);
	
	return 1;
}

/*
     Copyright (C) 2012-2016 Laboratoire d'Informatique de Grenoble (www.liglab.fr),
     Centre National de la Recherche Scientifique (www.cnrs.fr).
     The initial authors of the original code are Michel Vacher, François Portet 
     and Frédéric Aman.
     Contributors: Michel Vacher, François Portet, Frédéric Aman, Fabien Eloy, 
     William Duclot, Cheick Mahady Sissoko, Dan Istrate, Hubert Glasson, Stéphane 
     Chaillol and Alain Dufaux.
     
     This file is part of CirdoX.

     CirdoX is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     CirdoX is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CirdoX. If not, see <http://www.gnu.org/licenses/>.

     CirdoX is a software project initiated by the French Centre National 
     de la Recherche Scientifique (CNRS) and the Laboratoire d'Informatique de 
     Grenoble  (LIG) - France (https://www.liglab.fr/).
     This work was supported by the French funding agencies ANR and CNSA through 
     the CIRDO project (ANR-2010-TECS-012).
     CirdoX is a modular software able to analyse online the audio environment, 
     to extract the uttered sentences and then to process them thanks to an ASR stage 
     followed by a filtering stage.

     Contact Frédéric Aman <frederic.aman@gmail.com>, Michel Vacher
     <michel.vacher@imag.fr> or François Portet <francois.portet@imag.fr>
     for more information about the licence or the use of CirdoX.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>

#include "include/sound_object.h"
#include "include/record_play_manager.h"

//variables globales
static FILE **file_wav;
static short int *buffTemp;
static int frames_per_buffer;
static int nbChan;


/*******************************************************************************
 *	Fonction :	 	create_wav
 *
 *	Description : 	Creation d'un fichier audio wav avec libsndfile
 *	Input	:	SAMPLE signal, char *filename, int size
 *	Output/return : int err
 *******************************************************************************/
void record_wav_float(const SAMPLE *buffer_float, char filename[], int size, int sample_rate)
{
	//param : 0 -> global recording ; 1 -> event recording
	char path[100];
	char f_name[200];
	FILE *pFile;
	short bits = 16;
	short stereo = 1;
	int j = 0;
	int fs = sample_rate;
	short int *buffer_short_int;

	buffer_short_int = (short int *)malloc(size * sizeof(short int));
	for (j = 0; j < size; j++)
	{ // Scales data float to short int
		buffer_short_int[j] = buffer_float[j] * SHRT_MAX;
	}

	if (getenv("CIRDOX_WAV_PATH") != NULL)
	{
		sprintf(path, "%s/", getenv("CIRDOX_WAV_PATH"));
	}
	else
	{
		fprintf(stderr, "record_wav_float() : Error getenv(CIRDOX_WAV_PATH), exit program!\n");
		fflush(stderr);
		exit(EXIT_FAILURE);
	}
	strcat(path, filename);
	strcpy(f_name, path);

	pFile = fopen(f_name, "wb");
	if (pFile != NULL)
	{
		ecrire_entete_wav(pFile, &fs, &bits, &stereo, size);
		fwrite(buffer_short_int, sizeof(short int), size, pFile);
		fclose(pFile);
	}
	else
	{
		fprintf(stderr, "record_wav_float() : Error open file, exit program!\n");
		fflush(stderr);
		//exit(EXIT_FAILURE);
	}

	if (buffer_short_int != NULL)
	{
		free(buffer_short_int);
		buffer_short_int = NULL;
	}
}

void update_wav_float(const SAMPLE *buffer_float, char f_name[], int size, int sample_rate, int bits, FILE *pFile)
{
	char path[100];
	short stereo = 1;
	short int sbits = (short int) bits;
	int fs = sample_rate;
	short int *buffer_short_int;
	int j;


	buffer_short_int = (short int *)malloc(size * sizeof(short int));

	for (j = 0; j < size; j++)
	{ // Scales data float to short int
		buffer_short_int[j] = buffer_float[j] * SHRT_MAX;
	}

	// Writes data

	pFile = fopen(f_name, "ab");
	if (pFile != NULL)
	{
		fwrite(buffer_short_int, sizeof(short int), size, pFile);
		fclose(pFile);
	}
	else
	{
		fprintf(stderr, "update_wav_float() : Error open file, exit program!\n");
		fflush(stderr);
		//exit(EXIT_FAILURE);
	}

	// Updates header
	pFile = fopen(f_name, "rb+");
	if (pFile != NULL)
	{
		fseek(pFile, 0, SEEK_END);
		size = ftell(pFile);
		size = (size - 44) / sizeof(short int);
		rewind(pFile);
		ecrire_entete_wav(pFile, &fs, &sbits, &stereo, size);
		fclose(pFile);
	}
	else
	{
		fprintf(stderr, "update_wav_float() : Error open file, exit program!\n");
		fflush(stderr);
		//exit(EXIT_FAILURE);
	}

	if (buffer_short_int != NULL)
	{
		free(buffer_short_int);
		buffer_short_int = NULL;
	}
}

/*******************************************************************************
 * La fonction lit l'en-tete d'un fichier wave              
 * qui a t ouvert en f,et retourne la frquence d'chantillonnage
 * et aussi le nombre de bits utiliss,si il est stereo ou non et le nombre 
 * d'chantillons 
 *
 * 26 mars 2004 : correction bug () , possibilit d'extrabits
 *                (D. Istrate)
 * 9 janvier 2006 : modification pour prise en compte de "fact" si il existe,
 *                  il ne devrait pas y en avoir car pas de compression, mais
 *                  certains logiciels le mettent!
 *                  (S. Chaillol, M. Vacher)
 * 
 ******************************************************************************/
int lire_entete_wav(FILE *f, short int *bits, short int *stereo, int *nr_ech)
{
	char var[10];
	int t, bps;
	int longueur;
	short int type, t1;
	int freq, i;
	int t2;	// 26 mars 2004 : la valeur lue dans t ne doit pas tre
			   // efface
	int tfact; // taille data du fact
	char temp; // 26 mars 2004 : buffer de 1 char
	int coeff;

	/* RIFF Type Chunk - Tronon RIFF */
	fread(var, sizeof(char), 4, f); //On lit les 4 lettres de dbut "RIFF"
	if ((var[0] != 'R') || (var[1] != 'I') || (var[2] != 'F') || (var[3] != 'F'))
	{
		fprintf(stderr, "Structure RIFF non conforme!\n");
		fflush(stderr);
		var[4] = 0;
		fprintf(stderr, "LU : %s (\"RIFF\")\n", var);
		fflush(stderr);
		return (-1);
	}

	// chane ASCII : "RIFF" lue
	//On lit la longueur du fichier en octets -8 et on la mmorise
	fread(&longueur, sizeof(int), 1, f);

	//on doit lire WAVE
	fread(var, sizeof(char), 4, f);
	if ((var[0] != 'W') || (var[1] != 'A') || (var[2] != 'V') || (var[3] != 'E'))
	{
		fprintf(stderr, "Structure RIFF WAVE non conforme!\n");
		fflush(stderr);
		var[4] = 0;
		fprintf(stderr, "LU : %s (\"WAVE\")\n", var);
		fflush(stderr);
		return (-1);
	}
	// chane ASCII : "WAVE" lue
	// Fin du tronon RIFF

	// Format Chunk "fmt" - Tronon format
	fread(var, sizeof(char), 4, f); //On lit fmt
	if ((var[0] != 'f') || (var[1] != 'm') || (var[2] != 't') || (var[3] != ' '))
	{
		fprintf(stderr, "Structure RIFF : tronon fmt absent!\n");
		fflush(stderr);
		var[4] = 0;
		fprintf(stderr, "LU : %s (\"fmt \")\n", var);
		fflush(stderr);
		return (-1);
	}
	// chane ASCII : "fmt " lue
	// On lit la dimension du tronon
	fread(&t, sizeof(int), 1, f);
	//On lit le type de fichier : 1==non compress
	fread(&type, sizeof(short int), 1, f);
	if (type != 1)
	{
		fprintf(stderr, "\n Je ne peux pas lire ce type de fichier (fichier compress)! ");
		fflush(stderr);
		return (-1);
	}
	fread(stereo, sizeof(short int), 1, f); //On lit le nombre de canal audio
	// 1 = mono, 2 = stereo
	fread(&freq, sizeof(int), 1, f); //On lit frequence d'echantillonnage
	// exemple = 16000
	fread(&bps, sizeof(int), 1, f);		  //On lit le nombre d'octets par seconde pour temps rel
	fread(&t1, sizeof(short int), 1, f);  //le nombre d'octets  sortir en meme temps
	fread(bits, sizeof(short int), 1, f); //le nombre d'octets utiliss pour coder le signal
	// rajout 26 mars 2004 : lecture des extrabits
	// si t>16, il y a des bits supplmentaires dans le header (=extrabits)
	for (i = 1; i <= t - 16; i++)
		fread(&temp, sizeof(char), 1, f);
	// fin rajout
	// Fin du tronon Format "fmt"

	// Suppression ventuelle du tronon "fact" - Fact Chunck
	fread(var, sizeof(char), 4, f);
	if ((var[0] == 'f') && (var[1] == 'a') && (var[2] == 'c') && (var[3] == 't'))
	{
		// lecture taille donne "fact"
		fread(&tfact, sizeof(int), 1, f);
		// lecture des donnes "fact"
		for (i = 1; i <= tfact; i++)
			fread(&temp, sizeof(char), 1, f);
		// prparation lecture de "data"
		fread(var, sizeof(char), 4, f);
	}
	// Data Chunck "data" - Tronon data
	if ((var[0] != 'd') || (var[1] != 'a') || (var[2] != 't') || (var[3] != 'a'))
	{
		fprintf(stderr, "Structure RIFF : tronon data absent!\n");
		fflush(stderr);
		var[4] = 0;
		fprintf(stderr, "LU : %s (\"data\")\n", var);
		fflush(stderr);
		return (-1);
	}
	fread(&t2, sizeof(int), 1, f); //On lit la dim en octets des donnes audio

	t2 = t2 / (*stereo);
	coeff = *bits / 8;
	t2 = t2 / coeff;
	*nr_ech = t2; //On calcule le nombre d'echantillons pour chaque canal
	// exemple : si *bits=16, *stereo=1
	// 		alors *nr_ech = t2 / 2. = t2 % 2

	//	MessagePopup ("TEST","Avez-vous lu le message d'erreur?");
	return (freq);
}

/***************************************************************************
*La fonction crit l'en-tete d'un fichier wav ouvert en f                  * 
*avec la frquence d'chantillonnage "freq_ech" , le nombre de bits "bits" *
*le type stereo ou non "stereo" et le nombre d'chantillons "nr_ech        *
*
* correction bug dpassement valeur entire pour grands fichiers
* (au lieu de multiplier un grand nombre par une constante et de le diviser 
* ensuite par 8, il faut imprativement d'abord diviser la constante par 8, puis
* faire la multiplication!
* 
* D. Istrate - 9 juillet 2003
* M. Vacher - mise au point le 2 mars 2004
* 
*******************************************************************************/
void ecrire_entete_wav(FILE *f, int *freq_ech, short int *bits, short int *stereo, int nr_ech)
{
	char var[10];
	int longueur, t, bps;
	short int type, t1;

	int istereo, ibits;

	istereo = (int)(*stereo);
	ibits = (int)(*bits);

	/* correction bug 9 juillet 2003
	longueur=(nr_ech*(*stereo)*(*bits)/8)+44-8;//On calcule la longueur en octets du fichier */
	/*	longueur=(nr_ech*(*(stereo)*((*bits)/8))+44-8;//On calcule la longueur en octets du fichier */
	//On calcule la longueur en octets du fichier (modif 2 mars 2004)
	longueur = nr_ech * istereo * (ibits / 8) + 44 - 8;
	sprintf(var, "RIFF");
	fwrite(var, sizeof(char), 4, f);	  //Il faut commencer par RIFF
	fwrite(&longueur, sizeof(int), 1, f); //On ecrit la longueur du fichier
	sprintf(var, "WAVE");
	fwrite(var, sizeof(char), 4, f); //puis WAVE
	sprintf(var, "fmt ");
	fwrite(var, sizeof(char), 4, f); // fmt
	t = 16;
	fwrite(&t, sizeof(int), 1, f); //longueur du header
	type = 1;
	fwrite(&type, sizeof(short int), 1, f); // type PCM
	fwrite(stereo, sizeof(short int), 1, f);
	fwrite(freq_ech, sizeof(int), 1, f);
	/* correction bug 9 juillet 2003
	bps=(*freq_ech)*(*stereo)*(*bits)/8;//On calcule le nombre d'octets par second */
	bps = (*freq_ech) * (*stereo) * ((*bits) / 8); //On calcule le nombre d'octets par second
	fwrite(&bps, sizeof(int), 1, f);
	t1 = ((*bits) / 8) * (*stereo); //on calcule le nombre d'octets  sortir en meme temps
	fwrite(&t1, sizeof(short int), 1, f);
	fwrite(bits, sizeof(short int), 1, f);
	sprintf(var, "data");
	fwrite(var, sizeof(char), 4, f);
	/* correction bug 9 juillet 2003
	t=nr_ech*(*stereo)*(*bits)/8;//la longueur en octets des donnees */
	t = nr_ech * (*stereo) * ((*bits) / 8); //la longueur en octets des donnees
	fwrite(&t, sizeof(int), 1, f);
}

void generate_name_wav(char *filename, int numChan, int id, struct timeval tv)
{
	char channel_char[200];
	struct tm *ptm;
	ptm = localtime(&tv.tv_sec);
	strftime(filename, 200 * sizeof(char), "%Y-%m-%d_%H-%M-%S", ptm);
	sprintf(filename, "%s.%03ld", filename, tv.tv_usec / 1000);
	if (id == 0)
	{
		sprintf(channel_char, "global_channel_%d", numChan);
	}
	else
	{
		sprintf(channel_char, "evt_%d_channel_%d", id, numChan);
	}
	strcat(filename, "-");
	strcat(filename, channel_char);
}

/*
     Copyright (C) 2012-2016 Laboratoire d'Informatique de Grenoble (www.liglab.fr),
     Centre National de la Recherche Scientifique (www.cnrs.fr).
     The initial authors of the original code are Michel Vacher, François Portet 
     and Frédéric Aman.
     Contributors: Michel Vacher, François Portet, Frédéric Aman, Fabien Eloy, 
     William Duclot, Cheick Mahady Sissoko, Dan Istrate, Hubert Glasson, Stéphane 
     Chaillol and Alain Dufaux.
     
     This file is part of CirdoX.

     CirdoX is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     CirdoX is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CirdoX. If not, see <http://www.gnu.org/licenses/>.

     CirdoX is a software project initiated by the French Centre National 
     de la Recherche Scientifique (CNRS) and the Laboratoire d'Informatique de 
     Grenoble  (LIG) - France (https://www.liglab.fr/).
     This work was supported by the French funding agencies ANR and CNSA through 
     the CIRDO project (ANR-2010-TECS-012).
     CirdoX is a modular software able to analyse online the audio environment, 
     to extract the uttered sentences and then to process them thanks to an ASR stage 
     followed by a filtering stage.

     Contact Frédéric Aman <frederic.aman@gmail.com>, Michel Vacher
     <michel.vacher@imag.fr> or François Portet <francois.portet@imag.fr>
     for more information about the licence or the use of CirdoX.
*/

/*******************************************************************************
 *
 *	Fichier stampAudioOnline.h          
 *
 *******************************************************************************/

#ifndef STAMPAUDIOONLINE_HPP
#define STAMPAUDIOONLINE_HPP

/* ====================
    Constants
   ==================== */  
#define PA_SAMPLE_TYPE paFloat32 | paNonInterleaved
#define MACHINE "StampAudioOnline"
#define NBR_CLIENTS 10                                      // server's max number clients which can be accepted
#define ADRESS "127.0.0.1"
#define PORT 5050

/* ====================
    Variables globales
   ==================== */ 
PaStreamParameters inputParameters;

/* ====================
    Prototypes
   ==================== */ 
extern "C" int stampAudioOnline_init();
extern "C" int stampAudioOnline_preprocess();
extern "C" int stampAudioOnline_process();
extern "C" int stampAudioOnline_postprocess();
extern "C" int stampAudioOnline_finish(PaError err);

static int recordCallback(
    const void *inputBuffer, /*!!!!!!!!Buffer!!!!!!!!!*/
    void *outputBuffer,
    unsigned long samplesPerBuffer,
    const PaStreamCallbackTimeInfo *timeInfo,
    PaStreamCallbackFlags statusFlags,
    void *userData);
//void fini();

#endif

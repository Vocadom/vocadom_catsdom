/*
     Copyright (C) 2012-2016 Laboratoire d'Informatique de Grenoble (www.liglab.fr),
     Centre National de la Recherche Scientifique (www.cnrs.fr).
     The initial authors of the original code are Michel Vacher, François Portet 
     and Frédéric Aman.
     Contributors: Michel Vacher, François Portet, Frédéric Aman, Fabien Eloy, 
     William Duclot, Cheick Mahady Sissoko, Dan Istrate, Hubert Glasson, Stéphane 
     Chaillol and Alain Dufaux.
     
     This file is part of CirdoX.

     CirdoX is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     CirdoX is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CirdoX. If not, see <http://www.gnu.org/licenses/>.

     CirdoX is a software project initiated by the French Centre National 
     de la Recherche Scientifique (CNRS) and the Laboratoire d'Informatique de 
     Grenoble  (LIG) - France (https://www.liglab.fr/).
     This work was supported by the French funding agencies ANR and CNSA through 
     the CIRDO project (ANR-2010-TECS-012).
     CirdoX is a modular software able to analyse online the audio environment, 
     to extract the uttered sentences and then to process them thanks to an ASR stage 
     followed by a filtering stage.

     Contact Frédéric Aman <frederic.aman@gmail.com>, Michel Vacher
     <michel.vacher@imag.fr> or François Portet <francois.portet@imag.fr>
     for more information about the licence or the use of CirdoX.
*/

#ifndef _PLUGINS_MANAGER_H_
#define _PLUGINS_MANAGER_H_

#define LEN 256

/*!
     \brief the kind of functions that can be found in the plugins
    */
enum {INIT,FINISH,PROCESS};

/*!
     \brief the plugin structure to store the different function of the plugins
    */

typedef struct plugin_t
{
    char name[ LEN ];
    void* handler;
    void* process;
    void* init;
    void* finish;
} plugin;

/*!
     \brief the atomic element of the list
    */
typedef struct cell
{
    plugin val;
    struct cell* next;
} * plugin_list;


void print_plugin( plugin h );
plugin_list create_list(void);
int is_empty(plugin_list L);
plugin_list add_first(plugin e, plugin_list L);
plugin_list delete_first(plugin_list L);
plugin_list add_last(plugin e, plugin_list L);
plugin* look_up_plugin(char* name, plugin_list L);
void display(plugin_list L);
void print_plugin( plugin h );
char* get_postfix(int proto);
int retrieve_plugins( char* dir, plugin_list *plugins );
int fill_in_plugin(plugin* plug, int proto);
void destroy_plugins( plugin_list *L );


#endif


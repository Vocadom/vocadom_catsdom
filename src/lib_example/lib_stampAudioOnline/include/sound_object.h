/*
     Copyright (C) 2012-2016 Laboratoire d'Informatique de Grenoble (www.liglab.fr),
     Centre National de la Recherche Scientifique (www.cnrs.fr).
     The initial authors of the original code are Michel Vacher, François Portet 
     and Frédéric Aman.
     Contributors: Michel Vacher, François Portet, Frédéric Aman, Fabien Eloy, 
     William Duclot, Cheick Mahady Sissoko, Dan Istrate, Hubert Glasson, Stéphane 
     Chaillol and Alain Dufaux.
     
     This file is part of CirdoX.

     CirdoX is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     CirdoX is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CirdoX. If not, see <http://www.gnu.org/licenses/>.

     CirdoX is a software project initiated by the French Centre National 
     de la Recherche Scientifique (CNRS) and the Laboratoire d'Informatique de 
     Grenoble  (LIG) - France (https://www.liglab.fr/).
     This work was supported by the French funding agencies ANR and CNSA through 
     the CIRDO project (ANR-2010-TECS-012).
     CirdoX is a modular software able to analyse online the audio environment, 
     to extract the uttered sentences and then to process them thanks to an ASR stage 
     followed by a filtering stage.

     Contact Frédéric Aman <frederic.aman@gmail.com>, Michel Vacher
     <michel.vacher@imag.fr> or François Portet <francois.portet@imag.fr>
     for more information about the licence or the use of CirdoX.
*/

#ifndef SOUND_OBJECT_H
#define	SOUND_OBJECT_H

#include <time.h>
#include <sys/time.h>
#include <sys/types.h>

/* Message :
   - type = type of message, mandatory
   - data = data to transmit
 */


#define NB_CLASSES_MAX			17
#define SPEECH				1
#define SOUND				2
#define MAX_HYP_LENGTH			200000
#define MAX_WAV_LENGTH			200
#define MAX_PROBA_LENGTH		200
#define MAX_CONFIDENCE_LENGTH		200
#define UTT_TYPE_COMMAND		1
#define UTT_TYPE_OTHER			2
#define NB_CHAR_UTT			100000


typedef struct {	
 	char 	name[MAX_WAV_LENGTH];					// Nom du fichier wav
 	int 	start_detection;  						// positionnement du début de la détection dans le .wav (echantillons)
 	int 	end_detection; 							// idem pour la fin
	int 	start_memo;  							// positionnement du début de la détection dans le .wav avec quelques trames avant (echantillons)
 	int 	end_memo; 							// idem pour la fin
 	double	snr;								// rsb de l'évènement
 	double	snr_ref;							// rsb de référence au moment de l'evt
 	double	duration;							// duree de l'évènement en ms
	double	duration_with_memo;						// duree de l'évènement en ms avec quelques trames avant et après
 	double	proba_speech;		 					// probabilité Parole
 	double	proba_sound;	 						// probabilité Son
	double	proba_classe[NB_CLASSES_MAX];					// probabilité des classes sons
	char**	names_classes;//[NB_CLASSES_MAX][MAX_PROBA_LENGTH];		// probabilité des classes sons
	int	nb_classes;							// nombre de classes sons
	char**	utterance_hyp;//[MAX_HYP_LENGTH];
	char**	utterance_cmd;
	struct timeval timestamp;  // timestamp of the event that goes up to nanoseconds 
	int	sample_rate;
	char**	confidence_speech;//[MAX_CONFIDENCE_LENGTH];
	char**	distance_filter;//[MAX_CONFIDENCE_LENGTH];
	unsigned int id;
	int	chan_best_snr;
	int	best_asr_id;
	char	cmd[10000];
	float	confidence;
	double start_global;
} sound_object;


void init_sound_object(sound_object *Sound_object);

#endif

/*
     Copyright (C) 2012-2016 Laboratoire d'Informatique de Grenoble (www.liglab.fr),
     Centre National de la Recherche Scientifique (www.cnrs.fr).
     The initial authors of the original code are Michel Vacher, François Portet 
     and Frédéric Aman.
     Contributors: Michel Vacher, François Portet, Frédéric Aman, Fabien Eloy, 
     William Duclot, Cheick Mahady Sissoko, Dan Istrate, Hubert Glasson, Stéphane 
     Chaillol and Alain Dufaux.
     
     This file is part of CirdoX.

     CirdoX is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     CirdoX is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CirdoX. If not, see <http://www.gnu.org/licenses/>.

     CirdoX is a software project initiated by the French Centre National 
     de la Recherche Scientifique (CNRS) and the Laboratoire d'Informatique de 
     Grenoble  (LIG) - France (https://www.liglab.fr/).
     This work was supported by the French funding agencies ANR and CNSA through 
     the CIRDO project (ANR-2010-TECS-012).
     CirdoX is a modular software able to analyse online the audio environment, 
     to extract the uttered sentences and then to process them thanks to an ASR stage 
     followed by a filtering stage.

     Contact Frédéric Aman <frederic.aman@gmail.com>, Michel Vacher
     <michel.vacher@imag.fr> or François Portet <francois.portet@imag.fr>
     for more information about the licence or the use of CirdoX.
*/

/*******************************************************************************
*
*	Fichier trace.H 
*
*	Module de trace dans un fichier...
*
*	Frédéric Aman	09/2012
*
*******************************************************************************/

#ifndef TRACE_H
#define TRACE_H

// DEFINITIONS GLOBALES

// Entete ligne
struct entete_ligne
{
	char NumLigne[6];		// numéro de ligne, de 0 à 999999
	char DHM[25];			// DHM au format AAAA/MM/JJ HH:MM:SS:µµµ
};


// PROTOTYPES
int TRC_InitFichierTrace( char *NomFichier );
int TRC_InitFichierErreur( char *NomFichier );
int TRC_SwitchFichierTrace( FILE *fichierLog );
int TRC_FermeFichierTrace( FILE *fichierLog );
int TRC_FermeFichierErreur( FILE *fichierLog );
int TRC_TraceFichier( char *Buffer );

#endif

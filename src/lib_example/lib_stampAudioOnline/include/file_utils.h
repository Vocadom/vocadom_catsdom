/*
     Copyright (C) 2012-2016 Laboratoire d'Informatique de Grenoble (www.liglab.fr),
     Centre National de la Recherche Scientifique (www.cnrs.fr).
     The initial authors of the original code are Michel Vacher, François Portet 
     and Frédéric Aman.
     Contributors: Michel Vacher, François Portet, Frédéric Aman, Fabien Eloy, 
     William Duclot, Cheick Mahady Sissoko, Dan Istrate, Hubert Glasson, Stéphane 
     Chaillol and Alain Dufaux.
     
     This file is part of CirdoX.

     CirdoX is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     CirdoX is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CirdoX. If not, see <http://www.gnu.org/licenses/>.

     CirdoX is a software project initiated by the French Centre National 
     de la Recherche Scientifique (CNRS) and the Laboratoire d'Informatique de 
     Grenoble  (LIG) - France (https://www.liglab.fr/).
     This work was supported by the French funding agencies ANR and CNSA through 
     the CIRDO project (ANR-2010-TECS-012).
     CirdoX is a modular software able to analyse online the audio environment, 
     to extract the uttered sentences and then to process them thanks to an ASR stage 
     followed by a filtering stage.

     Contact Frédéric Aman <frederic.aman@gmail.com>, Michel Vacher
     <michel.vacher@imag.fr> or François Portet <francois.portet@imag.fr>
     for more information about the licence or the use of CirdoX.
*/

/*******************************************************************************
 *
 * FONCTIONS D'ACCES AUX REPERTOIRES UNIX GNU/LINUX
 *
 * origine : M. Vacher - 15 mars 2002
 *
 ******************************************************************************/

#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

char    *strtok(char *, const char *);

/*******************************************************************************
 *
 *  Type de variables utilisées par certaines fonctions de la librairie :
 *  
 *	Définitions et fonctions associées.
 * 
 ******************************************************************************/

typedef struct {
	char **Finom;	// tableau des éléments
	int NbFinom;		// nbre d'éléments
	int NbMax;		// nbre maximum d'éléments
} FILISTE ; 	// pointe sur une structure contenant des noms de fichiers
		// ou de racines de noms de fichiers

/********************************************************************/
/*                                                                  */
/* Créer une structure vide Filiste contenant au maximum m éléments */
/*                                                                  */
/********************************************************************/
int CreerFiliste( int max , FILISTE *Filiste);

/***********************************/
/*                                 */
/* Supprimer une structure Filiste */
/*                                 */
/***********************************/
int NettoyFiliste( FILISTE *Filiste ) ;

/*********************************************/
/*                                           */
/* Création d'un nouveau Finom dans la liste */
/*                                           */
/*********************************************/
int InserFinom( char *lenom , FILISTE *Filiste ) ;

/**************************************************/
/*                                                */
/* Supprimer le dernier Finom de la liste Filiste */
/*                                                */
/**************************************************/
int ViderFinom( FILISTE *Filiste ) ;

/*********************************************/
/*                                           */
/* Lire le contenu d'un Finom                */
/*                                           */
/*********************************************/
int LireFinom( char *lenom , FILISTE *Filiste , int ind ) ;

/*******************************************************************/
/*                                                                 */
/* Renseigner (remplir) une Filiste                                */
/*                                                                 */
/*******************************************************************/
int RenseignFiliste(FILISTE *Filiste,char *dir_name,char *type) ;

/******************************************************************************/
/*                                                                            */
/*   But: compter le nombre de fichiers ou répertoires                        */
/*                                                                            */
/*   Attention: il ne faut compter ni . , ni ..   !!                          */
/*                                                                            */
/*   Exemple : count_file("../Data");                                         */
/*                                                                            */
/******************************************************************************/

int count_file(char *dir_name);

/******************************************************************************/
/*                                                                            */
/*   But: compter le nombre de fichiers d'un type fixé (exemple = .sam)       */
/*        (le séparateur est toujours le .                                    */
/*         pour .sam indiquer char *type="sam")                               */
/*                                                                            */
/*   On considère des noms de fichier de 3 caractères minimum.                */
/*                                                                            */
/*   Exemple : count_type_file("..","sam");                                   */
/*                                                                            */
/******************************************************************************/

int count_type_file( char *dir_name , char *type);

/******************************************************************************/
/*                                                                            */
/*   But: lister les fichiers d'un type fixé (exemple = .sam)                 */
/*        (le séparateur est toujours le .                                    */
/*         pour .sam indiquer char *type="sam")                               */
/*                                                                            */
/*        l'extension du fichier n'est pas listée, data.sam -> data           */
/*                                                                            */
/*         la liste est mise dans un tableau de chaînes de caractères         */
/*                                                                            */
/*   Renvoie le nombre de fichiers de la liste ou -1 sur erreur               */
/*                                                                            */
/*   On considère des noms de fichier de 3 caractères minimum.                */
/*   Les noms de fichiers ont 250 caractères au maximum.                      */
/*                                                                            */
/*   Exemple : lis_type_file("..","sam",fichlis);                             */
/*                                                                            */
/*	char (*file_lis)[][250];				              */
/*	file_lis=(char (*)[][250]) calloc(nbre_result*250,sizeof(char));      */
/*                                                                            */
/******************************************************************************/

int lis_type_file(char *dir_name,char *type,char ( *tabfile)[][250]);

/******************************************************************************/
/*                                                                            */
/*   But: extraire la racine d'un nom de fichier d'un char[][]                */
/*        en la recopiant dans une chaîne de caractères.                      */
/*                                                                            */
/*        le résultat est mis dans char *name                                 */
/*                                                                            */
/*   Les noms de fichiers ont 250 caractères au maximum.                      */
/*                                                                            */
/*   Exemple : extract_name(fichlis,ind,name);                                */
/*                                                                            */
/*	char (*file_lis)[][250];				              */
/*	file_lis=(char (*)[][250]) calloc(nbre_result*250,sizeof(char));      */
/*      extract_name(file_lis,i,name)                                         */
/*                                                                            */
/******************************************************************************/

int extract_name ( char ( *tabfile)[][250] , int indic , char *name );

/******************************************************************************
 *
 * Déterminer si un nom passé en argument correspond à un répertoire 
 * accessible ou à un fichier.
 *
 * renvoie 'r' si répertoire
 * renvoie 'f' si fichier
 * renvoie -1 si inaccessible
 * 
 * Michel Vacher - 5 avril 2002.
 * 
 * ***************************************************************************/
int is_rep_valid(char * name) ;

/******************************************************************************
 *
 * Extraire le nom d'un fichier à partir d'une chaîne de caractère contenant
 * une aborescence se terminant par le nom du fichier. 
 * accessible ou à un fichier.
 *
 * char *sin	: chaîne de départ
 * char *name	: nom de fichier extrait
 * char type	: type de séparateur entre élément de l'arborescence
 * 	u ou x	pour unix ("/")
 * 	d ou w	pour windows ("\")
 * 	
 * renvoie -1 si erreur
 * 
 * Michel Vacher - 15 mai 2002.
 * 
 * ***************************************************************************/
int search_name_file(char *sin, char *name, char type );


/*
     Copyright (C) 2012-2016 Laboratoire d'Informatique de Grenoble (www.liglab.fr),
     Centre National de la Recherche Scientifique (www.cnrs.fr).
     The initial authors of the original code are Michel Vacher, François Portet 
     and Frédéric Aman.
     Contributors: Michel Vacher, François Portet, Frédéric Aman, Fabien Eloy, 
     William Duclot, Cheick Mahady Sissoko, Dan Istrate, Hubert Glasson, Stéphane 
     Chaillol and Alain Dufaux.
     
     This file is part of CirdoX.

     CirdoX is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     CirdoX is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CirdoX. If not, see <http://www.gnu.org/licenses/>.

     CirdoX is a software project initiated by the French Centre National 
     de la Recherche Scientifique (CNRS) and the Laboratoire d'Informatique de 
     Grenoble  (LIG) - France (https://www.liglab.fr/).
     This work was supported by the French funding agencies ANR and CNSA through 
     the CIRDO project (ANR-2010-TECS-012).
     CirdoX is a modular software able to analyse online the audio environment, 
     to extract the uttered sentences and then to process them thanks to an ASR stage 
     followed by a filtering stage.

     Contact Frédéric Aman <frederic.aman@gmail.com>, Michel Vacher
     <michel.vacher@imag.fr> or François Portet <francois.portet@imag.fr>
     for more information about the licence or the use of CirdoX.
*/

#include "plugins_manager.h"
#include "acquisition_multi_channels.h"//-------------------------------
#include "config.h"

#ifndef _CIRDOX_H_
#define _CIRDOX_H_

#define SAMPLE_RATE 16000
#define FRAMES_PER_BUFFER 2048

//pour gerer les fichiers
typedef struct{
	char name_wav[200];
	FILE *file_wav;
}file_channel;

// typedef enum BOOLEAN { false, true };

// BOOLEAN bool;

typedef struct{
	bool detector;
	bool discriminator;
	bool classificator;
	bool filter;
	bool asr;
	bool formatting;
}plugin_activated;

/*!
     \brief plugins: Defines the list of plugins that are used to process the data
    */
extern plugin_list plugins;
static configuration_cirdox *Cfg_cirdox;
static int pipefd[2];
static int pipestart[2]; //to synchronize the two process
static plugin_activated plugin_state;

void quit_cirdox_father();
void quit_cirdox_shared();
void print_duration();
void compute_max_peak(const SAMPLE **data);
void print_vu_meter(int *perc, int *maxperc);

#endif

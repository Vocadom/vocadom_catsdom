#include "NLU.hpp"
#include "tcpConnection.hpp"
#include "config.hpp"
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
//librairie pour le test
#include <iostream>
#include <fstream>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

#define CHUNK 8000 // correspond to 0.5 seconds

std::string inputFolder;
std::string outputFolder;
std::string creaLog;
const std::string file = "VocadomConf.xml";
static std::string scriptFile;
static std::string curlCommandNLU = "script/curlCommandNLU.sh";// TODO trouver une solution pour ajout à VocadomConf.xml ==> savoir command lister plusieurs script dans un module
//for NLU
static int sock_nlu;
static int port_nlu;
static std::string adress_nlu;

//for rap server
static int sock_rap;
static int port_rap;
static std::string adress_rap;

//for recoloc server
static int sock_recoloc;
static int port_recoloc;
static std::string adress_recoloc;

std::string getTime()
{
  time_t curr_time;
  tm * curr_tm;
  char time_string[100];
  time(&curr_time);
  curr_tm = localtime(&curr_time);	
  std::strftime(time_string, 50, "%Y-%m-%d_%H-%M-%S", curr_tm);

  return std::string(time_string);
}

/* ----------------------------------------------------------------------
    Thread : Connection to recoLoc
   ---------------------------------------------------------------------- */
void* server_nlu(void *)
{
    char buffer[CHUNK];
    bool connection = false;
    std::string rapFileName = inputFolder + "/Commande.txt";

    std::string outputFile = outputFolder + "/versOpenHab.out";
    std::string logFile = outputFolder + "/logNLU.txt";


    std::fstream fifoNLUIn, fifoNLUOut;
    system("rm -f fifoNLU* >> null");
    while(1)
    {
     
      if(fs::is_fifo("fifoNLUIn") )
      {
	fifoNLUIn.open("fifoNLUIn", std::ios::in);
	std::string tmpS;
	std::cout << "opening" << std::endl;
	getline(fifoNLUIn,tmpS);
	std::cout << "line " << tmpS << std::endl;
	if(fs::is_fifo("fifoNLUOut"))
	   fifoNLUOut.open("fifoNLUOut", std::ios::out);
	break;
      }
      else
      {

	sleep(1);
      }
    }

    while(1)
    { 
        int sock_recoloc = accept(sock_nlu, NULL, NULL);
	
        if(sock_recoloc == -1)
	{
	    perror("sock recoloc");
	    pthread_exit(NULL);	
	}
        
	connection = true;

	while(connection)
	{
	    int nb_read = 0;
	    memset(buffer, 0x00, CHUNK);
	    while(nb_read < CHUNK)
	    {
		
		int ret = read(sock_recoloc, buffer + nb_read, CHUNK - nb_read);
		
		if(ret <= 0)
		{
		    std::cout << "RecoLoc disconnected" << std::endl;
		    connection = false;
		    break;
		}
		nb_read += ret;
	    }   
	}
	
	std::cout << "buffer is " << std::string(buffer) << std::endl;

	boost::filesystem::ofstream rapFile{rapFileName};
	rapFile << buffer;
	rapFile.close();

	// Resetting output folder 
	if(!boost::filesystem::exists(outputFolder))                           
	  {
	    boost::filesystem::create_directory(outputFolder);
	  }
	boost::filesystem::ofstream currentOutFile{outputFile};
	// TEST CODE TO HANDLE THE FACT THAT RAP IS TOO SLOW IN WRITING
	std::cout << "start command nlu" << std::endl;
	fifoNLUOut << "start" << std::endl;
	std::string fifoLine;
        getline(fifoNLUIn, fifoLine);
	std::cout<<"stop commande nlu" <<std::endl;
    	
	//Save the NLU result in a string to add in log and send if in OpenHab 
	std::string command_line;
	  
	std::string line;
	boost::filesystem::ifstream ifile (outputFile);
	if ( ifile.is_open())
	  {
	    /*outputFile has 1 line getline read the file line by line to save and 
	      replace the previous line by the next.
	      We save the first line in another variable*/ 
	    while (getline (ifile, command_line))
	      {
		line = command_line;
	      }
	    ifile.close();
	    std::cout<<"NLU output: "<<line<<std::endl;
	  }  			
	//log NLU
	std::ofstream file_out;
	file_out.open(logFile, std::ios_base::app);
	file_out << getTime();
	file_out << " "+ line <<std::endl;
	//std::cout<<"log NLU output: "<<line<<std::endl;
	// Send NLU result to OpenHab using REST API
	std::ifstream ifs(curlCommandNLU.c_str());
	std::string curlCommandAndData;
	curlCommandAndData.assign((std::istreambuf_iterator<char>(ifs)),
				  (std::istreambuf_iterator<char>()));
	curlCommandAndData = curlCommandAndData.substr(0, curlCommandAndData.size()-1);
	curlCommandAndData = curlCommandAndData + "\"" + line + "\"" ;

	if(system(NULL))
	  system(curlCommandAndData.c_str());
			
	  
    }
}



int NLU_init()
{
    if(GetPort(file, MACHINE, "PortNLU", port_nlu) != 1)
    {
        std::cout <<"Failed to get port of NLU " << MACHINE << std::endl;
        return -1;
    }

    if(GetAdress(file, MACHINE, "AdressNLU", adress_nlu) != 1)
    {
        std::cout <<"Failed to get adress of NLU " << std::endl;
    }
    
//connexion RAP
    if(GetPort(file, MACHINE, "PortRAP", port_rap) != 1)
    {
        std::cout <<"Failed to get port of RAP " << MACHINE << std::endl;
        return -1;
    }

    if(GetAdress(file, MACHINE, "AdressRAP", adress_rap) != 1)
    {
	std::cout << "Failed to get adress of RAP" << std::endl;
    }
    
//connexion RecoLoc
    if(GetPort(file, MACHINE, "PortRecoLocution", port_recoloc) != 1)
    {
        std::cout <<"Failed to get port of RecoLocution " << port_recoloc << MACHINE << std::endl;
        return -1;
    }

    if(GetAdress(file, MACHINE, "AdressRecoLocution", adress_recoloc) != 1)
    {
	std::cout << "Failed to get adress of RecoLocution" << std::endl;
    }
//script NLU
    if(GetScriptKaldi(file, MACHINE, scriptFile) != 1)
    {
        scriptFile= SCRIPT_FILE;
        std::cout << "Failed to find scriptFile : " <<scriptFile<< std::endl;
    }
    /*if(GetScriptNLU(file, MACHINE, scriptFile) != 1)
    {
        scriptFile= SCRIPT_FILE;
        std::cout << "Failed to find scriptFile : " <<scriptFile<< std::endl;
    }*/
//connexion input output files
    if(GetConfig(file, MACHINE, inputFolder, outputFolder) == -1)
		return -1;  
    return 1;
}

void* runScript(void *)
{
  if(system(NULL))
  {
    system(scriptFile.c_str());
  }
  return NULL;
}

int NLU_preprocess()
{
    std::cout << "preprocess" << std::endl;
    sock_nlu = create_tcp_server(port_nlu, adress_nlu);
    std::cout << "port nlu "<<port_nlu <<" adress nlu "<<adress_nlu<< std::endl;
    if(sock_nlu < 0)
    {
	return -1;
    }
    return 1;
}

int NLU_process()
{

    
    pthread_t thread_server_nlu;
    int new_thread_server_nlu;
    pthread_t thread_script;
    int new_thread_script;
    
    pthread_t thread_server_rl; 
    int new_thread_server_rl; 

    new_thread_script = pthread_create( &thread_script , NULL , runScript, NULL); 
    if (new_thread_script)
    {
	std::cout << "thread script failed" << std::endl;
	return -1;
    }
    
    //partie connexion serveur RAP 
    new_thread_server_nlu = pthread_create( &thread_server_nlu , NULL ,  server_nlu, NULL); 
    if (new_thread_server_nlu)
    {
	std::cout << "thread client to stamp stream failed" << std::endl;
	return -1;
    }
    
    if (pthread_join(thread_server_nlu, NULL)) 
    {
	perror("pthread_join");
	return EXIT_FAILURE;
    }

    return 1;
}

int NLU_postprocess()
{
    std::cout <<"dans le postprocess" << std::endl; 
    return 1;
}
int NLU_finish()
{
    return 1;
}

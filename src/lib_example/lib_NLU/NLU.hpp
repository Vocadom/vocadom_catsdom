#ifndef NLU_HPP
#define NLU_HPP

#include <iostream>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#define MACHINE "NLU" // Module's name
#define SCRIPT_FILE "script/GENERATE_PREDICTIONS.sh"
/* ====================
	Prototypes
   ==================== */ 
extern "C" int NLU_init();
extern "C" int NLU_preprocess();
extern "C" int NLU_process();
extern "C" int NLU_postprocess();
extern "C" int NLU_finish();

#endif 

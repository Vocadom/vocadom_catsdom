#!/bin/bash

cp src/lib_example/lib_VAD/script/*\.py bin/script/
cp src/lib_example/lib_VAD/script/*\.npy bin/
cp src/lib_example/lib_VAD/script/model.pth bin/
cp -r src/lib_example/lib_VAD/script/Torch_Models bin/script/

var=$(conda env list)

if [[ $var =~ .*"SinaVAD".* ]]; then
    echo "SinaVAD already exist"
else
    echo "create environnement SinaVAD"
    conda env create -f src/lib_example/lib_VAD/script/environment.yml
fi

echo "before running VAD you must activate SinaVAD environnement"

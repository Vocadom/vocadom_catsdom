/*
 * @Author: Labadens Lucas
 * @Date: 2020-08-06
 */

#ifndef VAD_HPP
#define VAD_HPP

#define MACHINE       "VAD"	                // lib's name
#define PY_VAD_SCRIPT "script/TimesSox.py"      //End voice detection script
#define PY_KILL_SERV  "script/killVADServer.py" // Kill server if socket
                                                // won't quit properly

struct VoiceData
{
    std::string fileName;
    double voiceActivity;
    double cutTime;
};


/* ====================
	Prototypes
   ==================== */ 
extern "C" int VAD_init();
extern "C" int VAD_preprocess();
extern "C" int VAD_process();
extern "C" int VAD_postprocess();
extern "C" int VAD_finish();

#endif // VAD_HPP

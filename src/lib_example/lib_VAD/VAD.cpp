/*
 * @Author: Labadens Lucas 
 * @Date: 2020-08-06
 */

#include "Wrapper.hpp"
#include "config.hpp"
#include "tcpConnection.hpp"
#include "decodeEvent.hpp"
#include "regex"
#include "VAD.hpp"

std::string file = "VocadomConf.xml";                               

static int sock_event_manager;// socket connected to event manager
static int port_event_manager;// port of event manager
static std::string adress_event_manager; // adress of event manager
static int port_vad;// port use by python script
static std::string adress_vad;// adress use by python script
static int port_rap;// port use by python script to send to rap
static std::string adress_rap;// adress use by python script to send to rap
static int port_recoLocution;// port use by python script to send to recoLocution
static std::string adress_recoLocution;// adress use by python script to send to recoLocution

/**
 * @brief processReturnVAD Analyse stdout value of VAD to get VoiceData value
 * @param in stdout value
 * @param d VoiceData to fill
 * @return error value
 */
int processReturnVAD(std::string in,VoiceData &d)
{
    double  timeValues[2];
    int     count=0;
  
    std::regex doubleR("([0-9]+(\\.|,)?[0-9]+)s"); // find second information in text
    std::regex wavFile("[^\\s]+\\.wav");// find wav file in text
    std::smatch match;

    if(std::regex_search(in,match,wavFile))// if a wav file is found and save in match
    {
	std::regex_token_iterator<std::string::iterator> it(in.begin(),in.end(),doubleR,1); //find all iteration of a second (must be two)
	std::regex_token_iterator<std::string::iterator> end;
  
	if(it != end)
	{
	    for(;it != end;*it++)
	    {
		timeValues[count] =  std::stod(*it);
		count++;
	    }
	    d = {match[0],timeValues[0],timeValues[1]}; // save information in d
	    return 0;
	}
    }
    return -1;
}

/**
 * @brief send_vad_event send end of file to EventManager
 * @param d Voice Data to create event
 * @return
 */
int send_vad_event(VoiceData d)
{
    char message[100];
    sprintf(message, "Ef %.2f %.2f", d.voiceActivity , d.cutTime);
//try to dd a curl command to OH
    return  write(sock_event_manager, message, strlen(message));
}

/**
 * @brief runVAD run python module VAD
 * @param d return value to fill
 * @return error value
 */
int runVAD(VoiceData &d)
{
    std::list<std::string> args({adress_vad, std::to_string(port_vad),adress_rap,std::to_string(port_rap),adress_recoLocution,std::to_string(port_recoLocution)});
    return processReturnVAD(runPyModule(PY_VAD_SCRIPT, args), d);
}

/**
 * @brief printVoiceData print Voice Data
 * @param d Voice Data to print
 */
void printVoiceData(VoiceData d)
{
    std::cout << "file is " << d.fileName << "\ncut time is " << d.cutTime
	      << " voice start at\n" << d.voiceActivity << std::endl;
}

int VAD_init()
{
    if(GetPort(file, MACHINE, "PortEventManager", port_event_manager) != 1)
    {
	std::cout <<"Failed to get port of EventManager " << MACHINE << std::endl;
	return -1;
    }

    if(GetAdress(file, MACHINE, "AdressEventManager", adress_event_manager) != 1)
    {
	std::cout <<"Failed to get port of EventManager " << MACHINE << std::endl;
	return -1;
    }

    if(GetPort(file, MACHINE, "PortVAD", port_vad) != 1)
    {
	std::cout <<"Failed to get port of VAD " << MACHINE << std::endl;
	return -1;
    }

    if(GetAdress(file, MACHINE, "AdressVAD", adress_vad) != 1)
    {
	std::cout <<"Failed to get adress of VAD " << MACHINE << std::endl;
	return -1;
    }
     if(GetPort(file, MACHINE, "PortRAP", port_rap) != 1)
    {
	std::cout <<"Failed to get port of RAP " << MACHINE << std::endl;
	return -1;
    }

    if(GetAdress(file, MACHINE, "AdressRAP", adress_rap) != 1)
    {
	std::cout <<"Failed to get adress of RAP " << MACHINE << std::endl;
	return -1;
    }
    if(GetPort(file, MACHINE, "PortRecoLocution", port_recoLocution) != 1)
    {
	std::cout <<"Failed to get port of adress_recoLocution " << MACHINE << std::endl;
	return -1;
    }

    if(GetAdress(file, MACHINE, "AdressRecoLocution", adress_recoLocution) != 1)
    {
	std::cout <<"Failed to get adress of RAP " << MACHINE << std::endl;
	return -1;
    }

    return 1;
}

int VAD_preprocess()
{
    uint8_t ACK;
    char message[50];
    Event evt;

    sock_event_manager = connect_tcp_client(port_event_manager, adress_event_manager); // connect to event manager
    if(sock_event_manager < 0)
    {
	std::cout <<"Failed to connect to Event Manger " << MACHINE << std::endl;
	return -1;
    }

    // sending module ID (VAD) to the EventManager
    if(0 > write(sock_event_manager, MACHINE, sizeof(MACHINE)))
    {
	std::cout <<"Failed to send ID " << MACHINE << std::endl;
	return -1;
    }

    int ret = read(sock_event_manager, &ACK, sizeof(ACK));
    if(0 > ret || 0 != ACK )
    {
	std::cout <<"Failed to receive ACK from EventManager" << std::endl;
	return -1;
    }

    std::cout <<"Subscribe to EventManager SUCCESS" << std::endl;

    return 1;
}

int VAD_process()
{
    VoiceData current_data;
    while(1)
    {
	// run VAD script which is a blocking function
	// return when audio datas are send on VAD port
	// and end of audio is detected
	int ret =  runVAD(current_data);

	if(ret != -1)
	{
	    std::cout << "End Voice found" << std::endl;
	    send_vad_event(current_data);
	}
	else
	{
	    std::cout << "python script failed" << std::endl;
	    send_vad_event(VoiceData{"0",-1,-1});
	}
	runPyModule(PY_KILL_SERV);
    }
    return 1;
}

int VAD_postprocess()
{
    return 1;
}

int VAD_finish()
{
    std::cout << "vad finish" << std::endl;
    return 1;
}


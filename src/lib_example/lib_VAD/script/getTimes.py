'''
Get times of VAD for a file
'''
import torch
import torch.nn as nn
from torch.autograd import Variable
from shennong.audio import Audio
from shennong.features.processor.rastaplp import RastaPlpProcessor
import numpy as np
from model import FeatureModel

def main():
    path = "./test.wav"
    times = timesFromFile(path)
    print(times)

def timesFromFile(path):
    audio = Audio.load(path) # Loading audio file
    times = timesFromAudio(audio)
    return times
    
def timesFromAudio(audio):
    out = VadFromAudio(audio)
    times = getTimes(out)# Get the start and end times of each detected speech segment
    return times

def VadFromAudio(audio, smoothedWin=50):
    model = torch.load("model.pth", map_location="cpu") # Loading model into cpu
    processor = RastaPlpProcessor(order=8, sample_rate=audio.sample_rate)
    features = processor.process(audio)# Extracting features
    feats = features.data
    mean = np.load('mean.npy')
    std = np.load('std.npy')
    feats = (feats - mean) / std
    # for i in range(feats.shape[1]): # Standardizing features
    #     feats[:,i] = (feats[:,i] - mean) / std
    feats = torch.FloatTensor(feats)
    feats = Variable(feats).unsqueeze(0)
    out = model(feats)# Getting the output of the model based on input features
    out = out[:,:,:].view(out.size()[0]*out.size()[1]*out.size()[2])
    out = out.cpu().data.numpy()
    if smoothedWin>0: out = smooth(out, win=smoothedWin)# Smoothing the output of the model with a window of 50 samples (here it means 0.5s based on feature extraction process)
    return out

def getTimes(out, fs=0.01):
    ins = []
    outs = []
    last = 0
    for i, o in enumerate(out):
        if o == 1 and last != 1: ins.append(i)
        if o == -1 and last == 1: outs.append(i)
        last = o
    if out[-1] == 1: outs.append(len(out)-1)
    times = []
    for i, _ in enumerate(outs):
        times.append([round(ins[i]*fs,3), round(outs[i]*fs,3)])
    return times

def smooth(sig, win=25*1):
    import numpy as np
    mysig = sig.copy()
    aux = int(win/2)
    for i in range(aux, len(mysig)-aux):
        value = np.mean(sig[i-aux:i+aux])
        mysig[i] = 1 if value > 0 else -1
#        with open("log.txt", "a") as myfile:
#            print(value)
#            myfile.write(str(value))
    mysig[:aux] = 1 if np.mean(sig[:aux]) > 0 else -1
    mysig[-aux:] = 1 if np.mean(sig[-aux:]) > 0 else -1
    return mysig

if __name__ == "__main__":
    main()

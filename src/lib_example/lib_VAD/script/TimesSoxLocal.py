'''
Get times of VAD from a file sent on a socket connection
'''
from VAD_Module_online import VAD_Module
import numpy as np
import socket
import os
from scipy.io.wavfile import write, read

def main():
    HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
    PORT = 50000        # Port to listen on (non-privileged ports are > 1023)

    RATE = 16000
    chunk = int(RATE/2) # Receive data from client in chunks (exp. RATE/2 means 0.5s)

    meanPath = 'mean.npy'
    stdPath  = 'std.npy'

    smoothedWin=0.75 # in Seconds - Ignores detected voice less than this number
    mergeWin=0.5 # in Seconds - Merges detected voice frames that are apart from each other by this number 
    hysteresis_bottom=0.1 # Amplitude (between -1 and +1) - starts cutting detected voice if vad output for new audio frame goes below this number
    hysteresis_top=0.8 # Amplitude (between -1 and +1) - starts counting voice as detected if vad output for new audio frame goes above this number
    vadModule = VAD_Module(smoothedWin=smoothedWin, mergeWin=mergeWin, device="cpu", hysteresis_bottom=hysteresis_bottom, hysteresis_top=hysteresis_top, modelPath="./Models/Recola_46_MFB_standardized_LinTanh/model.pth")

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
        conn, addr = s.accept()
        DataFromStart = np.array([]) # Saving received data from the start of data transfer for better performance of VAD
        with conn:
            print('Connected by', addr)
            VoiceDetected = False
            EndOfVoice = False
            i=0
            while True:
                data = conn.recv(chunk*2)
                i+=1
                last_index = np.dtype(np.int16).itemsize * ((len(data)//np.dtype(np.int16).itemsize))
                npdata = np.frombuffer(data[:last_index],dtype=np.int16)
                # npdata = np.frombuffer(data,dtype=np.int16)
                # print(allData.shape, npdata.shape)
                DataFromStart = npdata if len(DataFromStart) == 0 else np.concatenate((DataFromStart, npdata), axis=0)
                # print("DataFromStart", DataFromStart)
                # audio = Audio(DataFromStart, RATE, validate=True)
                # vad = vadModule.VadFromAudio(DataFromStart, meanPath=meanPath, stdPath=stdPath)

                try:
                    # audio = Audio(DataFromStart, RATE, validate=True)
                    vad = vadModule.VadFromAudio(DataFromStart, meanPath=meanPath, stdPath=stdPath)
                    noVoice = np.where(vad==-1)[0]
                    # print(i,vad)
                    if len(noVoice) == 0: VoiceDetected = True
                    if len(noVoice) > 0 and VoiceDetected: 
                        print("End of Voice Activity Detected at", str(noVoice[0]/100)+"s")
                        EndOfVoice = True
                        break
                except:
                    pass

                if not data:
                    print(i, "connection terminated.")
                    break
                # conn.sendall(data)

            if EndOfVoice:
                path = os.path.join("VoicedOnly.wav")
                print("Saving file to", path, "Original file cut at", str(round(i*chunk/RATE,2))+"s")
                write(path, RATE, DataFromStart)

if __name__ == "__main__":
    main()

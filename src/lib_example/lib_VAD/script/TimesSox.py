'''
Get times of VAD from a file sent on a socket connection
'''
import numpy as np
from model import FeatureModel
from VAD_Module_online import VAD_Module
import socket
import os
from math   import ceil
from Stamp import getStampData, getUnstampData
from datetime import datetime
from scipy.io.wavfile import write, read

from logWrapper import init_log, close_log, Print

from sys import argv

def main():
    if(len(argv) < 7):
        HOST_SERVER = '127.0.0.1'  # Standard loopback interface address (localhost)
        PORT_SERVER = 50000        # Port to listen on (non-privileged ports are > 1023)
        HOST_RAP = '127.0.0.1'
        PORT_RAP = 17000
        HOST_RECOLOC = '127.0.0.1'
        PORT_RECOLOC = 15000
    else:
        HOST_SERVER = argv[1]
        PORT_SERVER = int(argv[2])
        HOST_RAP = argv[3]
        PORT_RAP = int(argv[4])
        HOST_RECOLOC = argv[5]
        PORT_RECOLOC = int(argv[6])

    saveFolder    = "../Data/Out_VAD/log"
    now = datetime.now()
    time = now.strftime("%Y-%m-%d_%H-%M-%S")
    if not os.path.exists(saveFolder): os.makedirs(saveFolder)
    RATE          = 16000
    BytePerSample = 2

    StampPerSec = 100
    SizeOfStamp = 8

    maxDuringTime = 5 #Maximum duration of an order in second
    minDuringTime = 2 #Minimum duration of an order in second

    BytesPerSec = RATE*BytePerSample + StampPerSec*SizeOfStamp #Audio Rate bytes plus size of stamps
    chunk       = int(BytesPerSec/2) # Receive data from client in chunks (exp. BytePerSec/2 means 0.5s)
    maxChunk    = (BytesPerSec/chunk)*maxDuringTime
    minChunk    = (BytesPerSec/chunk)*minDuringTime
    
    meanPath = 'mean.npy'
    stdPath  = 'std.npy'

    smoothedWin=0.75 # in Seconds - Ignores detected voice less than this number
    mergeWin=0.5 # in Seconds - Merges detected voice frames that are apart from each other by this number 
    hysteresis_bottom=0.1 # Amplitude (between -1 and +1) - starts cutting detected voice if vad output for new audio frame goes below this number
    hysteresis_top=0.8 # Amplitude (between -1 and +1) - starts counting voice as detected if vad output for new audio frame goes above this number
    vadModule = VAD_Module(smoothedWin=smoothedWin, mergeWin=mergeWin, device="cpu", hysteresis_bottom=hysteresis_bottom, hysteresis_top=hysteresis_top, modelPath="../src/lib_example/lib_VAD/script/Models/Recola_46_MFB_standardized_LinTanh/model.pth")

    init_log()

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        for i in range(5):
            try:
                s.bind((HOST_SERVER, PORT_SERVER + i ))
                s.listen()
                break
            except:
                Print('socket as not been killed')
                if i == 4:
                    exit -1

        conn, addr = s.accept()
        DataFromStart = np.array([]) # Saving received data from the start of data transfer for better performance of VAD

        with conn:
            Print('Connected by', addr)

            VoiceDetected = False
            EndOfVoice   =  False
            isFirstStamp =  True

            nbChunkRcv = 0
            last       = b""
            stampChar  = 0x7fff
            firstStamp = 0x00

            while True:
                try:
                    dataRcv = conn.recv(chunk)
                    data = last  + dataRcv
                except socket.timeout:
                    return

                if not data:
                    Print(nbChunkRcv, "connection terminated.")
                    break

                if len(data) < chunk:
                    last = data
                    continue
                elif len(data) > chunk:
                    last  = data[chunk: ]
                    data  = data[:chunk]

                else:
                    last = b""

                nbChunkRcv += 1

                #get Data without stamp and get the first value of the first data stamp
                unstampData,stamp = getUnstampData(data,stampChar,1)

                #if it's the first of all stamp save it
                if isFirstStamp:
                    firstStamp  = stamp
                    isFirstStamp = False

                npdata = np.frombuffer(unstampData,dtype=np.int16)

                DataFromStart = npdata if len(DataFromStart) == 0 else np.concatenate((DataFromStart, npdata), axis=0)

                if maxChunk == nbChunkRcv:
                        print("End of Voice Activity Detected at", str(maxDuringTime) + "s")
                        EndOfVoice = True
                        break
                #print(vadModule.VadFromAudio(DataFromStart, meanPath=meanPath, stdPath=stdPath))
                #np.savetxt("/home/courlam/Domus/Dev/Final/catsodom/Data/Out_VAD/log/log1.csv", np.array([[1,2,3]]), delimiter=",")
                #vad = vadModule.VadFromAudio(DataFromStart, meanPath=meanPath, stdPath=stdPath)
                
                try:
                    #audio = Audio(DataFromStart, RATE, validate=True)
                    #vad = VadFromAudio(audio, smoothedWin=smoothedWin)
                    vad = vadModule.VadFromAudio(DataFromStart, meanPath=meanPath, stdPath=stdPath)
                    np.savetxt("../Data/Out_VAD/log/log_vad.csv", vad, delimiter=",")
                    noVoice = np.where(vad==-1)[0]

                    if len(noVoice) == 0: VoiceDetected = True
                    if len(noVoice) > 0 and VoiceDetected and (nbChunkRcv > minChunk):
                        print("End of Voice Activity Detected at",str(noVoice[0]/100)+"s")
                        EndOfVoice = True
                        break
                except:
                    pass

                # conn.sendall(data)

            if EndOfVoice:
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s2:
                    s2.connect((HOST_RAP,PORT_RAP))
                    s2.sendall(DataFromStart)
#                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s3:
#                    s3.connect((HOST_RECOLOC,PORT_RECOLOC))
#                    s3.sendall(DataFromStart)
                path = os.path.join("../Data/Out_VAD/VoicedOnly.wav")
                print("Saving file to", path, "Original file cut at", str(round(nbChunkRcv*chunk/BytesPerSec,2))+"s")
                write(path, RATE, DataFromStart)
                write(os.path.join(saveFolder,time+"_logVAD.wav"), RATE, DataFromStart)
            close_log()

if __name__ == "__main__":
    main()

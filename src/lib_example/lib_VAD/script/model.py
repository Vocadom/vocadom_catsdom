import torch
import torch.nn as nn

class FeatureModel(nn.Module):
    def __init__(self, featSize, hidden_size = 32, device="cuda"):
        super(FeatureModel, self).__init__()
        self.featSize = featSize
        self.device = device
        self.hidden_size = hidden_size
        self.rnn = nn.GRU(
            input_size=featSize,
            hidden_size=self.hidden_size,
            # bidirectional=True,
            num_layers=2,
            batch_first=True)
        # self.out = nn.Linear(128, 1)
        self.out = nn.Sequential(
            nn.Linear(self.hidden_size, 1),
            nn.Tanh(),
        )

    def forward(self, x):
        batch_size, timesteps, sq_len = x.size()
        # x = self.sinc(x)
        # x = x.view(batch_size, timesteps, sq_len)
        output, _ = self.rnn(x)
        output = self.out(output)
        # output = output.view(batch_size, 1, timesteps, 1)

        return output
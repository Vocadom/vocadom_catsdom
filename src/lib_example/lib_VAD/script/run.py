## VAD related imports
from VAD_Module import VAD_Module
# from Models import FeatureModel

## Segmenter import
from Segmenter import Segmenter

## Other imports
import os, glob, argparse


def main(wavsFolder):
    path = os.path.join(wavsFolder, "*.wav")
    theFiles = glob.glob(path, recursive=True)

    for filePath in theFiles:
        vad = VAD_Module(smoothedWin=0.75, mergeWin=0.5, device="cpu", hysteresis_bottom=-0.8, hysteresis_top=0, modelPath="./Models/Recola_46_MFB_standardized_LinTanh/model.pth")
        times = vad.timesFromFile(filePath)
        print("VAD is done for", filePath)

        segmenter = Segmenter(savePath='./segments')
        segmenter.segmentFile(times, filePath)
        print("Segmentation is done for", filePath)
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--wavsFolder', '-i', help="path to input folder containing wav files", default="") # e.g. "./WavFiles"
    
    args = parser.parse_args()
    Flag = False
    if args.wavsFolder == "": Flag = True
    if Flag:
        parser.print_help()
    else:
        main(args.wavsFolder)
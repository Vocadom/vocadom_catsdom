import os, signal
import fcntl
from logWrapper import init_log, Print

init_log()


def check_pid(pid):
    if os.path.isdir('/proc/{}'.format(pid)):
        return True
    return False

for i in range(50000,50005):
    os.system("lsof -t -i tcp:" + str(i) + " -s tcp:listen > tmp.txt")

    f = open("tmp.txt","r")

    fd = f.fileno()

    flag = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, flag | os.O_NONBLOCK)
    flag = fcntl.fcntl(fd, fcntl.F_GETFL)

    r = f.read()

    try:
        pid = int(r)
        if check_pid(pid):
            os.kill(pid, signal.SIGKILL)
    except ValueError:
        pass

    f.close()
    os.unlink("tmp.txt")

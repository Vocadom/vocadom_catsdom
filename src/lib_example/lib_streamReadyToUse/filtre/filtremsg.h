
#ifndef FILTREMSG_H
#define FILTREMSG_H

#define STRLEN 2048
#define STRLIGNE 50000
#define MAXPHN 1000
#define MAXWUPHN 15

struct MotReco
{
        double debut;
        double fin;
        double phnTime[MAXWUPHN];
        char  *phnValue[MAXWUPHN];
        char   name[10];
};

/* ====================
	Prototypes
   ==================== */ 
int getnumphn(char **tabphn, char *phn, int nb_phn);
int extraire_un_mot(char *ligne, char *mot, int indice);
int * lire_pdf(const char *nom_fich, char** table_phn, int *nb_phn, int *nbpdf);
bool filtremsg(const char *pdfFile, const char* motCles, char* buffer, MotReco *output, int firstStamp);
std::string msgToString(MotReco msg, int micro);

#endif 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include <string>

#include "filtremsg.h"

#define STRLEN 2048
#define STRLIGNE 50000
#define MAXPHN 1000

/*************************************/
int getnumphn(char **tabphn, char *phn, int nb_phn)
/*************************************/
{
  int i;
  for(i=0;i<nb_phn; i++) {
    if(strcmp(phn,tabphn[i])==0) return(i);
  }
  return(-1);
}

/*************************************/
int extraire_un_mot(char *ligne, char *mot, int indice)
/*************************************/
{
  int deb,i;
  if(ligne[indice]=='\0') return(-1);
  if(ligne[indice]=='\n') return(-1);

  deb=indice;
  while(ligne[deb]==' ') deb++;
  if(ligne[deb]=='\0') return(-1);
  if(ligne[deb]=='\n') return(-1);

  indice=deb;
  while( (ligne[indice]!='\0') &&
         (ligne[indice]!='\n') &&
         (ligne[indice]!=' ')     ) indice++;
  for(i=deb;i<indice;i++) mot[i-deb]=ligne[i];
  mot[indice-deb]='\0';
  return(indice);
}

/****************************************/
int * lire_pdf(const char *nom_fich, char** table_phn, int *nb_phn, int *nbpdf)
/****************************************/
{
  int n,derpdf;
  FILE *fich_pdf;
  int *table_pdf;
  int numphn;
  char phn[STRLEN];

  *nb_phn = 0;
  fich_pdf=fopen(nom_fich,"r");
  // printf(" Lecture du fichier %s \n", nom_fich);

  if (fich_pdf==NULL) {
    printf("unable to open %s\n",nom_fich);
    exit(0);
  }

  if (fscanf(fich_pdf,"%d",&derpdf)!=1) {
    fprintf(stderr,"manque derpdf\n");
    exit(0);
  }

  *nbpdf = derpdf+1;
  // printf("nbpdf %d\n",*nbpdf);
  table_pdf = (int *) malloc(sizeof(int) * *nbpdf);
  while(fscanf(fich_pdf,"%d %s",&n,phn)==2) {
    if(n>=*nbpdf) {
      fprintf(stderr,"out of range pdf %d %d\n", n, *nbpdf);
      exit(0);
    }
    numphn = getnumphn(table_phn, phn, *nb_phn);
    if(numphn == -1) {
      if(*nb_phn >= MAXPHN) {
	fprintf(stderr,"out of range phn %d %d\n", *nb_phn, MAXPHN);
	
      } else {
	table_phn[*nb_phn] = (char *) malloc(sizeof(char) * (strlen(phn)+1) );
	strcpy(table_phn[*nb_phn], phn);
	numphn = *nb_phn;
	*nb_phn = *nb_phn + 1;
      }
    }
    table_pdf[n]= numphn;
    }

  fclose(fich_pdf);
  return table_pdf;
}

/*******************************************/
bool filtremsg(const char *pdfFile, const char* wuw, char *buffer, MotReco *output,int firstStamp)
{
  int nb_pdf;
  int nb_phn;
  int ok, okphn;
  int *table_pdf;
  char **table_phn;
  char pronom[256];
  char tt[256];
  char ligne[STRLIGNE];
  //char buffer[STRLIGNE];
  char mot[STRLEN];
  char phonemes[STRLEN];
  int numpred;
  char **wuwphn;
  int nb_wuwphn = 0;
  int indice;
  //char *wuw;
  double t;
  int j, k, s;
  double deb[MAXPHN];
  double fin[MAXPHN];
  int ph[MAXPHN];
  int ind_deb;
  int ind_fin;
  double debdeb;
  double finfin;
  int tr;
  bool mot_trouve = false;
  

  table_phn = (char **) malloc(sizeof(char *) * MAXPHN);
  table_pdf = lire_pdf(pdfFile, table_phn, &nb_phn, &nb_pdf);

  //wuw =(char *) malloc(sizeof(motCles));
  //memcpy(wuw, motCles, sizeof(motCles));
  strcpy(pronom, "");

  if(strcmp(wuw,"minouche")==0) {
    nb_wuwphn = 5;
    wuwphn = (char **) malloc(sizeof(char *) * nb_wuwphn);
    wuwphn[0] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[0], "S");
    wuwphn[1] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[1], "u");
    wuwphn[2] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[2], "n");
    wuwphn[3] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[3], "i");
    wuwphn[4] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[4], "m");
    strcpy(pronom, "minuS");
  }



  if(strcmp(wuw,"hestia")==0) {
    nb_wuwphn = 5;
    wuwphn = (char **) malloc(sizeof(char *) * nb_wuwphn);
    wuwphn[0] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[0], "a");
    wuwphn[1] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[1], "j");
    wuwphn[2] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[2], "t");
    wuwphn[3] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[3], "s");
    wuwphn[4] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[4], "e");
    strcpy(pronom, "estja");
  }

  if(strcmp(wuw,"ulysse")==0) {
    nb_wuwphn = 4;
    wuwphn = (char **) malloc(sizeof(char *) * nb_wuwphn);
    wuwphn[0] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[0], "s");
    wuwphn[1] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[1], "i");
    wuwphn[2] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[2], "l");
    wuwphn[3] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[3], "y");
    strcpy(pronom, "ylis");
  }

  if(strcmp(wuw,"ichefix")==0) {
    nb_wuwphn = 7;
    wuwphn = (char **) malloc(sizeof(char *) * nb_wuwphn);
    wuwphn[0] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[0], "s");
    wuwphn[1] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[1], "k");
    wuwphn[2] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[2], "i");
    wuwphn[3] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[3], "f");
    wuwphn[4] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[4], "e");
    wuwphn[5] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[5], "S");
    wuwphn[6] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[6], "i");
    strcpy(pronom, "iSefiks");
  }
  
  if(strcmp(wuw,"vocadom")==0) {
    nb_wuwphn = 7;
    wuwphn = (char **) malloc(sizeof(char *) * nb_wuwphn);
    wuwphn[0] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[0], "m");
    wuwphn[1] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[1], "o");
    wuwphn[2] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[2], "d");
    wuwphn[3] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[3], "a");
    wuwphn[4] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[4], "k");
    wuwphn[5] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[5], "o");
    wuwphn[6] = (char *) malloc(sizeof(char) * 2 );
    strcpy(wuwphn[6], "v");
    strcpy(pronom, "vokadom");
  }

  t = 0.0;

  // 0.00 1.40 ws 0 as 140  align 4 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1

  //while (fgets(buffer,STRLIGNE,stdin)!=NULL) {
    if(buffer[strlen(buffer)-1]=='\n') buffer[strlen(buffer)-1]='\0';
    indice=0;
    indice=extraire_un_mot(buffer,mot,indice);
    t=atof(mot);
    ok = 0;
    strcpy(ligne, "");
    
    while (((indice=extraire_un_mot(buffer,mot,indice))!=-1) && (strcmp(mot,"align")!=0)) {
      // ok == 1  si il y a le mot cle dans la chaine de mots reconnus
      strcat(ligne," ");
      strcat(ligne, mot);
      if (strcmp(mot,wuw)==0) {
	       ok = 1;
      }
    }

    if(ok == 1) {
      numpred = -2;
      strcpy(phonemes, "");
      k = 0;
      // debdeb  debut du premier phoneme de wuw
      // finfni  debut du dernier phoneme de wuw
      ind_deb = -1;
      ind_fin = -1;
      while ((indice=extraire_un_mot(buffer,mot,indice))!=-1) {
      	tr = atoi(mot);
      	if(tr<nb_pdf) {
      	  if(numpred != table_pdf[tr]) {
      	    sprintf(tt,"%.2f",t);
      	    strcat(ligne, " ");
      	    strcat(ligne, tt);
      	    strcat(ligne, " ");
      	    strcat(ligne, table_phn[table_pdf[tr]]);
      	    
      	    ph[k]=table_pdf[tr];
      	    deb[k] = t;
      	    fin[k] = t + 0.01;
      	    if(k >= nb_wuwphn-1) {
      	      okphn = 1;
      	      s = 0;
      	      while((okphn == 1) && (s<nb_wuwphn)) {
          		  // print $ph[$k-$s]." ".$wuwphn[$s]."\n";
                if(strcmp(table_phn[ph[k-s]], wuwphn[s])!=0) {
            		  okphn = 0;
            		}
            		s++;
      	      }
      	      
      	      // if(($k>=4) && ($ph[$k] eq "S") && ($ph[$k-1] eq "u") && ($ph[$k-2] eq "n") && ($ph[$k-3] eq "i") && ($ph[$k-4] eq "m")) {
      	      if((ind_deb == -1) && (okphn == 1)) {
            		ind_deb = k-nb_wuwphn+1;
            		ind_fin = k;
            	}
	          }
      	    k++;
      	    strcat(phonemes,table_phn[table_pdf[tr]]);
      	    numpred = table_pdf[tr];
      	  } else {
      	    // meme phoneme
      	    fin[k-1] = t + 0.01;
      	  }
      	} else {
      	  printf("bizarre trans %d %d\n", tr, nb_pdf);
      	}
    	  t += 0.01;
      }
      if((ok == 1) && (strstr(phonemes, pronom)!=NULL)) {	
        if( fin[ind_fin] - deb[ind_deb] >= 0.5)
	{
	     debdeb = deb[ind_deb];
	     finfin = fin[ind_fin];

	     int phnTimeCount = 0;
	     //printf("%.2f %.2f %s", debdeb, finfin, wuw);
	     for(j=ind_deb; j<=ind_fin; j++) {
		 //printf(" %.2f %s",deb[j],table_phn[ph[j]]);
		 output->phnTime[phnTimeCount] = fin[j] + firstStamp/100;
		 output->phnValue[phnTimeCount] = table_phn[ph[j]];
		 phnTimeCount++;
	     }
	     //sprintf(output, "%.2f %.2f %s %.2f\n",debdeb, finfin, wuw);
	     output->debut = debdeb + firstStamp/100;
	     output->fin = finfin + firstStamp/100;
	     memcpy(output->name, wuw,sizeof(wuw));
	     mot_trouve = true;
	}
      	//printf(" %.2f\n",finfin);
      }
    }

  free(wuwphn);
  //free(wuw);
  free(table_phn);

  return mot_trouve;
}

std::string msgToString(MotReco msg, int micro)
{
    std::string msgString = "Ep " + std::to_string(micro) + " " +
	std::to_string(msg.debut) + " " + std::to_string(msg.fin) + " " +
	msg.name;

    int count = 0;
    while(count < MAXWUPHN)
    {
	if(msg.phnTime[count] > 0)
	{
	    msgString += " " + std::to_string(msg.phnTime[count]) + " " + msg.phnValue[count];
	    count++;

	}
	else
	    break;
    }

    return msgString;
}

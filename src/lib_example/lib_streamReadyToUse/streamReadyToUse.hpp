/*
 * @Author: Clémence VIAL 
 * @Date: 2019-11-23 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2019-11-25 11:08:28 
 */

#ifndef STREAMREADYTOUSE_HPP
#define STREAMREADYTOUSE_HPP

#include <iostream>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

/* ====================
	Constants
   ==================== */  
#define MACHINE "SRTU"							// lib's name
#define PORTSTAMPSTREAM 4040   								// stampStream server's port (server send flux with stamps)
#define PORTMODULE 5050										// module server's port (example : Kaldi server)
#define NUMBER_KALDI 4
#define READ 0
#define WRITE 1
#define NBR_BYTES 100

/* ====================
	Constants
   ==================== */  
enum kaldi
{
	KALDI1 = 0,
	KALDI2,
	KALDI3,
	KALDI4
};

/* ====================
	Prototypes
   ==================== */ 
extern "C" int streamReadyToUse_init();
extern "C" int streamReadyToUse_preprocess();
extern "C" int streamReadyToUse_process();
extern "C" int streamReadyToUse_postprocess();
extern "C" int streamReadyToUse_finish();

#endif //STREAMREADYTOUSE_HPP

/*
 * @Author: Clémence VIAL 
 * @Date: 2019-11-23 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2019-11-25 11:08:28 
 */

#include "streamReadyToUse.hpp"
#include "config.hpp"
#include "decodeEvent.hpp"
#include "config_acquisitor_portaudio_online.hpp"
#include "tcpConnection.hpp"
#include "filtre/filtremsg.h"
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <fcntl.h>                      // using for open file with O_RDONLY, S_IRUSR, S_IWUSR, S_IROTH
#include <string.h>

std::string inputFolder;                                           
std::string outputFolder;                                       // contains a file with the module server's answer
std::string file = "VocadomConf.xml";                               

static configuration_acquisitor *Cfg_acquisitor; 						   // structure pour la configuration de l'acquisition

static int port_stamp_stream = PORTSTAMPSTREAM;                              // stampstream server's port 
static std::string adress_stamp_stream;                                   // stamp stream adress
static SoundInfo current_sound_info;                                         // Informations about sound receive

static pthread_cond_t condition = PTHREAD_COND_INITIALIZER;                // condition use to synchronize the connection with servers
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;                  // mutex use to synchronize the connection with servers
static int sock_stamp_stream;                                              // socket connected to the stampStream server
static unsigned int first_stamp = 0;                                               // first stamp detected in the sound received
static int all_ports_kaldi[NUMBER_KALDI];                                  // server Kaldis's ports
static int all_sockets_kaldi[NUMBER_KALDI];                                // sockets Kaldis
static int pipe_fd[2];                                                     // pipe's file descriptor :  fd[0] for reading fd[1] for writting
static int kaldi_created = 0;                                              // number of thread created to connect to Kaldi
static int port_event_manager;                                             // the event manager's port
static std::string adress_event_manager;                                   // the event manager's adress
static int sock_event_manager;
static bool eventManager_connected = true;
static bool kaldis_connected = true;
static bool keyword_detected = false;
static double  prev_keyword_start = -1.;
std::string Mfound;
static std::string MinoucheInfo = "script/MinoucheId.sh";// file use in StampAudioOnline to turn up the light in blue use 2 time in SRTU to switch color of light and to send the location of te speaker 

/**
 * @brief client_to_event_manager Thread: client connected to event manager
 * @return
 */
void* client_to_event_manager(void*)
{
    uint8_t ACK;
    char message[1000];
    Event evt;

    // sending module ID (STRU) to the EventManager
    if(0 > write(sock_event_manager, MACHINE, sizeof(MACHINE)))
    {
        std::cout <<"Failed to send ID " << MACHINE << std::endl;
        eventManager_connected = false;
        exit(0);
    }

    int ret = read(sock_event_manager, &ACK, sizeof(ACK));
    if(0 > ret || 0 != ACK )
    {
        std::cout <<"Failed to receive ACK from EventManager" << std::endl;
        eventManager_connected = false;
        exit(0);
    }

    std::cout <<"Subscribe to EventManager SUCCESS" << std::endl;

    while(1)
    {
        int ret = read(sock_event_manager, message, sizeof(message));
        if(0 >= ret)
        {
            std::cout <<"EventManager disconnected" << std::endl;
            eventManager_connected = false;
            break;
        }
        else
        {
            Event *current_event = decodeEvent(message,ret);
            if(current_event->type != None)
            {
                if(current_event->type == Ep)
                {
                    std::cout <<"EventManager ACK Ep"<< std::endl;
                }
                else if(current_event->type == Ef)
                {
                    std::cout << "End of word detected" << std::endl;
                    keyword_detected = false;
                }
            }
            delete current_event;
        }
    }

    pthread_exit(NULL);
}

/**
 * @brief dispatch_stream_to_kaldis Thread: split data channels to send it to kaldis
 * @return
 */
void* dispatch_stream_to_kaldis(void*)
{
    uint8_t inBuffer[NBR_BYTES_PER_RECV*16];           // input buffer from fdin
    uint8_t dispatch_stream[NUMBER_KALDI][NBR_BYTES_PER_RECV];
    int nb_bytes_in, nb_bytes_output ;
    int bit_depth = current_sound_info.bitDepth/8;
    int channel;
    int num_kaldi[NUMBER_KALDI] = {KALDI1,KALDI2,KALDI3,KALDI4};
    int index_1, index_2, index_3, index_4;

    int current_channel = 0;

    while(kaldis_connected)
    {
    	index_1 = 0;
    	index_2 = 0;
    	index_3 = 0;
    	index_4 = 0;

	int nb_read = 0;

	while(nb_read < NBR_BYTES_PER_RECV*16)
	{
	    nb_bytes_in = read(pipe_fd[READ], inBuffer + nb_read, NBR_BYTES_PER_RECV*16 - nb_read);

	    if(0 >= nb_bytes_in)
	    {
		printf("problem stamp\n");
		break;
	    }

	    nb_read += nb_bytes_in;
	}

	for(int i = 0; i < nb_read; i+=2 )
	{
	    current_channel %= 16;

	    if(current_channel == 0)
	    {
		dispatch_stream[KALDI1][index_1] = inBuffer[i];
		dispatch_stream[KALDI1][index_1+1] = inBuffer[i+1];
		index_1 += 2;
	    }
	    else if(current_channel == 4)
	    {
		dispatch_stream[KALDI2][index_2] = inBuffer[i];
		dispatch_stream[KALDI2][index_2+1] = inBuffer[i+1];
		index_2 += 2;
	    }
	    else if(current_channel== 8)
	    {
		dispatch_stream[KALDI3][index_3] = inBuffer[i];
		dispatch_stream[KALDI3][index_3+1] = inBuffer[i+1];
		index_3 += 2;
	    }
	    else if(current_channel == 12)
	    {
		dispatch_stream[KALDI4][index_4] = inBuffer[i];
		dispatch_stream[KALDI4][index_4+1] = inBuffer[i+1];
		index_4 += 2;
	    }
	    current_channel++;
	}

	for(int i =0; i < NUMBER_KALDI; i++ )
	{
	    nb_bytes_output = write(all_sockets_kaldi[i], dispatch_stream[num_kaldi[i]], nb_read/16);

	    if(0 >= nb_bytes_output )
	    {
		std::cout << "Servers Kaldis are deconnected..." << std::endl;
		kaldis_connected = false;
	    }
	}
    }

    pthread_exit(NULL);
}

/**
 * @brief client_to_stamp_stream Thread : Connection to the stampStream server
                                          First stamp's detection
                                          Sending sound to the module server
 * @return
 */
void* client_to_stamp_stream(void*)
{
    int ret;

    // Sound received from module stampStream, stamp is removed to sound and the new sound is sent to module server
    ret  = start_SoundDeSync(&current_sound_info, sock_stamp_stream, pipe_fd[WRITE], first_stamp);
                                                                   
	if(0 < ret )                                                    // an error happend
		std::cout <<"ERROR : reading from server failed" << std::endl;

	else if(0 == ret)                                              // stampStream server exit
		std::cout << "Serveur StampStrem disconnected... " << std::endl;

	else                                                          // module server exit
		std::cout << "Serveur Kaldi disconnected... " << std::endl;

    kaldis_connected = false;

	pthread_exit(NULL);
}

/**
 * @brief client_to_kaldi Thread : Connection to the module server (example : Kaldi online)
                                   Adding first stamp in the output file
                                   Listenning to the module server's answer and adding in the output file
 * @param num
 * @return
 */
void* client_to_kaldi(void* num)
{
    int number_kaldi = *(int*) num;
	int port_kaldi = all_ports_kaldi[number_kaldi];
    std::string output = outputFolder;                              // output folder will contains out.txt
    std::string outputFile = output + "/" + "Out" + std::to_string(number_kaldi) + ".txt";              // output file : out.txt
    std:: string pdf = "../Models/KeysWords/filtre/minouche.pdf";
    char mot[] = "minouche";
    char output_before_filter[STRLIGNE];                                          // server's answer 
    int nb_bytes_in;                                                        // start_SoundDeSync's retrurn 
    char message[100];
    MotReco msg;
    double deltaTime;
    bool ret_filtre;

    if(!boost::filesystem::exists(output))                           // Resetting output folder 
    {
        boost::filesystem::create_directory(output);        
    }
    
    boost::filesystem::ofstream currentOutFile{outputFile};         // openning outputFile

    if(!currentOutFile.is_open())
    {
        std::cout <<"failed to open file " << std::endl;
        exit(0);
    }

    pthread_mutex_lock(&mutex);
    kaldi_created++;
    pthread_mutex_unlock(&mutex);

    if(kaldi_created == NUMBER_KALDI)
    {         
        pthread_mutex_lock(&mutex); 
        pthread_cond_signal (&condition); // mutex condition to prevent that connection with kaldis are established
        pthread_mutex_unlock(&mutex); 
    }
    
    std::cout <<"Waiting detection of first stamp " << std::endl;

    while(0 == first_stamp);                                         // Waiting first stamp's detection

    deltaTime = first_stamp * 0.10;                                   // Calculate the begin time
    std::cout << "FIRST STAMP : " <<  first_stamp << std::endl;
    currentOutFile << "T0 : " << first_stamp << std::endl;           // Adding first stamp in output file

    while(kaldis_connected)
    {
        nb_bytes_in = read(all_sockets_kaldi[number_kaldi], output_before_filter, sizeof(output_before_filter)-1); // Reading server's answer

        if(0 < nb_bytes_in)
        {
	    pthread_mutex_lock(&mutex);
	    if(!keyword_detected)
	    {
		output_before_filter[nb_bytes_in] = '\0';
		currentOutFile << output_before_filter << std::endl;            // saving in a file

		//pthread_mutex_lock(&mutex);
		ret_filtre = filtremsg(pdf.c_str(), mot, output_before_filter, &msg, first_stamp);
		//pthread_mutex_unlock(&mutex);

		
		// filter message received from Kaldi
		if(ret_filtre && msg.debut > prev_keyword_start +  5)
		{
		    //Get string to send from MotReco
		    // détection antenne mot clé
		    std::string tmpMessage = msgToString(msg,number_kaldi);
		    prev_keyword_start = msg.debut;
	        //Talk to OH
	        
	        std::ifstream ifs(MinoucheInfo.c_str());
		std::string curlCommandAndData;
		//Key word detected
		Mfound="Loading";
		curlCommandAndData.assign((std::istreambuf_iterator<char>(ifs)),
					   (std::istreambuf_iterator<char>()));
		curlCommandAndData = curlCommandAndData.substr(0, curlCommandAndData.size()-1);
		curlCommandAndData = curlCommandAndData + "\"" + Mfound + "\"" ;
		if(system(NULL))
		    system(curlCommandAndData.c_str());
		    
		//send the location
		/*Location=std::to_string(number_kaldi);
		curlCommandAndData.assign((std::istreambuf_iterator<char>(ifs)),
					   (std::istreambuf_iterator<char>()));
		curlCommandAndData = curlCommandAndData.substr(0, curlCommandAndData.size()-1);
		curlCommandAndData = curlCommandAndData + "\"" + Location + "\"" ;
		if(system(NULL))
		    system(curlCommandAndData.c_str());*/
		    
                    if(eventManager_connected && !keyword_detected )
                    {
                        int ret = write(sock_event_manager, tmpMessage.c_str(), tmpMessage.size());

                        if(0 >= ret)
                        {
                            std::cout << "Server EventManager is deconnected..." << std::endl;
                            eventManager_connected = false;
                        }
                        keyword_detected = true;
                    }
                }
            }
            pthread_mutex_unlock(&mutex);
        }
        else
        {
            // Kaldi server is deconnected
            std::cout << "Kaldi server " << port_kaldi << " disconnected..." << std::endl;
            kaldis_connected = false;
        }

    }

    std::cout << "Close client to Kaldi " << all_ports_kaldi[number_kaldi] << std::endl;
    close(all_sockets_kaldi[number_kaldi]);
    pthread_exit(NULL);

}


int streamReadyToUse_init()
{
	
    if(GetPort(file, MACHINE, "PortStampStream", port_stamp_stream) != 1)
    {
        std::cout <<"Failed to get port of StampStream " << MACHINE << std::endl;
        return -1;
    }
 
    if(GetPort(file, MACHINE, "PortEventManager", port_event_manager) != 1)
    {
        std::cout <<"Failed to get port of EventManager " << MACHINE << std::endl;
        return -1;
    }

    if(GetAdress(file, MACHINE, "AdressEventManager", adress_event_manager) != 1)
    {
        std::cout <<"Failed to get adress of EventManager " << MACHINE << std::endl;
        return -1;
    }

    if(GetAdress(file, MACHINE, "AdressStampStream", adress_stamp_stream) != 1)
    {
        std::cout <<"Failed to get adress of Stamp Stream " << MACHINE << std::endl;
        return -1;
    }

    if(GetPorts(file, MACHINE, "PortKaldi", all_ports_kaldi, NUMBER_KALDI) != 1)
    {
        std::cout <<"Failed to get port Kaldi " << MACHINE << std::endl;
        return -1;
    }

    if(GetConfig(file, MACHINE, inputFolder, outputFolder) != 1)
    {
        std::cout <<"Failed to get inputFolder or outputFolder " << MACHINE << std::endl;
        return -1;
    }

    return 1;
} 

int streamReadyToUse_preprocess()
{
	char Cfg_acquisitor_file[100];       // file contains config for portaudio

    if(pipe(pipe_fd) != 0)               // init pipe to trasnfer stream from stampStream to Kaldi
        return -1;
    
    // Loading portaudio_acquisitor configuration file
    sprintf(Cfg_acquisitor_file, "%s", inputFolder.c_str());

	Cfg_acquisitor = (configuration_acquisitor *)malloc(sizeof(configuration_acquisitor));
	if (Cfg_acquisitor == NULL)
	{
		std::cout << "Allocation Impossible <Cfg_acquisitor> " << stderr << std::endl;
		return -1;
	}

    // Load configuation file 
	load_configuration_portaudio_acquisitor(Cfg_acquisitor, Cfg_acquisitor_file);
    current_sound_info.numChannels = Cfg_acquisitor->channel;
    current_sound_info.sampleRate = Cfg_acquisitor->sample_rate;
    current_sound_info.bitDepth = Cfg_acquisitor->bit_depth;
	
    // connection to the EventManger 
    sock_event_manager = connect_tcp_client(port_event_manager, adress_event_manager);
    if(0 > sock_event_manager)
    {
        std::cout <<"Failed to connect to EventManager " << std::endl;    
        eventManager_connected = false;                                        
    }
    else
        std::cout <<"Server EventManager connected on port " << port_event_manager << std::endl;

    // connection to the StampStream server 
    sock_stamp_stream = connect_tcp_client(port_stamp_stream, adress_stamp_stream);
    if(0 > sock_stamp_stream)
    {
        std::cout << "Connection serveur StampStream failed... " << std::endl;
        return -1;
    }
    std::cout <<"Server StampStream connected on port " << port_stamp_stream << std::endl;

    //connection to the Kaldis server
    for(int i=0; i<NUMBER_KALDI; i++)
    {
        all_sockets_kaldi[i] = connect_tcp_client(all_ports_kaldi[i], "127.0.0.1");      // connection to a server Kaldi
        if(0 > all_sockets_kaldi[i])
        {
            std::cout <<"Failed to connect to Kaldi " << all_ports_kaldi[i] << std::endl;
            return -1;
        }
        std::cout <<"Server Kaldi connected on port " << all_ports_kaldi[i] << std::endl;
    }
    

    return 1;
}

int streamReadyToUse_process()
{
    std::cout << "Running StreamReadyToUse" << std::endl;

    pthread_t thread_client_to_stamp_stream;
    pthread_t thread_client_to_event_manager;
    pthread_t thread_dispatch_stream_to_kaldis;
    pthread_t thread_client_to_kaldi[NUMBER_KALDI];

    int new_thread_client_to_stamp_stream;
    int new_thread_client_to_event_manager;
    int new_thread_dispatch_stream_to_kaldis;
    int new_thread_client_to_kaldi[NUMBER_KALDI];
    int num_kaldi[NUMBER_KALDI] = {KALDI1,KALDI2,KALDI3,KALDI4};  

    if(eventManager_connected)
    {
        // Thread to receive messages from eventManager.
        new_thread_client_to_event_manager = pthread_create( &thread_client_to_event_manager , NULL ,  client_to_event_manager , NULL);
        if(new_thread_client_to_event_manager)
        {
            std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
            return -1;
        }
    }    

    // One thread for each connection to a server Kaldi and receive the answer of each Kaldi
    for(int i =0; i< NUMBER_KALDI; i++)
    {
        new_thread_client_to_kaldi[i] = pthread_create( &thread_client_to_kaldi[i] , NULL ,  client_to_kaldi , (void *) &num_kaldi[i]);
        if(new_thread_client_to_kaldi[i])
        {
            std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
            return -1;
        }
    }

    pthread_mutex_lock(&mutex);                 // mutex condition to waiting new clients
    pthread_cond_wait (&condition, &mutex); 
    pthread_mutex_unlock(&mutex); 

    // Thread to connect to the server module which send the audio stamped
	new_thread_client_to_stamp_stream = pthread_create( &thread_client_to_stamp_stream , NULL ,  client_to_stamp_stream , NULL);
    if(new_thread_client_to_stamp_stream)
    {
		std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
		return -1;
    }
    
    // Thread to dispatch the audio without stamp to each Kaldi
    new_thread_dispatch_stream_to_kaldis = pthread_create( &thread_dispatch_stream_to_kaldis , NULL ,  dispatch_stream_to_kaldis , NULL);
    if(new_thread_dispatch_stream_to_kaldis)
    {
        std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
        return -1;
    }

    if (pthread_join(thread_client_to_stamp_stream, NULL) && 
        pthread_join(thread_dispatch_stream_to_kaldis, NULL) &&
        pthread_join(thread_client_to_kaldi[KALDI1], NULL) &&
        pthread_join(thread_client_to_kaldi[KALDI2], NULL) &&
        pthread_join(thread_client_to_kaldi[KALDI3], NULL) &&
        pthread_join(thread_client_to_kaldi[KALDI4], NULL) ) 
    {
        perror("pthread_join");
        return -1;
    }

    return 1;
}

int streamReadyToUse_postprocess()
{
    return 1;
}

int streamReadyToUse_finish()
{
    return 1;
}

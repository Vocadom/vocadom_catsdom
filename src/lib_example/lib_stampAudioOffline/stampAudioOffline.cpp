
/*
 * @Author: Clémence VIAL 
 * @Date: 2019-11-23 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2019-11-25 11:08:28 
 */

#include "stampAudioOffline.hpp"
#include "config.hpp"
#include "SoundSync.hpp"
#include "tcpConnection.hpp"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>                   // Using for signal(SIGPIPE, SIG_IGN)-> not stop when the server writing fails
#include <fcntl.h>                      // using for open file with O_RDONLY, S_IRUSR, S_IWUSR, S_IROTH
#include <pthread.h>
#include <sys/time.h>

std::string inputFolder;
std::string outputFolder;
std::string file = "VocadomConf.xml";
int portServer=0;

int nbSock=0;                                           // socket client's number open
int sockclientTab[NBR_CLIENTS];                         // Array of sockets clients
pthread_cond_t condition = PTHREAD_COND_INITIALIZER;    // mutex condition to waiting new client connexion
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

//time
struct timeval start, end, result;
long mtime, seconds, useconds;


int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y)
{
  /* Perform the carry for the later subtraction by updating y. */
  if (x->tv_usec < y->tv_usec) {
    int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
    y->tv_usec -= 1000000 * nsec;
    y->tv_sec += nsec;
  }
  if (x->tv_usec - y->tv_usec > 1000000) {
    int nsec = (x->tv_usec - y->tv_usec) / 1000000;
    y->tv_usec += 1000000 * nsec;
    y->tv_sec -= nsec;
  }

  /* Compute the time remaining to wait.
     tv_usec is certainly positive. */
  result->tv_sec = x->tv_sec - y->tv_sec;
  result->tv_usec = x->tv_usec - y->tv_usec;

  /* Return 1 if result is negative. */
  return x->tv_sec < y->tv_sec;
}


/* ----------------------------------------------------------------------
    Thread : Data's reception of one client 
             Detect if the client is deconnected
             If the client is deconnected, it is deleted from socket client Tab
             [in] socket client
   ---------------------------------------------------------------------- */
void *receiveData(void *sock)
{
    if(sock==NULL)
        return 0;    

    char bufferRecv[2];                                 // received buffer from client
    int client =*(int*) sock;                           // client's socket
    int i =0, j=0;

    // While bufferRcv > 0 client is connected
    while(read(client, bufferRecv, sizeof(bufferRecv)) > 0);

    std::cout << "Client " << client << " exit" << std::endl; 
    
    pthread_mutex_lock(&lockFdOut);

    shutdown(client, 2);
    close(client);                                        // client socket is closed

    for(i=0; i<(nbSock-1); i++)                           // client is deleted from socket clients array
    {
        if(sockclientTab[i]==client)
        {
            for(j=i;j<(nbSock-1);j++)
                *(sockclientTab+j)=*(sockclientTab+j+1);

            break;
        }
    }

    nbSock--;

    pthread_mutex_unlock(&lockFdOut);

    pthread_exit(NULL);
}

/* ----------------------------------------------------------------------
    Thread : Sending WAV file to socket clients 
   ---------------------------------------------------------------------- */
void *sendData(void*)
{   
    int filefd;                                                 // Input file descriptor
    int err;                                                    // error return by start_SoundSync
    std::string input = inputFolder;                            // Contains WAV files
    SoundInfo currentSoundInfo;                                 // Information about Sound in WAV file's header
    std::vector<std::string> currentFileList;                   // List of file in Input folder
    std::string currentFile;                                    // current WAV file

    GetFileList(currentFileList, input, ".wav");                // List of input files

    while(1)
    {
        while(1)
        {
            gettimeofday(&start, NULL);
            //foreach input file we send to the clients
            BOOST_FOREACH(currentFile, currentFileList)
            {
                if( 0 >= nbSock )
                    break;
                
                // Openning file 
                filefd = open(currentFile.c_str(), O_RDONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

                init_SoundSync(filefd, &currentSoundInfo);        // Info (numChannels, freq, nbBits) about the file .WAV

                std::cout << "WAV FILE : " << currentFile.c_str() << std::endl;
                std::cout << "NbrChannel : "<< currentSoundInfo.numChannels << std::endl;
                std::cout << "Freq : " << currentSoundInfo.sampleRate << std::endl;
                std::cout << "NbBits :" << currentSoundInfo.bitDepth << std::endl;
                std::cout << "Sending..." << std::endl;

                if(filefd < 0)
                {
                    std::cout << "Can't open file:" << strerror(errno) << std::endl; 
                    break; 
                }
                
                // Start_soundSync stamp the sound and send to the clients    
                err = start_SoundSync(&currentSoundInfo, filefd, sockclientTab, &nbSock);      
            
                if(err < 0)
                    std::cout << "Failed to read file:" << strerror(errno) << std::endl;

                else if(err == 0)
                   std::cout << "finished.\n"  << std::endl ;
            
                close(filefd);
            } 

            gettimeofday(&end, NULL);
        
            timeval_subtract(&result, &end, &start);

            std::cout << "diff sec : " << result.tv_sec << " diff usec : " << result.tv_usec << std::endl;
            
            if( 0 >= nbSock )
               break;
        }

        std::cout << "Waiting for clients...\n"  << std::endl;

        pthread_mutex_lock(&mutex);                             // Mutex condition waiting new client 
        pthread_cond_wait (&condition, &mutex); 
        pthread_mutex_unlock(&mutex); 

    }

    pthread_exit(NULL);
}


int stampAudioOffline_init()
{
    if(GetPort(file, MACHINE, "PortStampStream", portServer) == -1)
        portServer = PORT;

    return GetConfig(file, MACHINE, inputFolder, outputFolder);
} 

int stampAudioOffline_preprocess()
{
       return 1;
}

int stampAudioOffline_process()
{
    std::cout << "Running StampAudioOffline" << std::endl;
      
    int new_thread_rcv;                                 // return after thread creation
    int new_thread_send;                                // return after thread creation
    int newsock;                                        // Socket descriptor of the new client
    int sockserverfd;                                   // Server's socket descriptor
    socklen_t len;                                      // client size
    struct sockaddr_in servaddr;                        // Informations about server
    struct sockaddr_in cli;                             // information about client
    pthread_t thread_rcv[NBR_CLIENTS];
    pthread_t thread_send;
    bool thread_send_exist = false;                     // Using to create only once sending thread
    int enable = 1;                                     // enable SO_REUSEADDR
    
    signal(SIGPIPE, SIG_IGN);                           // Disabling SIGPIPE whitch makes the server crash
    
    sockserverfd = socket(AF_INET, SOCK_STREAM, 0);     // socket create and verification 

    if (sockserverfd == -1) 
    { 
        std::cout << "socket creation failed...\n" << std::endl; 
        exit(0); 
    } 
    std::cout << "Socket successfully created..\n" << std::endl; 
    
     
    servaddr.sin_family = AF_INET;                      // TCP/IP
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);       // server adresse
    servaddr.sin_port = htons(portServer);              // server port 


    if (setsockopt(sockserverfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0){
        std::cout << "setsockopt(SO_REUSEADDR) failed" << std::endl; 
        exit(0); 
    }

    // Binding newly created socket to given IP and verification 
    if ((bind(sockserverfd, (SA*)&servaddr, sizeof(servaddr))) != 0) 
    { 
        std::cout << "socket bind failed..." << std::endl; 
        exit(0); 
    } 
    std::cout << "Socket successfully binded.." << std::endl; 
    
    
    if ((listen(sockserverfd, 5)) != 0)                 // Now server is ready to listen and verification 
    { 
        std::cout << "Listen failed..." << std::endl; 
        exit(0); 
    } 
    std::cout <<"Server listening on port "<< portServer << "..." << std::endl;

    len = sizeof(cli); 
  
    while(1)
    {
        // Accept the data packet from client and verification 
        newsock = accept(sockserverfd, (SA*)&cli, &len); 

        if (newsock < 0) 
        { 
            std::cout << "server acccept failed..." << std::endl; 
            break; 
        } 
        
        pthread_mutex_lock(&lockFdOut);             // LockFdOut is a global mutex share with SoundSync.h a
                                                    // Using to access clients array

        if(nbSock >= NBR_CLIENTS)
        {
            std::cout << "There is too much clients. Can't accept the client" << std::endl; 
        }
        else
        {
            std::cout << "Server acccept the client " << newsock << std::endl; 
            
            sockclientTab[nbSock] = newsock;        // Adding new client to clients array

           //Each time a new client is accepted a new thread is created to receive its data 
            new_thread_rcv = pthread_create( &thread_rcv[nbSock] , NULL ,  receiveData , (void*) &sockclientTab[nbSock]);
            if(new_thread_rcv)
            {
                std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
                break;
            }

            // Only once thread is created to send WAV file to clients array
            if(!thread_send_exist)
            {
                new_thread_send = pthread_create( &thread_send , NULL ,  sendData , NULL);

                if(new_thread_send)
                {
                    std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
                    break;
                }

                thread_send_exist =true;
            }

            nbSock++;
        }
        
        pthread_mutex_unlock(&lockFdOut);           // lockFdOut released

        pthread_mutex_lock (&mutex);                // A mutex condition released to prevent the precense of a new client
        pthread_cond_signal (&condition); 
        pthread_mutex_unlock (&mutex); 

    }

    for(int i = 0; i < nbSock; i++)                 // Close for each received thread 
        pthread_exit(&thread_rcv[i]);               

    pthread_exit(&thread_send);                     // Close sending thread

    for(int i = 0 ; i < nbSock ; i++ )              // Close clients
        close(sockclientTab[i]);
     
    close(sockserverfd);                            // Close server

    return 1;    
}

int stampAudioOffline_postprocess()
{
    return 1;
}

int stampAudioOffline_finish()
{
    return 1;
}
/*
 * @Author: Clémence VIAL 
 * @Date: 2019-11-23 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2019-11-25 11:08:28 
 */

#ifndef STAMPAUDIOOFFLINE_HPP
#define STAMPAUDIOOFFLINE_HPP

#include <iostream>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

/* ====================
	Constants
   ==================== */  
#define MACHINE "StampAudioOffline"							        // lib's name
#define PORT 5050   										// server's port
#define ADRESS "127.0.0.1"                                                                      // server's adress
#define SA struct sockaddr 									// server's struct contains its informations
#define NBR_CLIENTS 10										// server's max number clients which can be accepted 

/* ====================
	Prototypes
   ==================== */ 
extern "C" int stampAudioOffline_init();
extern "C" int stampAudioOffline_preprocess();
extern "C" int stampAudioOffline_process();
extern "C" int stampAudioOffline_postprocess();
extern "C" int stampAudioOffline_finish();

#endif
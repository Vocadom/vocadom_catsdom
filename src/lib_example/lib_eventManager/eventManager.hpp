/*
 * @Author: Clémence VIAL 
 * @Date: 2019-11-23 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2019-11-25 11:08:28 
 */

#ifndef EVENTMANAGER_HPP
#define EVENTMANAGER_HPP

#include <iostream>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#define SIZE_ID 10
#define SIZE_MSG 1000
#define NBR_CLIENTS 5
#define NBR_SUBSCRIBERS 3
/* ====================
	Constants
   ==================== */  
#define MACHINE "EventManager"							// lib's name
#define PORT 5050										// module server's port (example : Kaldi server)


/* ====================
	Prototypes
   ==================== */ 
extern "C" int eventManager_init();
extern "C" int eventManager_preprocess();
extern "C" int eventManager_process();
extern "C" int eventManager_postprocess();
extern "C" int eventManager_finish();

#endif //EVENTMANAGER_HPP
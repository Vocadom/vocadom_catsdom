
/*
 * @Author: Clémence VIAL 
 * @Date: 2019-11-23 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2019-11-25 11:08:28 
 */

#include "eventManager.hpp"
#include "config.hpp"
#include "tcpConnection.hpp"
#include "decodeEvent.hpp"
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/time.h>

#include <fcntl.h>                      // using for open file with O_RDONLY, S_IRUSR, S_IWUSR, S_IROTH
#include <string.h>
                                  // contains a file with the module server's answer
std::string file = "VocadomConf.xml";                               

static int port_serveur = PORT;                              // stampstream server's port
static std::string adress_serveur;
static SoundInfo current_sound_info;                                         // Informations about sound receive

static pthread_cond_t condition = PTHREAD_COND_INITIALIZER;                // condition use to synchronize the connection with servers
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;                  // mutex use to synchronize the connection with servers
static int sock_stamp_stream;                                              // socket connected to the stampStream server
static int nbSock = 0;
static int sockclientTab[NBR_CLIENTS];
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
static std::vector<std::string> subscribers;

/**
 * @brief client_subscriber Thread : Data's reception of one client
             Detect if the client is deconnected
             If the client is deconnected, it is deleted from socket client Tab
 * @param sock socket client
 * @return
 */
void *client_subscriber(void *sock)
{
    if(sock==NULL)
        return 0;    

    char bufferRecv[SIZE_MSG];                                 // received buffer from client
    int client =*(int*) sock;                           // client's socket
    char id[SIZE_ID]; 
    int nbBytes;
    uint8_t subscriber_accepted = 1;
    char filename[2048];
    char my_date[1024];
    struct timeval ini_timestamp;
    struct tm *ptm;

    nbBytes = read(client, id, SIZE_ID);

    if(0 <= nbBytes )
    {
        for(int i =0; i < NBR_SUBSCRIBERS; i++)
        {
            if(0 == subscribers[i].compare(id))
            {
                std::cout << "Client " << subscribers[i] << " connected" << std::endl; 
                subscriber_accepted = 0;
            }
        }

        // Send ACK to subscriber
        write(client, &subscriber_accepted, 1);
    }
    else
    {
        std::cout << "Client " << client << " exit" << std::endl; 
        pthread_exit(NULL); 
    }

    // create the name of the file (one file for each subscriber) 
    gettimeofday(&ini_timestamp, NULL);   
    ptm = localtime(&ini_timestamp.tv_sec);
    my_date[0]=0;
    strftime(my_date, 200 * sizeof(char), "%Y-%m-%d_%H-%M-%S", ptm);
    sprintf(filename, "../logs/%s.%03ld_log_%s.log", my_date,  ini_timestamp.tv_usec / 1000, id);
     std::string logsFile = filename;
     
     // openning outputFile
     boost::filesystem::ofstream currentOutFile{logsFile};        
     //std::cout << logsFile << std::endl;

     if(!currentOutFile.is_open())
     {
         std::cout <<"failed to open file " << std::endl;
         exit(0);
     }
    

    while(0 == subscriber_accepted)
    {
        memset(bufferRecv, 0, SIZE_MSG);
        nbBytes = read(client, bufferRecv, SIZE_MSG);
        std::cout << "Receive from "  << id << " : " << bufferRecv << std::endl;
        currentOutFile << id << " : " << bufferRecv << std::endl;

        if(0 >= nbBytes)
	{
            subscriber_accepted = 1;
	    std::cout << "Subscriber " << id << " is deconnected" << std::endl;

	    break;
	}

        Event *evt = decodeEvent(bufferRecv, nbBytes);
        if(evt->type != None)
        {
	    std::cout << bufferRecv  << "end " << std::endl;
            pthread_mutex_lock(&lock);
            for(int i =0; i < nbSock; i++  )
            {
                nbBytes = write(sockclientTab[i], bufferRecv, nbBytes);
                if(0 >= nbBytes)
                    std::cout << "Dispatch message to " << sockclientTab[i] << " FAILED " << std::endl;
            }
            pthread_mutex_unlock(&lock);
        }else
        {
            nbBytes = write(client, "ACK_NO", nbBytes);
            if(0 >= nbBytes)
	    {
		std::cout << "Subscriber " << id << " is deconnected" << std::endl;
		break;
	    }
        }
	free(evt);
    }

    std::cout << "Client " << client << " exit" << std::endl; 

    pthread_mutex_lock(&lock);

    shutdown(client, 2);
    close(client);                                        // client socket is closed

    for(int i=0; i<(nbSock-1); i++)                           // client is deleted from socket clients array
    {
        if(sockclientTab[i]==client)
        {
            for(int j=i;j<(nbSock-1);j++)
                *(sockclientTab+j)=*(sockclientTab+j+1);

            break;
        }
    }

    nbSock--;

    pthread_mutex_unlock(&lock);

    pthread_exit(NULL);
}



int eventManager_init()
{
	return 1;
} 

int eventManager_preprocess()
{
    if(GetPort(file, MACHINE, "PortEventManager", port_serveur) != 1)
    {
        std::cout << "Failed to find port : " << std::endl;
        return -1 ;
    }

    if(GetAdress(file,MACHINE, "AdressEventManager", adress_serveur) != 1)
    {
        std::cout << "Failed to find adress" << std::endl;
        return -1;
    }

    if(GetSubscribers(file, MACHINE, subscribers, NBR_SUBSCRIBERS) != 1)
    {
        std::cout << "Failed to find port : " << std::endl;
        return -1 ;
    }  

    for(int i =0; i< NBR_SUBSCRIBERS; i++) 
        std::cout << "Subscriber : " << subscribers[i] << std::endl;
    return 1;
}

int eventManager_process()
{
    std::cout << "Running EventManager" << std::endl;
      
    int new_thread_rcv;                                 // return after thread creation
    int new_thread_send;                                // return after thread creation
    int newsock;                                        // Socket descriptor of the new client
    int sockserverfd;                                   // Server's socket descriptor
    socklen_t len;                                      // client size
    struct sockaddr_in servaddr;                        // Informations about server
    struct sockaddr_in cli;                             // information about client
    pthread_t thread_rcv[NBR_CLIENTS];
    pthread_t thread_send;
    bool thread_send_exist = false;                     // Using to create only once sending thread
    int enable = 1;                  

                                      // enable SO_REUSEADDR
    sockserverfd = create_tcp_server(port_serveur, adress_serveur);   // Init tcp server

    if( sockserverfd == -1)
        return -1;

    len = sizeof(cli); 
    
    while(1)
    {
        // Accept the data packet from client and verification 
        newsock = accept(sockserverfd, (SA*)&cli, &len); 

        if (newsock < 0) 
        { 
            std::cout << "server acccept failed..." << strerror(errno) << std::endl; 
            break; 
        } 
        
        pthread_mutex_lock(&lock);             // LockFdOut is a global mutex share with SoundSync.h a
                                                    // Using to access clients array

        if(nbSock >= NBR_CLIENTS)
        {
            std::cout << "There is too much clients. Can't accept the client" << std::endl; 
        }
        else
        {
            std::cout << "Server acccept the client " << std::endl; 
            
            sockclientTab[nbSock] = newsock;        // Adding new client to clients array

           //Each time a new client is accepted a new thread is created to receive its data 
            new_thread_rcv = pthread_create( &thread_rcv[nbSock] , NULL ,  client_subscriber, (void*) &sockclientTab[nbSock]);
            if(new_thread_rcv)
            {
                std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
                break;
            }
            nbSock++;
        }
        
        pthread_mutex_unlock(&lock);           // lockFdOut released

        pthread_mutex_lock (&mutex);                // A mutex condition released to prevent the precense of a new client
        pthread_cond_signal (&condition); 
        pthread_mutex_unlock (&mutex); 

    }

    for(int i = 0; i < nbSock; i++)                 // Close for each received thread 
        pthread_exit(&thread_rcv[i]);               

    pthread_exit(&thread_send);                     // Close sending thread

    for(int i = 0 ; i < nbSock ; i++ )              // Close clients
        close(sockclientTab[i]);
     
    close(sockserverfd);                            // Close server

    return EXIT_SUCCESS;
}

int eventManager_postprocess()
{
    return 1;
}

int eventManager_finish()
{
    return 1;
}

#!/bin/bash
# Script indication d'état de la chaîne appeler dans StampAudioOnlineet SRTU
cp src/lib_example/MinoucheId.sh bin/script/

#Script RAP
cp src/lib_example/lib_RAP/script/englishWords.sh bin/script/

cp src/lib_example/lib_RAP/script/motsT.sh bin/script/

cp src/lib_example/lib_RAP/script/motsFr.sh bin/script/

#Modèle et Script NLU
cp src/lib_example/lib_NLU/Data/GENERATE_PREDICTIONS.sh bin/script/

cp src/lib_example/lib_NLU/Data/curlCommandNLU.sh bin/script/

#!/bin/bash

cp src/lib_example/lib_rehauss/script/*\.py bin/script/
cp src/lib_example/lib_rehauss/script/*\.hdf5 bin/script/
cp src/lib_example/lib_rehauss/script/*\.h5py bin/script/

var=$(conda env list)

if [[ $var =~ .*"location_env".* ]]; then
    echo "location_env already exist"
else
    echo "create environnement location_env"
    conda env create -f src/lib_example/lib_Rehauss/script/rehauss.yml
fi

echo "before running Rehauss you must activate location_env environnement"

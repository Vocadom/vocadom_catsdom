#include "Wrapper.hpp"
#include "config.hpp"
#include "tcpConnection.hpp"
#include "decodeEvent.hpp"
#include "rehauss.hpp"

#include <string>
#include <iostream>
#include <fstream>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

std::string file = "VocadomConf.xml";

// port for event manager
static int port_event_manager;
static std::string adress_event_manager;
// port for stamp stream
static int port_stamp_stream;
static std::string adress_stamp_stream;
// port for VAD
static int port_vad;
static std::string adress_vad;

static bool eventManager_connected = true;


// the event manager's port
static int sock_event_manager;

/**
 * @brief thread function receiving event from event manager
 * @param NULL
 * @return (void)
 */
void* client_to_event_manager(void*)
{
    uint8_t ACK;
    char message[1000];

    // sending module ID (Rehauss) to the EventManager
    if(write(sock_event_manager, MACHINE, sizeof(MACHINE)) < 0)
    {
        std::cout <<"Failed to send ID " << MACHINE << std::endl;
        eventManager_connected = false;
        exit(0);
    }

    // read ack from EventManager
    int ret = read(sock_event_manager, &ACK, sizeof(ACK));
    if(ret < 0 || ACK != 0 )
    {
        std::cout <<"Failed to receive ACK from EventManager" << std::endl;
        eventManager_connected = false;
        exit(0);
    }

    std::cout <<"Subscribe to EventManager SUCCESS" << std::endl;

    std::ofstream fifoWrapperPython;

    while(1)
    {
        //wait for fifo to be created from python script
        if(fs::is_fifo("fifoWrapper"))
        {
            fifoWrapperPython.open("fifoWrapper",std::ofstream::out);
	    std::cout << "fifo open" << std::endl;
            break;
        }
        else
        {
            sleep(1);
        }
    }

    while(1)
    {
        memset(message,0,1000);
        int ret = read(sock_event_manager, message, sizeof(message));

        std::cout << "message receive is" <<  message<< std::endl;
        if(ret <= 0)
        {
            std::cout <<"EventManager disconnected" << std::endl;
            eventManager_connected = false;
           break;
        }

        Event *current_evt = decodeEvent(message,ret);

        /* if an event is received send signal the python script
           with the correct information through the fifo*/
        if(current_evt->type != None)
        {
            /* Event from recotMotClé */
            if(current_evt->type  == Ep)
            {
                auto str = EventToString(*current_evt);
                fifoWrapperPython << str.c_str() << std::endl;
                std::cout << "Keyword detected :" << str << std::endl;
            }
            /*Event from VAD*/
            else if(current_evt->type == Ef)
            {
                std::cout << "VAD finish" << std::endl;
                fifoWrapperPython << "stop" << std::endl;
            }
            else
            {
                std::cout << "dont know" << std::endl;
            }
        }
        delete current_evt;
    }

    pthread_exit(NULL);
}

/**
 * @brief thread running Rehauss module
 * @param NULL
 * @return (void)
 */
void* run_rehauss(void*)
{
    std::cout << port_vad << " " << port_stamp_stream << std::endl;

    std::list<std::string> args({adress_stamp_stream,std::to_string(port_stamp_stream),
                adress_vad, std::to_string(port_vad)});
    runPyModule(PY_REHAUSS_SCRIPT, args);

    std::cout << "end of run\n" << std::endl;
    pthread_exit(NULL);
}

int rehauss_init()
{
    if(GetPort(file, MACHINE, "PortStampStream", port_stamp_stream) != 1)
    {
        std::cout <<"Failed to get port of StampStream " << MACHINE << std::endl;
        return -1;
    }

    if(GetAdress(file, MACHINE, "AdressStampStream", adress_stamp_stream) != 1)
    {
        std::cout <<"Failed to get adress of StampStream " << MACHINE << std::endl;
        return -1;
    }

    if(GetPort(file, MACHINE, "PortEventManager", port_event_manager) != 1)
    {
        std::cout <<"Failed to get port of EventManager" << MACHINE << std::endl;
        return -1;
    }

    if(GetAdress(file, MACHINE, "AdressEventManager", adress_event_manager) != 1)
    {
        std::cout <<"Failed to get adress of EventManager " << MACHINE << std::endl;
        return -1;
    }

    if(GetPort(file, MACHINE, "PortVAD", port_vad) != 1)
    {
        std::cout <<"Failed to get port of VAD " << MACHINE << std::endl;
        return -1;
    }

    if(GetAdress(file, MACHINE, "AdressVAD", adress_vad) != 1)
    {
        std::cout <<"Failed to get adress of VAD " << MACHINE << std::endl;
        return -1;
    }

    return 1;
}

int rehauss_preprocess()
{
    uint8_t ACK;
    char message[50];

    sock_event_manager = connect_tcp_client(port_event_manager, adress_event_manager); // connect to event manager
    if(sock_event_manager < 0)
    {
        std::cout <<"Failed to connect to Event Manger " << MACHINE << std::endl;
        return -1;
    }

    /*if fifo to communicate with script already exist
      delete it*/
    if(fs::is_fifo("fifoWrapper"))
    {
        system("rm fifoWrapper");
    }

    return 1;
}

int rehauss_process()
{
    pthread_t thread_run_rehauss;
    pthread_t thread_client_to_event_manager;

    int ret_thread;

    ret_thread = pthread_create(&thread_run_rehauss, NULL, run_rehauss, NULL);
    if(ret_thread != 0)
    {
        std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
        return -1;
    }

    ret_thread  = pthread_create(&thread_client_to_event_manager, NULL, client_to_event_manager, NULL);
    if(ret_thread != 0)
    {
        std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
        return -1;
    }

    if (pthread_join(thread_client_to_event_manager, NULL) &&
        pthread_join(thread_run_rehauss, NULL))
    {
        perror("pthread_join");
        return -1;
    }

    return 1;
}

int rehauss_postprocess()
{
    return 1;
}

int rehauss_finish()
{
    return 1;
}

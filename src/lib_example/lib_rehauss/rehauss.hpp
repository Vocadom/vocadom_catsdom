/*
 * @Author: Labadens Lucas 
 * @Date: 2020-09-06
 */

#ifndef REHAUSS_HPP
#define REHAUSS_HPP

#define MACHINE           "Rehauss"
#define PY_REHAUSS_SCRIPT "script/loc_enhance.py"

/* =========================
	Prototypes
   ========================= */ 
extern "C" int rehauss_init();

extern "C" int rehauss_preprocess();

extern "C" int rehauss_process();

extern "C" int rehauss_postprocess();

extern "C" int rehauss_finish();

#endif // REHAUSS_HPP

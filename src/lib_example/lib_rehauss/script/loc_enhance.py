from queue import Queue
from threading import Thread
import sys
import re
import collections
import socket
import os
import numpy as np
from Stamp import *
from scipy.io.wavfile import write
import standalone as doa_if
from standalone import ph_data_to_ctm, align2ph_frame, M1, MicPair
from enh_standalone import Beamform, nnet_lstm
import subprocess

from logWrapper import init_log, close_log, Print


signal = collections.deque(maxlen=10)

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 70000        # Port to listen on (non-privileged ports are > 1023)

DOA_MODEL = "script/doa_cnn_clean_mask_est.hdf5"
DOA_MASK_MODEL = "script/mask_est_cnn_clean_mask_est.hdf5"
SPEC_NORM = "script/norm.h5py"
PHONE_SPEC = "script/ester_phone_spectrum_25ms_new.h5py"
DS_MASK_MODEL = "script/lstm_dropout_mag_spec_cmvn_csipd_multi_src.hdf5"

#do we use enhancement or not 
ENHANCE	= False

# Network for enhancement


SAMPLING_RATE       = 16000
ENCODING            = 16
CHANNELS            = 16
NB_ANTENNA          = 4
CHANNEL_PER_ANTENNA = 4

BytesPerSample = 2

StampPerSec = 100
SizeOfStamp = 8


BytesPerSec    = (SAMPLING_RATE*BytesPerSample)*CHANNELS + StampPerSec*SizeOfStamp #Audio Rate bytes plus size of stamps
BytesPer10mSec =  (SAMPLING_RATE*BytesPerSample)/100
SamplePer10mSec = BytesPer10mSec // BytesPerSample
chunk          = int(BytesPerSec/4) # Receive data from client in chunks (exp. BytePerSec/2 means 0.5s)
maxSizeBuf     = SAMPLING_RATE*100 # Store 1.5s of Audio data

stamp = 0x7fff

def enhance(DataFromStart, DOA_OBJ, DS_NNET, mic_pair_array):
    '''
    Input
    -----
        DataFromStart: Full segment of 4 channel speech
        mic_pair_array: MicPair object with speaker_location location info
    Output
    ------
        Enhanced speech
    '''
    mic_array = M1['mics']
    diag_mic  = M1['diag'][0]
    beamform_at = M1['beamform_at']
    temp_mic_pair_array = []
    for mic_pair in mic_pair_array:
        mic = mic_pair.mic_pair
        wav1 = DataFromStart[:,mic[0]-1]
        wav2 = DataFromStart[:,mic[1]-1]
        mic_pair = MicPair(mic, mic_pair.doa, 0.1, wav1, wav2)
        temp_mic_pair_array.append(mic_pair)
    beam = Beamform(DOA_OBJ.frame_stft_if, temp_mic_pair_array, beamform_at)
    #beam.set_doa_from_doa_array()
    ds_sig = beam.ds_beamform()
    mask = beam.compute_mask_with_model_pd_feats(ds_sig, DS_NNET.model)
    try:
        enh_sig = beam.r1_mwf(mask)
        return enh_sig
    except:
        return DataFromStart[:,0]
       

def loc(DataFromStart, DOA_OBJ,phone_seq):
    '''
    Input
    -----
        DataFromStart: shape 4xN containing wav data from one microphone array
        phone_seq:[(m,122),(i,193)] An array of tuples containing phone seq and corresponding duration in samples. The keyword is assumed to start from zero i.e , in the above example 'm' range is [0 -122] samples
    Output
    ------
        speaker_loc: The speaker location
    '''
    mic_array = M1['mics']
    diag_mic = M1['diag'][0]
    beamform_at = M1['beamform_at']
    phone_id_ctm = ph_data_to_ctm(phone_seq, phone_seq[len(phone_seq)-1][1]) # 8000 corresponds to 0.5s
    phone_subseg = align2ph_frame(phone_id_ctm, 800)

    mic_pair_array = []
    for mic in mic_array:
        wav1 = DataFromStart[:,mic[0]-1][:8000]
        wav2 = DataFromStart[:,mic[1]-1][:8000]

        mask_est_feats = DOA_OBJ.create_feats_for_mask_est(wav1, wav2, phone_subseg)
        mask = DOA_OBJ.estimate_mask(mask_est_feats)
        doa = DOA_OBJ.estimate_doa(wav1, wav2, mask)
        mic_pair = doa_if.MicPair(mic, doa, 0.1, wav1, wav2)
        mic_pair_array.append(mic_pair)
    return mic_pair_array

#SUNIT CODE SHOULD REPLACE THIS FUNCTION
# for the example select the first channel of the antenna (antenna number starts with 1) indicated by the keyword
def loc_enhance(DataFromStart, antenna, keyword_start,keyword_finish):
        channel = (antenna-1)*4
        return DataFromStart[:,channel]


# A thread that produces data
# get event from stdin in the form of
# \#antenna begining_of_keyword end_of_keyword
# antenna from {1,4}
# beginning end in number of sample (not used in this example code)
# if stop is sent then the enhancement should stop and get ready for next keyword
def producer(out_q):
        os.mkfifo("fifoWrapper")

        with open("fifoWrapper","r") as fifo:
                while True:

                        info = fifo.readline()

                        if info == "":

                                fifo.close()
                                os.unlink("fifoWrapper")
                                exit(1);

                        #End of voice event
                        if info == "stop\n":
                                out_q.put("stop")

                        #Keyword detected event
                        elif re.search('(\d+)\s+(\d*\.\d+|\d+)\s+(\d*\.\d+|\d+)\s+(.*)', info):
                                m = re.search('(\d+)\s+(\d*\.\d+|\d+)\s+(\d*\.\d+|\d+)\s+(.*)', info)
                                #m= re.search('(\d+)\s+(\d+)\s+(\d+)', info)
                                antenna=int (m.group(1))
                                start  = float (m.group(2))
                                finish = float (m.group(3))
                                phnm   = [tuple(i.split("-")) for i in m.group(4).split(" ")]
                                phnm   = [(x,float(y)) for (x,y) in phnm]
                                out_q.put([antenna,start,finish,phnm])
                        else:
                                Print("nothing",info)
        fifo.close()

#get buffer keyword based on stamp
def getKeyWordBuffer(Data,keyword_end):
        if keyword_end  > len(Data):
                return Data[:8000,]
        elif keyword_end < 8000:
                return Data[:8000,]

        else:
                return Data[:keyword_end, ]

# A thread that consumes data
# it constantly reads the input signal from the socket
# regarding processing it has 2 modes :
# ACCUMULATE: just put recent samples in a buffer (should be circular)
# PROCESS : when a keyword is sent, then find it in the buffer and process all incoming data. Should send the processed data to the next module (here only stored in outDataFromStart and written in a wav)
# the thread terminates in case of timeout
def consumer(in_q,addrStamp,portStamp, addrVAD, portVAD):
        keyword_antenna=None
        keyword_start=None
        keyword_finish=None
        PROCESS = False
        kBufFound = False

        DataFromStart    = np.array([])
        outDataFromStart = np.array([])
        outdata          = np.array([])
        lastStamp        = 0
        bufFirstStamp    = -1
        global mic_info

	#if enhance then load DOA and enhance models
        if (ENHANCE):
                DOA_OBJ = doa_if.DoaEstimation(DOA_MASK_MODEL, DOA_MODEL, PHONE_SPEC, SPEC_NORM)
                DS_NNET = nnet_lstm(3*801, 801)
                DS_NNET.load_model(DS_MASK_MODEL)

        firstDataRcv = True

        #connect to main stream
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                #connect to stamp stream
                s.connect((addrStamp,int(portStamp)))
                # Set timeout period
                #s.settimeout(2)
                socVAD = None
                with s:
                        Print('Connected with', addrStamp)
                        last       = b""

                        currentStamps = []
                        connectVAD = False
                        while True:
                                if not in_q.empty():
                                        # Get some data
                                        info = in_q.get()

                                        # Process the data
                                        if info == "stop":
                                                PROCESS = False
                                                kBufFound = False

                                                DataFromStart = np.array([]) # clear old buffer

                                                currentStamps = []
                                                connectVAD    = False
                                                socVAD.close()
                                                Print("VAD stop")
                                        else:
                                                keyword_antenna = info[0]*4
                                                keyword_start   = info[1]
                                                keyword_finish  = info[2]
                                                phnm_info       = info[3]

                                                phnm_info = [(ord(val),int((time-keyword_start)*100*SamplePer10mSec)) for val,time in phnm_info]

                                                socVAD =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                                                while not connectVAD:
                                                        try:
                                                                #verify if previous connection is closed properly
                                                                str = "lsof -i tcp:50000 -s tcp:established"
                                                                lsof = subprocess.Popen(list(str.split()),stdout = subprocess.PIPE, stderr = subprocess.PIPE)
                                                                try:
                                                                        output, errs = lsof.communicate()
                                                                except subprocess.TimeoutExpired:
                                                                        lsof.kill()

                                                                if output == b'':
                                                                        Print("connect to VAD")

                                                                        #connect to VAD
                                                                        for i in range(5):
                                                                            try:
                                                                                socVAD.connect((addrVAD,int(portVAD) + i))
                                                                                connectVAD = True
                                                                                break
                                                                            except:
                                                                                pass

                                                        except:
                                                                pass

                                                PROCESS = True

                                # Perform a blocking I/O operation w/timeout
                                try:
                                        dataRcv = s.recv(chunk)

                                        #first data receive should be a stamp, otherwise reject everything before the first stamp
                                        if firstDataRcv:
                                            firstStampPos = [i for i in range(len(dataRcv) - 2) if dataRcv[i] == 0xff and dataRcv[i+1] == 0x7f][0]
                                            dataRcv = dataRcv[firstStampPos:]
                                            Print(firstStampPos)
                                            firstDataRcv = False

                                        data    = last + dataRcv
                                except socket.timeout:
                                        Print("connection terminated.")
                                        path = os.path.join("enhanced.wav")
                                        Print("Saving file to", path)
                                        write(path, SAMPLING_RATE, outDataFromStart)
                                        break
                                #wait for more data
                                if len(data) < chunk:
                                        last = data
                                        continue
                                elif len(data) > chunk:
                                        last  = data[chunk: ]
                                        data  = data[:chunk]

                                else:
                                        #clear previous buffer
                                        last = b""

                                unstampData,currentStamp = getUnstampData(data,stamp,16)

                                currentStamps.append(currentStamp)
                                if bufFirstStamp == -1:
                                        bufFirstStamp = currentStamp

                                npdata = np.frombuffer(unstampData,dtype=np.int16)

                                #divide sample per channel
                                nb_samples = (len(unstampData) // BytesPerSample)
                                npdata = npdata.reshape((nb_samples//CHANNELS, CHANNELS))

                                #divide data to max short value to get float
                                npdata = np.multiply(npdata,3.0517578125e-5)
                                npdata = npdata.astype('float32')

                                if len(DataFromStart) == 0:
                                        DataFromStart = npdata
                                        currentStamps = currentStamps[len(currentStamps) - 1:]

                                elif len(DataFromStart) < maxSizeBuf:
                                        DataFromStart = np.concatenate((DataFromStart, npdata), axis=0)

                                else:
                                        DataFromStart = DataFromStart[nb_samples//CHANNELS:,:]
                                        DataFromStart = np.concatenate((DataFromStart, npdata), axis=0)
                                        currentStamps.pop(0)


                                #if process active, treats incomming signal and free input buffer
                                #otherwise continue to accumulate signal (should be done using a circular buffer)
                                if PROCESS:
                                        if  not kBufFound:

                                                bufFirstStamp = currentStamps[0]
                                                #position of the first sample of the keyword
                                                startKeyword = int(SamplePer10mSec * (keyword_start * 100 - bufFirstStamp))
                                                #position of the last sample of the keyword based on the beginning of it
                                                endKeyword =  phnm_info[len(phnm_info)-1][1]

                                                #Keyword not save in current buffer
                                                #Too much latency between keyword recognition
                                                #and enhancement module
                                                if startKeyword < 0:
                                                        Print("Keyword already pass retry")
                                                        outdata = DataFromStart[:,keyword_antenna]                                                        
                                                        #if enhance then do DOA and enhancement
                                                        if (ENHANCE):
                                                                mic_info = loc(DataFromStart[:8000,keyword_antenna:keyword_antenna + 4], DOA_OBJ, phnm_info)
                                                                outdata = enhance(DataFromStart[:,keyword_antenna:keyword_antenna + 4], DOA_OBJ, DS_NNET, mic_info)
                                                        Print("But location work with current buffer",outdata)
                                                        PROCESS = False
                                                        connectVAD = False

                                                        socVAD.close()

                                                #keyword in current buffer
                                                elif len(DataFromStart) > startKeyword > 0 and endKeyword < len(DataFromStart):

                                                        #save the buffer only at the beginning of the keyword
                                                        DataFromStart = DataFromStart[startKeyword:, ]
                                                        #get the part of the speech where the keyword has been told
                                                        KeywordBuffer = getKeyWordBuffer(DataFromStart, endKeyword)

                                                        #reset currentStamps
                                                        currentStamps = []
                                                        
                                                        #if enhance then do DOA 
                                                        if (ENHANCE):
                                                                mic_info = loc(KeywordBuffer[:,keyword_antenna:keyword_antenna + 4], DOA_OBJ, phnm_info)

                                                        kBufFound = True

                                                else:
                                                        print("wait for keyword to arrive")
                                                        continue

                                        if PROCESS :
                                                    outdata = DataFromStart[:,keyword_antenna]
                                                    #if enhance then do Enhancement 
                                                    if (ENHANCE):
                                                        outdata = enhance(DataFromStart[:,keyword_antenna:keyword_antenna + 4], DOA_OBJ, DS_NNET, mic_info)
                                                    outdata = np.divide(outdata,3.0517578125e-5)
                                                    outdata = outdata.astype('int16')
                                                    #restamp data
                                                    sendDataFromStart = getStampData(outdata.tobytes(),lastStamp,1,0x7fff)
                                                    lastStamp += (StampPerSec // 2)

                                                    try:
                                                        socVAD.sendall(sendDataFromStart)
                                                        DataFromStart = np.array([]) # clear old buffer
                                                        outData = np.array([])
                                                    except:
                                                        #Connection has been closed by VAD
                                                        Print("VAD closed connection")
                                                        connectVAD = False
                                                        socVAD.close()
                                                        pass






def main(argv):

        #verify number of argument
        if len(argv) < 5:
                print("missing argument should be loc_enhance.py ADRESS_STREAM PORT_STREAM")
                return
        print(chunk)

        init_log()

        # Create the shared queue and launch both threads
        q = Queue()
        t1 = Thread(target = consumer, args =(q, argv[1], argv[2], argv[3], argv[4]))
        t2 = Thread(target = producer, args =(q, ))

        t1.start()
        t2.start()

        # Wait for all produced items to be consumed
        q.join()
        t1.join()
        t2.join()

        close_log()

if __name__ == "__main__":
    main(sys.argv)

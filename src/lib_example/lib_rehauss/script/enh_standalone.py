import torch
import torch.nn as nn
from ipdb import set_trace
import numpy as np
from pooling import PoolDoa
from standalone import  STFT
from all_beamforming import gev_wrapper_on_masks


class DelayAndSum(object):
    '''
        A class to do delay and sum beamforming
            input:
            -------
            tdoa_array: An array containing tdoas of multiple microphones
            sampling_rate: sampling frequency
            freq_bin: Number of frequency bins

        '''
    def __init__(self, tdoa_array, sampling_rate=16000, freq_bin=801):
        self.tdoa = tdoa_array
        self.microphone_cnt = len(tdoa_array) + 1
        self.max_freq = sampling_rate/2.0
        self.freq_bin_count = freq_bin
        self.frequency = np.linspace(0, sampling_rate/2, freq_bin)
        self.steering_vector_fast = self._create_steering_vector_fast()
        self.stft_if = STFT(2*(freq_bin-1))


    def _create_steering_vector_fast(self):
        self.tdoa = np.array(self.tdoa)
        phase_shift = np.einsum('i,j->ij', self.frequency, self.tdoa)
        steering_vector = np.exp(-2*np.pi*1j*phase_shift)
        return steering_vector

    def apply_beamforming_stft_fast(self, signal_array, average=False):
        '''
        Apply ds beamforming on time domain signal and return a ds signal in STFT doman
            input:
                signal_array: A list containing signals for
                        each microphone. The first element in the
                        list is the reference microphone.
        '''
        assert len(signal_array) == self.microphone_cnt, \
                "Signal array does not contain signals for all microphones"
        stft_array = [self.stft_if.compute_stft(sig) for sig in \
                signal_array]
        stft_array = np.stack(stft_array,0)
        return self.beamforming_stft_fast(stft_array, average)

    def beamforming_stft_fast(self, stft_array, average=False):
        ''''
        Apply ds beamforming in stft domain and return a ds signal in STFT doman
        Apply beamforming on stft
            Input
            stft_array is of shape (channels, freq , time)
        '''
        assert stft_array.shape[0] == self.microphone_cnt, \
                "Signal array does not contain signals for all microphones"
        steering_vector = self.steering_vector_fast
        delayed_and_sum = np.einsum('ijk,ji->jk', stft_array[1:], steering_vector)
        delayed_and_sum += stft_array[0]
        if average:
            delayed_and_sum /= self.microphone_cnt
        return delayed_and_sum

    def apply_beamforming_signal_fast(self, signal_array, \
            return_stft=False, average=False):
        '''
        Apply ds beamforming on time domain signal and return a ds signal in time doman
        '''
        delayed_and_sum = self.apply_beamforming_stft_fast(signal_array, average)
        if return_stft:
            return delayed_and_sum
        ds_sig = self.stft_if.compute_istft(delayed_and_sum, length=signal_array[0].shape[0])
        return ds_sig


class nnet_lstm(object):
    def __init__(self, total_input_dim, output_dim):
        self.input_dim = total_input_dim
        self.output_dim = output_dim
        self.model = DsBeamedLstmNetwork(total_input_dim, output_dim)
        self.optimizer = torch.optim.Adam(self.model.parameters())

    def predict(self, csipd, mag_spec):
        self.model.eval()
        mask = self.model(csipd, mag_spec)
        return mask

    def load_model(self, path):
        self.model.load_state_dict(torch.load(path, map_location='cpu'))

class DsBeamedLstmNetwork(nn.Module):
    def __init__(self, input_dim, output_dim):
        nn.Module.__init__(self)
        self.ds_lstm = nn.LSTM(input_dim, output_dim, 2,\
                dropout=0.5, batch_first=True, bidirectional=True)
        self.pre_mask = nn.Linear(2*output_dim, output_dim)
        self.mask = nn.Sigmoid()

    def forward(self, csipd, mag_spec):
        batch_cnt = csipd.shape[0]
        time_cnt = csipd.shape[-1]
        feats = torch.cat((csipd,mag_spec),dim=1).reshape(batch_cnt,-1, time_cnt)
        feats = feats.permute(0, 2, 1)
        lstm_out, _ = self.ds_lstm(feats)
        pre_mask = self.pre_mask(lstm_out)
        mask = self.mask(pre_mask)
        return mask

class EnhSpeech(object):
    def __init__(self, mask_model):
        model_path = mask_model
        self.ds_mask_object = nnet_lstm(3*801, 801)
        self.ds_mask_object.load_model(mask_model)

class Beamform(object):
    '''
    beamform_at: The microphone at which beamforming will take place
    Input:
    ------
    stft_if: interface to compute stft
    mic_pairs: list of MicPair instances
    beamform_at: Reference mic to beamform at
    pool_if: instance of doa pool interface of class experiments.localization.ester.pooling

    '''
    def __init__(self, stft_if, mic_pairs, beamform_at):
        self.doa_pool_if = PoolDoa()
        self.mic_pair = mic_pairs
        #self.doa_array = [mic.doa_array for mic in mic_pairs]
        self.ds_beamformer = None
        self.doa = None
        self.beamform_at = beamform_at
        self.mic_pairs_to_beamform = []
        self.stft_if = stft_if
        self.reference_sig = None

    def pool_doa_with_search_grid(self, invert=False):
        doa_pair_vec = {}
        ref_mic = self.beamform_at
        for mic in self.mic_pair:
            mic_pair = np.array(mic.mic_pair)[:2] - ref_mic + 1
            mic_pair = tuple(mic_pair)
            if mic.doa_array is None:
                continue
            doa_array = self.smooth_doa(self.normalize_doa(mic.doa_array))
            if invert:
                #doa_pair_vec[mic_pair] = mic.doa_array[::-1]
                doa_pair_vec[mic_pair] = doa_array[::-1]
            else:
                doa_pair_vec[mic_pair] = doa_array
        #grid_val = self.doa_pool_if.do_pooling(doa_pair_vec)
        grid_val = self.doa_pool_if.do_pooling_by_interpolation(doa_pair_vec)
        best_pos = self.doa_pool_if.find_best_pos(grid_val)
        tdoa = self.doa_pool_if.get_tdoa(best_pos)
        doa = self.doa_pool_if.compute_doa_for_points(best_pos[np.newaxis])
        for mic_pair in self.mic_pair:
            dummy_mic_pair = np.array(mic_pair.mic_pair)[:2] - ref_mic + 1
            dummy_mic_pair = tuple(dummy_mic_pair)
            mic_pair.tdoa = tdoa[dummy_mic_pair] 
        return doa

    def pick_mic_pairs_to_beamform_tdoa(self):
        '''
            Collect all the mic pairs containing beamform_at mic
        '''
        self.mic_pairs_to_beamform = []
        for mic in self.mic_pair:
            if self.beamform_at == mic.mic_pair[0]:
                self.mic_pairs_to_beamform.append(mic)
                if self.reference_sig is None:
                    self.reference_sig = mic.wav_data[0]
            elif self.beamform_at == mic.mic_pair[1]:
                mic.flip_mic_tdoa()
                self.mic_pairs_to_beamform.append(mic)
                if self.reference_sig is None:
                    self.reference_sig = mic.wav_data[0]

    def pick_mic_pairs_to_beamform(self):
        '''
            Collect all the mic pairs containing beamform_at mic
        '''
        self.mic_pairs_to_beamform = []
        for mic in self.mic_pair:
            if self.beamform_at == mic.mic_pair[0]:
                self.mic_pairs_to_beamform.append(mic)
                if self.reference_sig is None:
                    self.reference_sig = mic.wav_data[0]
            elif self.beamform_at == mic.mic_pair[1]:
                mic.flip_mic()
                self.mic_pairs_to_beamform.append(mic)
                if self.reference_sig is None:
                    self.reference_sig = mic.wav_data[0]

    def set_doa_from_doa_array(self, invert=False):
        for mic in self.mic_pair:
            if invert:
                mic.doa = 180 - mic.doa_array.argmax()
            else:
                mic.doa = mic.doa_array.argmax()


    def ds_beamform(self):
        self.pick_mic_pairs_to_beamform()
        tdoa = []
        signal = [self.mic_pairs_to_beamform[0].wav_data[0]]
        for mic in self.mic_pairs_to_beamform:
            tdoa.append(mic.get_tdoa())
            signal.append(mic.wav_data[1])
        self.ds_beamformer = DelayAndSum(tdoa)
        #return self.ds_beamformer.apply_beamforming(signal, average=True)
        return self.ds_beamformer.apply_beamforming_signal_fast(signal, average=True)


    def ds_beamform_from_tdoa(self):
        self.pick_mic_pairs_to_beamform_tdoa()
        tdoa = []
        signal = [self.mic_pairs_to_beamform[0].wav_data[0]]
        for mic in self.mic_pairs_to_beamform:
            assert mic.tdoa is not None, 'tdoa of mic pair not set'
            tdoa.append(mic.tdoa)
            signal.append(mic.wav_data[1])
        self.ds_beamformer = DelayAndSum(tdoa)
        #return self.ds_beamformer.apply_beamforming(signal, average=True)
        return self.ds_beamformer.apply_beamforming_signal_fast(signal, average=True)



    def compute_mask_with_model(self, ds_beamformed_signal, model):
        sig_stft = self.stft_if.compute_stft(ds_beamformed_signal)
        data = np.expand_dims(np.abs(sig_stft).T, 0)
        mask = model.predict(data)[0]
        return mask

    def compute_mask_with_model_pd_feats(self, ds_beamformed_signal_orig, \
            model, frames=None, zero_pad_flag=False):
        '''
        Input
        ------
            frames: Number of frames needed to compute the mask. Give None if all is required
            zero_pad_flag: The network seems to have learnt that the initial few frames 
            are silence. To compensate, silence is included in the first 10 frames 
        '''
        # Zero padding initially, to provide some initial silence 
        # frames for the network
        #zero_pad_size = self.stft_if.win_len
        #ds_beamformed_signal  = np.zeros((zero_pad_size + \
        #        ds_beamformed_signal_orig.shape[0]))
        #ds_beamformed_signal[zero_pad_size:] = ds_beamformed_signal_orig

        ds_beamformed_signal = ds_beamformed_signal_orig
        
        sig_stft = self.stft_if.compute_stft(ds_beamformed_signal)
        sig_stft = sig_stft[:, :frames]
        mag_spec = np.abs(sig_stft)
        mag_mean = np.expand_dims(mag_spec.mean(1),1)
        mag_std = np.expand_dims(mag_spec.std(1),1)
        mag_spec = (mag_spec - mag_mean)/mag_std
        assert self.reference_sig is not None, \
                'Reference signal is not set'
        ref_sig_stft = self.stft_if.compute_stft(self.reference_sig)
        ref_sig_stft = ref_sig_stft[:, :frames]
        phase_diff = np.angle(ref_sig_stft/(sig_stft+np.finfo(float).eps))

        if zero_pad_flag:
            zero_pad = np.zeros((mag_spec.shape[0], 10), dtype=np.float32)
            mag_spec = np.hstack((zero_pad, mag_spec))
            phase_diff = np.hstack((zero_pad, phase_diff))

        data = torch.tensor(np.expand_dims(mag_spec, 0))
        data = data.unsqueeze(1)
        csipd = np.stack(cos_sin_phase_diff(phase_diff),0)
        csipd = torch.tensor(np.expand_dims(csipd, 0))
        mask = model(csipd, data)
        if zero_pad_flag:
            mask = mask[:,10:,:]
        return mask.data.cpu().squeeze().numpy()

    def r1_mwf(self, mask):
        output_setup = {}
        output_setup['output_type'] = 'r1-mwf'
        output_setup['gev_ban'] = True
        output_setup['mwf_mu'] = 1  # trade-off parameter: a scalar value or 'rnn'
        output_setup['evd'] = False # rank-1 reconstruction or not
        output_setup['gevd'] = True
        output_setup['vs_Qrank'] = 1 #
        signal = self.get_all_wav_data()
        all_sig = np.stack([self.stft_if.compute_stft(signal[ele]) for ele in signal], 0)
        Y_hat = gev_wrapper_on_masks(np.moveaxis(all_sig, 2,0), target_mask=mask, setup=output_setup)
        return self.stft_if.compute_istft(Y_hat.T)

    def get_all_wav_data(self):
        signal = {}
        for mic in self.mic_pair:
            if mic.mic_pair[0] not in signal:
                signal[mic.mic_pair[0]] = mic.wav_data[0]
            if mic.mic_pair[1] not in signal:
                signal[mic.mic_pair[1]] = mic.wav_data[1]
        return signal

def cos_sin_phase_diff(angle_diff):
    cos_theta = np.cos(angle_diff)
    sin_theta = np.sin(angle_diff)
    return [cos_theta, sin_theta]

def test():
    from standalone import M1, MicPair, gen_random_wav_file, STFT
    ds_mask_model = 'lstm_dropout_mag_spec_cmvn_csipd_multi_src.hdf5'
    ds_nnet = nnet_lstm(3*801, 801)
    ds_nnet.load_model(ds_mask_model)
    frame_stft_if = STFT(1600)
    mic_array = M1['mics']
    diag_mic = M1['diag'][0]
    beamform_at = M1['beamform_at']
    mic_pair_array = []
    wav1 = gen_random_wav_file()
    wav2 = gen_random_wav_file()
    for mic in mic_array:
        mic_pair = MicPair(mic, 30, 0.1, wav1, wav2)
        mic_pair_array.append(mic_pair)
    beam = Beamform(frame_stft_if, mic_pair_array, beamform_at)
    #beam.set_doa_from_doa_array()
    ds_sig = beam.ds_beamform()
    mask = beam.compute_mask_with_model_pd_feats(ds_sig, ds_nnet.model)
    #enh_sig = beam.r1_mwf(mask) # Will give singular matrices
    print('Enhancement works!')


if __name__ == '__main__':
    set_trace()
    test()

import numpy as np
import itertools
import ipdb

def polar2cart(polar):
    '''
        Convert polar in (r, theta, phi) to cartesian x, y, z
        r is radius
        theta is elevation
        phi is azimuth

    '''
    if len(polar) == 2:
        theta, phi = polar
        r = 1
    else:
        r, theta, phi = polar
    theta = np.deg2rad(theta)
    phi   = np.deg2rad(phi)
    return [r * np.sin(theta) * np.cos(phi),\
            r * np.sin(theta) * np.sin(phi),\
            r * np.cos(theta)]



class PoolDoa(object):
    ROOM_DIMENSION = np.array([7, 7])
    MIC_Z = 2.5
    GRID_RESOLUTION = 2 # in degrees
    GRID_Z_AXIS = 1.4
    SOUND_SPEED = 340 #m/s

    def __init__(self):
        elevation_max = int(np.floor(90/PoolDoa.GRID_RESOLUTION))
        elevation = [-90+(i*90/elevation_max) for i in \
                range(0, elevation_max)]
        self.theta_phi = []
        self.cartesian = []
        for phi in elevation:
            theta_phi_max = abs(int(np.floor(360 * np.cos(phi)/PoolDoa.GRID_RESOLUTION)))
            #theta_phi_max = abs(int(np.floor(360/PoolDoa.GRID_RESOLUTION)))
            theta_ij = [(phi, -180 + (j*360)/(theta_phi_max+1))\
                    for j in range(theta_phi_max)]
            #theta_ij = [(j,phi) for j in range(0, 360)]
            self.theta_phi.extend(theta_ij)
            cartesian = [polar2cart(_polar_) for _polar_ in theta_ij]
            self.cartesian.extend(cartesian)
        self.cartesian = -1 * np.array(self.cartesian)
        self.theta_phi = np.array(self.theta_phi)

        #Place the mic in the middle of the room
        self.mic_array_position = np.array([0, 0, 0])
        self.mic = {}
        self.mic[1] = self.mic_array_position + [ 0.05, 0.05, 0]
        self.mic[2] = self.mic_array_position + [-0.05, 0.05, 0]
        self.mic[3] = self.mic_array_position + [-0.05,-0.05, 0]
        self.mic[4] = self.mic_array_position + [-0.05, 0.05, 0]
    
        self.mid_point = {}
        self.mid_point[(1,2)] = (self.mic[1] + self.mic[2])/2
        self.mid_point[(2,3)] = (self.mic[2] + self.mic[3])/2
        self.mid_point[(3,4)] = (self.mic[3] + self.mic[4])/2
        self.mid_point[(4,1)] = (self.mic[4] + self.mic[1])/2
        self.doa_all_points = self.compute_doa_all_points()
        self.avail_doa = self.pair_available_doa()

        self.diag = {}
        self.diag[(1,3)] = (self.mic[1] + self.mic[3])/2
        self.diag[(2,4)] = (self.mic[2] + self.mic[4])/2

    def pair_available_doa(self):
        doa_available = {}
        for pairs in self.doa_all_points:
            doa_available[pairs] = {}
            doa_available[pairs]['avail'] = \
                   np.sort(np.unique(self.doa_all_points[pairs]['doa']))
            doa_map_idx = np.searchsorted(doa_available[pairs]['avail'], range(181))
            doa_map_idx[doa_map_idx>=len(doa_available[pairs]['avail'])] = len(doa_available[pairs]['avail'])-1
            doa_map = doa_available[pairs]['avail'][doa_map_idx]
            doa_available[pairs]['closest'] = doa_map
        return doa_available


    def compute_doa_all_points(self):
        info = self.compute_doa_for_points(self.cartesian)
        return info

    def compute_doa_for_points(self, src_points):
        '''
            src_points dim is Nx3
        '''
        info = {}
        for pairs in self.mid_point:
            mid_point = self.mid_point[pairs]
            mic1, mic2 = pairs
            mic_line = self.mic[mic1] - self.mic[mic2]
            src_line = mid_point - src_points 
            src_line_norm = np.linalg.norm(src_line, axis=1)
            mic_line_norm = np.linalg.norm(mic_line)
            doa = np.einsum('j,kj->k',mic_line, src_line)/\
                    (mic_line_norm * src_line_norm)
            doa = np.round(np.rad2deg(np.arccos(doa)))
            if pairs not in info:
               info[pairs] = {}
            info[pairs]['doa'] = doa
            idx_doa = {}
            plot = False
            for angle in range(181):
                idx_doa[angle] = (doa==angle)
                if plot and idx_doa[angle].sum() > 0 :
                    grid = np.zeros((self.cartesian.shape[0]))
                    grid[idx_doa[angle]] = 1
                    self.plot_grid(grid, 'view'+str(angle)+'.png')
            info[pairs]['doa_idx'] = idx_doa
        return info

    def do_pooling_by_interpolation(self, doa_dict):
        '''
        Do the actual pooling by interpolation the doa prob values
        Input:
        doa_dict: A dictionary containing DOA values for mic pairs (1,2), (2, 3), (3,4) and (4,1)
        doa value is a vector containing probability for each DOA direction
        '''
        #grid = np.zeros((self.grid_x_len, self.grid_y_len))
        grid = np.zeros((self.cartesian.shape[0]))
        fig_idx = 0
        for pair in doa_dict:
            doa_prob = doa_dict[pair]
            doa_angle = range(len(doa_prob))
            grid_doa = self.doa_all_points[pair]['doa']
            grid += np.interp(grid_doa, doa_angle, doa_prob)
            self.plot_grid(grid, 'grid'+str(fig_idx)+'.png')
            fig_idx += 1
        return grid

    def do_pooling(self, doa_dict):
        '''
        Do the actual pooling
        Input:
        doa_dict: A dictionary containing DOA values for mic pairs (1,2), (2, 3), (3,4) and (4,1)
        doa value is a vector containing probability for each DOA direction
        '''
        #grid = np.zeros((self.grid_x_len, self.grid_y_len))
        grid = np.zeros((self.cartesian.shape[0]))
        fig_idx = 0
        for pair in doa_dict:
            doa = doa_dict[pair]
            doa_idx = self.doa_all_points[pair]['doa_idx']
            angle_map = self.avail_doa[pair]['closest']
            for doa_angle, doa_prob in enumerate(doa):
                doa_angle = angle_map[doa_angle]
                grid_idx = doa_idx[doa_angle]
                grid[grid_idx] += doa_prob
            self.plot_grid(grid, 'grid'+str(fig_idx)+'.png')
            fig_idx += 1
        return grid

    def find_best_pos(self, grid_value):
        idx = np.argmax(grid_value)
        pos = self.cartesian[idx]
        return pos
    
    def get_tdoa(self, src_pos):
        '''
            Get tdoa for all pairs, given the src position in the grid
            input:
            -----
            pos: src position in the grid
        '''
        tdoa = {}
        all_pair = {**self.mid_point, **self.diag}
        for pair in all_pair:
            mic1, mic2 = pair
            mic1_pos = self.mic[mic1]
            mic2_pos = self.mic[mic2]
            mic1_distance = np.sqrt(np.sum((mic1_pos - src_pos)**2))
            mic2_distance = np.sqrt(np.sum((mic2_pos - src_pos)**2))
            tdoa_for_pair = (mic1_distance - mic2_distance)/PoolDoa.SOUND_SPEED
            tdoa[pair] = tdoa_for_pair
        return tdoa

    def plot_grid(self, grid_value, file_name):
        from mpl_toolkits.mplot3d import Axes3D
        import matplotlib.pyplot as plt
        import matplotlib
        from matplotlib.colors import ListedColormap
        fig = plt.figure(figsize=(12, 12), dpi=90)
        ax = fig.add_subplot(111, projection='3d')
        cmap_reversed = matplotlib.cm.get_cmap('autumn_r')
        ax.scatter(\
                self.cartesian[:,0], \
                self.cartesian[:,1], \
                self.cartesian[:,2], c = grid_value, cmap=cmap_reversed)
        ax.scatter(self.mic[1][0], self.mic[1][1], self.mic[1][2], c='g', marker='^', s=90)
        ax.scatter(self.mic[2][0], self.mic[2][1], self.mic[2][2], c='g', marker='^', s=90)
        ax.scatter(self.mic[3][0], self.mic[3][1], self.mic[3][2], c='g', marker='^', s=90)
        #plt.colorbar()
        #plt.show()
        plt.savefig(file_name)
        plt.close()

        #fig = plt.figure(figsize=(12, 12), dpi=90)
        #ax = fig.add_subplot(111)
        #cmap_reversed = matplotlib.cm.get_cmap('autumn_r')
        #ax.scatter(\
        #        self.cartesian[:,0], \
        #        self.cartesian[:,1], c = grid_value, cmap=cmap_reversed)
        ##plt.colorbar()
        #file_name = ''.join(file_name.split('.')[:-1])+'2d.png'
        #plt.show()
        #plt.savefig(file_name)
        #plt.close()

class PoolDoaRect(PoolDoa):
    ROOM_DIMENSION = np.array([7, 7, 2.5])
    HGT_MAX = 1.9
    RES = np.array([46, 46, 10])

    def __init__(self):
        PoolDoa.__init__(self)
        ROOM_DIMENSION = PoolDoaRect.ROOM_DIMENSION 
        RES = PoolDoaRect.RES
        xpos = np.linspace(-ROOM_DIMENSION[0]/2, ROOM_DIMENSION[0]/2, RES[0])
        ypos = np.linspace(-ROOM_DIMENSION[1]/2, ROOM_DIMENSION[1]/2, RES[1])
        zpos = -1*np.linspace(0, ROOM_DIMENSION[2], RES[2])

        self.cartesian = np.array(list(itertools.product(xpos, ypos, zpos)))
        self.doa_all_points = self.compute_doa_all_points()
        self.avail_doa = self.pair_available_doa()

if __name__ == '__main__':
    import sys
    sys.path.append('/talc3/multispeech/calcul/users/ssivasankaran/experiments/code/sunit-code/sourceLocalization/rir_creation/vocadom_data')
    import angle_check
    ipdb.set_trace()
    #pool_if = PoolDoa()
    pool_if = PoolDoaRect()
    #src_pos = np.hstack((np.random.rand(2)*7, [1.4]))
    #src_pos = np.random.rand(3)*7 - 3.5
    #src_pos[-1] = -np.abs(src_pos[-1])
    src_pos = np.array([2,-1,-1.4])
    #src_pos = src_pos/np.linalg.norm(src_pos)
    doa_val = {
                    (1,2): 180-angle_check.compute_angle(pool_if.mic[1], pool_if.mic[2], src_pos),\
                    (2,3): 180-angle_check.compute_angle(pool_if.mic[2], pool_if.mic[3], src_pos),\
                    (3,4): 180-angle_check.compute_angle(pool_if.mic[3], pool_if.mic[4], src_pos),\
                    (4,1): 180-angle_check.compute_angle(pool_if.mic[4], pool_if.mic[1], src_pos)}

    doa_pair_vec = {}

    for pair in doa_val:
        doa_vec = np.zeros(181)
        doa = doa_val[pair]
        doa_vec[doa] = 1
        doa_pair_vec[pair] = doa_vec

    #grid_val = pool_if.do_pooling(doa_pair_vec)
    grid_val = pool_if.do_pooling_by_interpolation(doa_pair_vec)
    best_pos = pool_if.find_best_pos(grid_val)
    tdoa = pool_if.get_tdoa(best_pos)
    best_pos = best_pos/np.linalg.norm(best_pos)
    print(src_pos, src_pos/np.linalg.norm(src_pos), best_pos)
    est_doa = pool_if.compute_doa_for_points(best_pos[np.newaxis])
    true_doa = pool_if.compute_doa_for_points(src_pos[np.newaxis])
    for pair in est_doa:
        print(pair, 'est', est_doa[pair]['doa'], 'true', true_doa[pair]['doa'])



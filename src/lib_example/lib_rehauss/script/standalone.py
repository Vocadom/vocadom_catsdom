import argparse
import numpy as np
import librosa
import h5py
from keras.models import load_model as keras_load_model

from ipdb import set_trace

np.random.seed(0)
SOUND_SPEED = 340

M1 = {'mics':[(1, 2, 0), (2, 3, 90), (3, 4, 180), (4, 1, 270)],
        'diag':[(1, 3, 45)],
        'beamform_at':1}

def load_model(file_path, **kwargs):
    return keras_load_model(file_path, **kwargs)

def gen_random_wav_file(wav_len=None):
    if wav_len is None:
        wav_len = int(0.5 * 16000)
    return np.random.rand(wav_len)

def cos_sin_phase_diff(angle_diff):
    cos_theta = np.cos(angle_diff)
    sin_theta = np.sin(angle_diff)
    return [cos_theta, sin_theta]



def ph_data_to_ctm(ph_id_array, end_sample):
    final_array = []
    final_array.append((0, ph_id_array[0][1], ph_id_array[0][0]))
    for idx, ph_entry in enumerate(ph_id_array[1:]):
        final_array.append((final_array[-1][0]+ph_id_array[idx][1],\
                ph_id_array[idx+1][1], ph_id_array[idx+1][0]))
    return final_array

### Models
class STFT(object):
    '''
    Class to handle STFT creation.
    fft_dim is the same as window length
    parameters:
    -----------
    fft_dim : Number of fft samples to take
    Window length is set to fft_dim
    hop_len : Number of samples to hop
    set to 50% of fft_dim by default
    '''
    def __init__(self, fft_dim, hop_len=None):
        self.window = np.sin((np.array(range(fft_dim))+0.5)/fft_dim* np.pi)
        self.win_len = len(self.window)
        self.nfft = fft_dim
        self.stft_freq_dim = int(1+fft_dim//2)
        if hop_len is None:
            self.hop_len = int(0.5*fft_dim)
        else:
            self.hop_len = hop_len

    def compute_stft(self, signal, hop_len=None, **stft_args):
        '''
            Compute the stft of the signal.
            Hop length is an optional input. If not provided 50% overlap is
            assumed
        '''
        if hop_len is None:
            hop_len = self.hop_len
        assert len(signal.shape) == 1, 'Expecting one dimensional signal'
        stft = librosa.core.stft(signal, \
            n_fft=self.nfft, \
            hop_length=hop_len, \
            window=self.window,\
            **stft_args)
        return stft

    def compute_istft(self, stft_sig, hop_len=None, length=None):
        '''
            compute the inverse of the stft given the STFT signal
        '''
        if hop_len is None:
            hop_len = self.hop_len
        sig = librosa.core.istft(stft_sig, \
            hop_length=hop_len, \
            window=self.window,
            length=length)
        return sig

def spec_hdf5_to_dict(spec_hdf5_file):
## Dictionaries are painfull if you are doing multiprocessing. Replacingin this with np array
    fid = h5py.File(spec_hdf5_file)
    all_ids = np.sort([int(ele) for ele in list(fid['spectrum'].keys())])
    spec_dimension = len(fid['spectrum'][str(all_ids[0])])
    spec_dict = np.zeros((int(all_ids[-1])+1, spec_dimension))
    mean = None
    std = None
    if 'stats' in fid:
        mean = fid['stats/mean'][...]
        std = fid['stats/std'][...]
    spec_keys = list(fid['spectrum'].keys())
    for key in spec_keys:
        if mean is None:
            spec_dict[int(key)] = fid['spectrum'][key][...]
        else:
            spec_dict[int(key)] = (fid['spectrum'][key][...] - mean)/std
    fid.close()
    return spec_dict



class MicPair(object):
    '''
    A class to process data for each microphone pair
    '''
    def __init__(self, mic_pair, doa, distance, wav1, wav2):

        self.mic_pair = mic_pair
        self.rotate = mic_pair[2]
        self.doa = doa 
        self.distance = distance
        self.wav_data = np.array([wav1, wav2])
        self.tdoa = self.get_tdoa()


    def flip_mic(self):
        '''
            Rotate the mic pair by 180
        '''
        self.mic_pair = (self.mic_pair[1], self.mic_pair[0], None)
        self.rotate = None
        self.doa = 180 - self.doa
        self.wav_data = np.array([self.wav_data[1], self.wav_data[0]])

    def flip_mic_tdoa(self):
        '''
            Rotate the mic pair by 180
        '''
        self.mic_pair = (self.mic_pair[1], self.mic_pair[0], None)
        self.rotate = None
        self.wav_data = np.array([self.wav_data[1], self.wav_data[0]])
        self.tdoa = -self.tdoa

    def rotate_doa(self, rotate):
        assert self.doa is not None, 'DOA not set'
        new_angle = self.doa + rotate
        if new_angle >= 360:
            new_angle -= 360
        return new_angle

    def rotate_doa_to_base_angle(self):
        '''
        Rotate DOA to original orientation
        '''
        assert self.doa is not None, 'DOA not set'
        if self.rotate == 0:
            new_angle = self.doa
        else:
            new_angle = 360 - self.rotate + self.doa
        if new_angle >= 360:
            new_angle -= 360
        if new_angle > 180:
            new_angle = 360 - new_angle 
        self.doa = new_angle

    def get_tdoa(self):
        return  self.distance * np.cos(np.deg2rad(self.doa))/SOUND_SPEED


class DoaEstimation(object):
    def __init__(self, mask_model_path, doa_model_path, ph_spec_dict, \
            spec_normalizer_dict, sub_frame_dur=0.025, \
            frame_dur=0.1):
        self.fs = 16000 # Sampling rate, do not change
        self.mask_model = load_model(mask_model_path)
        self.doa_model = load_model(doa_model_path)
        self.ph_dict = spec_hdf5_to_dict(ph_spec_dict)
        sub_frame = int(sub_frame_dur * self.fs)
        self.sub_frame_stft_if = STFT(sub_frame)
        samples = int(frame_dur * self.fs)
        self.frame_stft_if = STFT(samples)
        self.spec_norm = h5py.File(spec_normalizer_dict, 'r')
        self.frame_window = int(frame_dur * self.fs)
        self.frame_shift = int(frame_dur * 0.5 * self.fs)
        self.sub_frame_window = int(sub_frame_dur * self.fs)

    def __del__(self):
        self.spec_norm.close()

    def estimate_mask(self, mask_predict_input):
        mask = self.mask_model.predict(mask_predict_input).T
        mask = np.swapaxes(mask, 0, 2)
        mask = np.swapaxes(mask, 1, 2)
        return mask



    def fake_data_test(self):
        wav1 = gen_random_wav_file()
        wav2 = gen_random_wav_file()
        fs = 16000
        phone_id = [(80, int(0.1*fs)), (82, int(.15*fs)), (123, int(0.25*fs)), (110, int(0.35*fs))]
        phone_id_ctm = ph_data_to_ctm(phone_id, int(0.5*fs))
        pad = int(self.frame_stft_if.nfft/2)
        phone_subseg = align2ph_frame(phone_id_ctm, pad)
        mask_est_feats = self.create_feats_for_mask_est(wav1, wav2, phone_subseg)
        mask = self.estimate_mask(mask_est_feats)
        doa = self.estimate_doa(wav1, wav2, mask)
        return doa

    def estimate_doa(self, wav1, wav2, mask):
        wav1_stft = self.frame_stft_if.compute_stft(wav1)
        wav2_stft = self.frame_stft_if.compute_stft(wav2)
        wav_data = np.array([wav1, wav2]).squeeze()
        phase_diff = np.angle(wav1_stft) - np.angle(wav2_stft)
        csipd = np.stack(cos_sin_phase_diff(phase_diff))
        csipd = np.expand_dims(csipd, 0)
        csipd[:, 0, :, :] = (csipd[:, 0, :, :].squeeze() * mask.squeeze())
        csipd[:, 1, :, :] = (csipd[:, 1, :, :].squeeze() * mask.squeeze())
        csipd = np.float32(csipd)
        doa = self.doa_model.predict(csipd)
        return doa[0].sum(0).argmax()


    def compute_sub_spec(self, wav_data, window=1600, shift=800):
        '''
            Creates stft frames 
            wav_data is in the format [number_of_channels x time]
            returns [ channel x 100ms frame count x fft_size x 25ms frame count]
        '''
        assert len(wav_data.shape) == 2, \
                'wav data should be of format channel x time'
        spec = []
        segment_stft_if = self.frame_stft_if
        sub_seg_stft_if = self.sub_frame_stft_if
        for wav in wav_data.T:
            wav = np.pad(wav, int(segment_stft_if.nfft // 2), mode='reflect')
            wav_data_frame = librosa.util.frame(wav, window, shift)
            sub_spec = []
            for sub_wav in wav_data_frame.T:
                sub_wav_stft = np.abs(sub_seg_stft_if.compute_stft(sub_wav))
                sub_wav_stft = sub_wav_stft[:, :self.mask_model.input.shape[2]]
                sub_spec.append(sub_wav_stft)
            spec.append(np.array(sub_spec))
        return np.array(spec)

    def generate_random_phones(self, dim):
        number_of_elements = np.prod(dim)
        ph_index = np.random.choice(self.ph_dict.shape[0],\
                number_of_elements)
        #ph_idx = np.array([self.ph_dict[_id] for _id in ph_index])
        return ph_index.reshape(dim[0], dim[1])


    def compute_phone_spec(self, phones):
        '''
            Get phonetic spectrum from phone sequence
            phones are of shape []
        '''
        spec_dict = self.ph_dict
        #dict_val = next(iter(spec_dict.values()))
        out_shape = (phones.shape[0], phones.shape[2], spec_dict.shape[1], \
                phones.shape[1])
        output = np.zeros(out_shape)
        phones = np.int16(phones)
        for sample_idx in range(phones.shape[0]):
            for frame_idx in range(phones.shape[1]):
                for sub_frame_idx in range(phones.shape[2]):
                    output[sample_idx, sub_frame_idx, :, frame_idx] = \
                            spec_dict[phones[sample_idx, frame_idx, \
                            sub_frame_idx]]
        return output


    def create_feats_for_mask_est(self, wav1_data, wav2_data, phones=None):
        '''
            phones should be of dimension [long_frame x short_frame]
        '''
        wav1_stft = self.frame_stft_if.compute_stft(wav1_data)
        wav2_stft = self.frame_stft_if.compute_stft(wav2_data)
        wav_data = np.array([wav1_data, wav2_data]).squeeze()
        phase_diff = np.angle(wav1_stft) - np.angle(wav2_stft)
        subseg_spec = self.compute_sub_spec(wav_data.T)
        #subseg_spec = subseg_spec[:, :, :, :phones.shape[1]]
        if phones is None:
            phones = self.generate_random_phones((subseg_spec.shape[1], \
                subseg_spec.shape[-1]))
        phones = np.expand_dims(phones, axis=0)
        phone_spec = self.compute_phone_spec(phones)

        mean0 = self.spec_norm['mean/src_reverb_subseg_spec'][str(1)][...]
        mean1 = self.spec_norm['mean/src_reverb_subseg_spec'][str(3)][...]
        mean = [np.swapaxes(np.tile(mean0, (1, 1, 1, 1)), 2, 3), \
                    np.swapaxes(np.tile(mean1, (1, 1, 1, 1)), 2, 3)]
        std0 = self.spec_norm['std/src_reverb_subseg_spec'][str(1)][...]
        std1 = self.spec_norm['std/src_reverb_subseg_spec'][str(3)][...]
        std = [np.swapaxes(np.tile(std0, (1, 1, 1, 1)), 2, 3), \
                    np.swapaxes(np.tile(std1, (1, 1, 1, 1)), 2, 3)]
        subseg_spec[0] = (subseg_spec[0] - mean[0])/std[0]
        subseg_spec[1] = (subseg_spec[1] - mean[1])/std[1]
        mix_spec_data = np.expand_dims(subseg_spec, axis=2)

        mix_spec_data = np.swapaxes(mix_spec_data, 1, 4)
        mix_spec_data = np.swapaxes(mix_spec_data, 1, 2)
        mic1_data = np.expand_dims(mix_spec_data[0], axis=0)
        mic2_data = np.expand_dims(mix_spec_data[1], axis=0)
        phone_spec = np.expand_dims(phone_spec,axis=0)
        feats = np.vstack((mic1_data, phone_spec, mic2_data))
        feats = np.float32(np.swapaxes(feats, 0, 1))
        return feats

def align2ph_frame(phone_alignment, pad_size, \
         segment_size=0.5, frame_size=0.1, frame_shift=0.05,\
         sub_frame_size=0.025, sub_frame_shift=0.010, SAMPLING_RATE=16000):
     '''
     create phoneme frames
     creates a representative signal of phone sequence, breaks it into 
     set of frames and then decide the phone id based on the maximum number
     of samples associated to a ph
     '''
     sub_sampling = 1.0
     ph_window_samples = int(sub_frame_size*SAMPLING_RATE)
     ph_shift_samples = int(sub_frame_shift*SAMPLING_RATE)
     ph_sample_seq = []
     max_sample = int(segment_size * SAMPLING_RATE)
     frame_size_sample = int(frame_size * SAMPLING_RATE)
     frame_shift_sample = int(frame_shift* SAMPLING_RATE)
     for ph_align in phone_alignment:
         sample_cnt = int(ph_align[1])
         ph_sample_seq += [ph_align[2]] * sample_cnt
         if len(ph_sample_seq) >= max_sample:
             break
     ph_sample_seq = ph_sample_seq[:max_sample]
     ph_sample_seq = np.pad(ph_sample_seq, pad_size,  mode='reflect')

     frames = librosa.util.frame(np.array(ph_sample_seq),\
             frame_size_sample, frame_shift_sample)
     ph_seq = []
     for sub_frame in frames.T:
         ph_frames = librosa.util.frame(sub_frame,\
                 ph_window_samples, ph_shift_samples)
         ph_sub_frame_seq = []
         for ph_frame in ph_frames.T:
             ph, cnt = np.unique(ph_frame, return_counts=True)
             ph_sub_frame_seq.append(int(ph[np.argmax(cnt)]))
         ph_seq.append(ph_sub_frame_seq)
     return np.array(ph_seq)




def interface():
    parser = argparse.ArgumentParser('Compute DOA using multichannel data')
    parser.add_argument('mask_model', help='Mask estimation model')
    parser.add_argument('doa_model', help='doa estimation model')
    parser.add_argument('ph_spec_dict', \
            help='HDF5 file containing phone spectra')
    parser.add_argument('spec_norm', \
            help='HDF5 file containing mean and std norm for spectra')
    args = parser.parse_args()
    #mask_nnet_object = CRNNMask.load_model(args.mask_model)
    #mask_nnet_object = load_model(args.mask_model)
    #doa_object = load_model(args.doa_model)
    #doa_if = DoaEstimation(mask_nnet_object, doa_object, \
    doa_if = DoaEstimation(args.mask_model, args.doa_model, \
            args.ph_spec_dict, args.spec_norm)
    #return doa_if
    # set_trace()
    print(doa_if.fake_data_test())

if __name__ == '__main__':
    interface()

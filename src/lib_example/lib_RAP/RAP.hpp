#ifndef RAP_HPP
#define RAP_HPP

#include <iostream>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#define MACHINE "RAP" // Module's name
#define PORTKALDI 8000	       //Kadi Server 
#define SA struct sockaddr 
#define SCRIPT_FILE "script/motsT.sh"
#define NBR_BYTES_TO_RCV 4096
/* ====================
	Prototypes
   ==================== */ 
extern "C" int RAP_init();
extern "C" int RAP_preprocess();
extern "C" int RAP_process();
extern "C" int RAP_postprocess();
extern "C" int RAP_finish();

#endif //RAP_HPP

#include "RAP.hpp"
#include "tcpConnection.hpp"
#include "config.hpp"
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>
#define CHUNK 8000 // correspond to 0.5 seconds

std::string inputFolder;
std::string outputFolder;
std::string rawFile;
std::string rawFileFolder;
std::string command, creaLog;
const std::string file = "VocadomConf.xml";

// port for VAD
static int sock_vad;
static int port_vad;
static std::string adress_vad;

//socket connected to vad server
static int sock_rap;

//rap server port 
static int port_rap;
static std::string  adress_rap;

//nlu server port
static int port_nlu;
static std::string adress_nlu;
//reco parole 
static int all_sockets_kaldi;
static int all_ports_kaldi;
static int port_kaldi;
static int socket_kaldi;
static std::string scriptFile;

//gestion de thread
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;          //verrouillage de valeur
static pthread_cond_t condition, conditionn = PTHREAD_COND_INITIALIZER;       // condition use to synchronize the connection with servers

std::string getTime()
{
  time_t curr_time;
  tm * curr_tm;
  char time_string[100];
  time(&curr_time);
  curr_tm = localtime(&curr_time);	
  std::strftime(time_string, 50, "%Y-%m-%d_%H-%M-%S", curr_tm);

  return std::string(time_string);
}

/* ----------------------------------------------------------------------
    Thread : Connection to VAD
   ---------------------------------------------------------------------- */
void* server_rap(void *)
{
    int buffer[CHUNK];
    bool connection = false;
// output file : out.txt
    std::string outputFile = outputFolder + "/Commande.txt";
    rawFileFolder = outputFolder + "/Commande_Raw";
    std::string output = outputFolder;
    std::string input = inputFolder;
    std::string logFolder = outputFolder + "/logRAP/";
    std::string logFile = outputFolder + "/logRAP.txt";
    
    //Numéro fichier
    int num=0;

    // taille du fichier raw
    int lengthFileRaw;
    //Socket TCP
    int nb_byte_read;

    while(1)
    {
	int sock_vad = accept(sock_rap, NULL, NULL);
	if(sock_vad == -1)
	{
	    perror("sock VAD");
	    pthread_exit(NULL);	
	}
        
	connection = true;
	
	while(connection)
	{
	   	int nb_read = 0;

	    while(nb_read < CHUNK)
	    {
		
		int ret = read(sock_vad, buffer + nb_read, CHUNK - nb_read);
		
		if(ret <= 0)
		{
		    std::cout << "VAD disconnected" << std::endl;
		    connection = false;
		    break;
		}
		nb_read += ret;
		
	    }   
	}

	
// Resetting output folder 
    if(!boost::filesystem::exists(output))                           
    {
    boost::filesystem::create_directory(output);
    //boost::filesystem::create_directory(logFolder);
    }
    boost::filesystem::ofstream currentOutFile{outputFile};
    
    
//reinitialisation du dossier raw
    if(boost::filesystem::exists(rawFileFolder))
    {
        boost::filesystem::remove_all(rawFileFolder);    
    }
    boost::filesystem::create_directory(rawFileFolder);
    
//ouverture du fichier 
    std::ifstream inputFileW(input);  
//conversion wav raw
    if (inputFileW.is_open())
    {
        //num++;
        //rawFile = rawFileFolder + "/Commande"+std::to_string(num)+".raw";
        rawFile = rawFileFolder + "/Commande.raw";
        command = "sox " + input + " -t raw -c 1 -b 16 -r 16k -e signed-integer " + rawFile;

//execution commande wav to raw        
        if(system(NULL))
        {
            system(command.c_str());
        }

        inputFileW.close();
    }
    else
    {
        std::cout << "Opening " << input << " Failed!" << std::endl;
        
    }

      //connection on port kaldi
    socket_kaldi = connect_tcp_client(port_kaldi,"127.0.0.1");
    if(0 > socket_kaldi)
    {
            std::cout <<"Failed to connect to Kaldi " << port_kaldi << std::endl;
           
     }
 
    std::ifstream inputFileR(rawFile);
    
    
    if(inputFileR.is_open())
    {
      inputFileR.seekg (0, inputFileR.end);
      lengthFileRaw = inputFileR.tellg();
      inputFileR.seekg (0, inputFileR.beg);
      char bufferWrite [lengthFileRaw];

      // lecture du fichier RAW
      inputFileR.read(bufferWrite,lengthFileRaw);


      // Envoie des données au serveur
      if(write(socket_kaldi, bufferWrite, lengthFileRaw))
	std::cout << "Finished to send raw file." << std::endl;
      else
	std::cout << "SEND FAILED" << std::endl;  

      std::cout <<"Waiting for an answer from server..." << std::endl;      
    }
   
	
    /*poll_sock.fd = socket_kaldi;
    poll_sock.events = POLLIN;*/

              
//Lecture de la réponse du serveur
//

/* for one itterance such as " minouche allume la lumière [long silence]  Minouche fait du café"  kaldi's output looks like this :
minouche\r
minouche allume la lumière\n
Minouche fait \r
Minouche fait du café\n

where \r separates temporary hypotheses about a transcription
where \n terminate the final transcription

so the code below looks for \n and \r in order to retrieve only the hypotheses between \r hypo \n.  (when \r is not present the beginning is the beginning of the string)

*/

    char* bufferTempo=NULL;
    int k=0;
    int i_buff=0;
    std::string tmp = "";

    int last_start_pos;
    char bufferRecv[NBR_BYTES_TO_RCV]; // received buffer from server

    while((nb_byte_read = read(socket_kaldi, (char*) bufferRecv, NBR_BYTES_TO_RCV)) > 0)
    {
      tmp += std::string(bufferRecv);
      memset(bufferRecv,0x00,NBR_BYTES_TO_RCV);

      if(tmp.find('\n') != std::string::npos)
      {
	int start, end;
	end = 0;

	while((start = tmp.find_first_not_of('\r', end)) != std::string::npos)
	{
	    end = tmp.find('\r',start);

	    if(start > 0)
	    {
	      last_start_pos = start;
	    }
        }
      }
    }
    int socket_nlu = connect_tcp_client(port_nlu, adress_nlu);

    std::string cmd = tmp.substr(last_start_pos);
    std::cout << "command is" << cmd << std::endl;
    if(socket_nlu > 0)
    {
      int ret = write(socket_nlu,cmd.c_str(),cmd.size());
    }

    close(socket_nlu);
    close(socket_kaldi);
    
    std::ofstream file_out;
    file_out.open(logFile, std::ios_base::app);
    file_out << getTime() ;
    file_out << " "+ cmd <<std::endl;
    }
}
/* ----------------------------------------------------------------------
    Thread : Run Kaldi
   ---------------------------------------------------------------------- */
void* run_kaldi(void*)
{
    std::string command;

    command = scriptFile + " " + std::to_string(port_kaldi);

    if(system(NULL))
        system(command.c_str());
    
    if (wait(NULL) == -1) {
        perror("wait :");
        exit(EXIT_FAILURE);
    }
   pthread_exit(NULL);
}
int RAP_init()
{
    if(GetPort(file, MACHINE, "PortVAD", port_vad) != 1)
    {
        std::cout <<"Failed to get port of VAD " << MACHINE << std::endl;
        return -1;
    }

    if(GetAdress(file, MACHINE, "AdressVAD", adress_vad) != 1)
    {
        std::cout <<"Failed to get adress of VAD " << MACHINE << std::endl;
        return -1;
    }

//connexion RAP
    if(GetPort(file, MACHINE, "PortRAP", port_rap) != 1)
    {
        std::cout <<"Failed to get port of RAP " << MACHINE << std::endl;
        return -1;
    }

    if(GetAdress(file, MACHINE, "AdressRAP", adress_rap) != 1)
    {
	std::cout << "Failed to get adress of RAP" << std::endl;
    }
//connexion Kalid    
    if(GetScriptKaldi(file, MACHINE, scriptFile) != 1){
        scriptFile= SCRIPT_FILE;
        std::cout << "Failed to find scriptFile : " <<scriptFile<< std::endl;
    }

    if(GetPort(file, MACHINE, "PortKaldi0", port_kaldi) != 1){
        std::cout << "Failed to find portKaldi : " << std::endl;
        return -1 ;
    } 
//connexion 
    if(GetPort(file, MACHINE, "PortNLU", port_nlu) != 1)
    {
        std::cout <<"Failed to get port of NLU " << MACHINE << std::endl;
        return -1;
    }

    if(GetAdress(file, MACHINE, "AdressNLU", adress_nlu) != 1)
    {
	std::cout << "Failed to get adress of NLU" << std::endl;
    }
//connexion input output files
    if(GetConfig(file, MACHINE, inputFolder, outputFolder) == -1)
		return -1;  
    return 1;
}

int RAP_preprocess()
{
    uint8_t ACK;
    std::cout << "preprocess" << std::endl;
    sock_rap = create_tcp_server(port_rap, adress_rap);
    std::cout << "port rap "<<port_rap <<" adress rap "<<adress_rap<< std::endl;
    if(sock_rap < 0)
    {
	return -1;
    }

    return 1;
}
int RAP_process()
{
    pthread_t thread_server_rap, thread_kaldi;
    int  new_thread_server_rap, new_thread_kaldi;
    
//début connexion VAD to RAP      
    new_thread_server_rap = pthread_create( &thread_server_rap , NULL ,  server_rap , NULL); 
    if (new_thread_server_rap)
    {
	std::cout << "thread client to stamp stream failed" << std::endl;
	return -1;
    }

//fin

// partie pour kaldi
    if (sock_vad != -1)
    {
    new_thread_kaldi = pthread_create( &thread_kaldi , NULL ,  run_kaldi , NULL);

    if(new_thread_kaldi)
    {
	std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
	return EXIT_FAILURE;
    }
    
    if (pthread_join(thread_kaldi, NULL)) 
    {
	perror("pthread_join");
	return EXIT_FAILURE;
    }
    
    }
//fin     
return 1;
}

int RAP_postprocess()
{
    return 1;
}
int RAP_finish()
{
    return 1;
}

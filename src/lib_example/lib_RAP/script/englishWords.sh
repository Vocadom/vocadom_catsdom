#!/bin/bash

../3rd_party/online2-tcp-nnet3-decode-faster --samp-freq=8000 --frames-per-chunk=20 --extra-left-context-initial=0 --frame-subsampling-factor=3 --config=../Models/EnglishWords/conf/online.conf --min-active=200 --max-active=7000 --beam=15.0 --lattice-beam=6.0 --acoustic-scale=1.0 --port-num=5050 ../Models/EnglishWords/final.mdl ../Models/EnglishWords/graph/HCLG.fst ../Models/EnglishWords/graph/words.txt

#!/bin/bash

../3rd_party/online2-tcp-nnet3-decode-faster --samp-freq=16000 --read-timeout=1 --frames-per-chunk=20 --extra-left-context-initial=0 --frame-subsampling-factor=3 --config=../Models/ModeleFr/conf/online.conf  --min-active=200 --max-active=7000 --beam=5 --lattice-beam=6.0 --acoustic-scale=1.0 --port-num=5008 ../Models/ModeleFr/final.mdl ../Models/ModeleFr/graph/HCLG.fst ../Models/ModeleFr/graph/words.txt


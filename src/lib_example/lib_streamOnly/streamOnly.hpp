/*
 * @Author: Clémence VIAL 
 * @Date: 2019-11-23 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2019-11-25 11:08:28 
 */

#ifndef STREAMONLY_HPP
#define STREAMONLY_HPP

#include <iostream>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

/* ====================
	Constants
   ==================== */  
#define MACHINE "StreamOnly"							// lib's name
#define PORT 5050   										// server's port
#define SA struct sockaddr 									// server's struct contains its informations
#define NBR_CLIENTS 10										// server's max number clients which can be accepted 
#define NBR_BYTES_TO_RCV 1000

/* ====================
	Prototypes
   ==================== */ 
extern "C" int streamOnly_init();
extern "C" int streamOnly_preprocess();
extern "C" int streamOnly_process();
extern "C" int streamOnly_postprocess();
extern "C" int streamOnly_finish();

#endif
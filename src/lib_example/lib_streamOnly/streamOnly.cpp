
/*
 * @Author: Clémence VIAL 
 * @Date: 2019-11-23 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2019-11-25 11:08:28 
 */

#include "streamOnly.hpp"
#include "config.hpp"
#include "SoundSync.hpp"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>                   // Using for signal(SIGPIPE, SIG_IGN)-> not stop when the server writing fails
#include <fcntl.h>                      // using for open file with O_RDONLY, S_IRUSR, S_IWUSR, S_IROTH
#include <pthread.h>
#include <string.h>

std::string inputFolder;
std::string outputFolder;
std::string file = "VocadomConf.xml";
int portServer=0;

int sock;                                          // client's socket descriptor
pthread_cond_t condition = PTHREAD_COND_INITIALIZER;    // mutex condition to waiting new client connexion
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;


int start_SendWavFile(int filefd){

    uint8_t bufferRcv[NBR_BYTES_TO_RCV];
    int nbBytesWrite;
    int nbBytesRead;

    while(1)
    {
        nbBytesRead = read(filefd, (char*) bufferRcv, NBR_BYTES_TO_RCV);

        if(nbBytesRead <= 0)
            break;

        nbBytesWrite = write(sock, bufferRcv, nbBytesRead);
        
        if(0 >= nbBytesWrite)
            break;
    }
            
    return nbBytesRead;  
}

/* ----------------------------------------------------------------------
    Thread : Data's reception of one client 
             Detect if the client is deconnected
             If the client is deconnected, it is deleted from socket client Tab
             [in] socket client
   ---------------------------------------------------------------------- */
void *receiveData(void *){  
 
    uint8_t bufferRecv[NBR_BYTES_TO_RCV];                                          // received buffer from server
    int nbBytesRead;
    std::string output = outputFolder;                              // output folder will contains out.txt
    std::string outputFile = output + "/" + "Out.txt";              // output file : out.txt

    if(boost::filesystem::exists(output))                           // Resetting output folder 
    {
        boost::filesystem::remove_all(output);    
    }
    boost::filesystem::create_directory(output);

    boost::filesystem::ofstream currentOutFile{outputFile};         // openning outputFile

    while((nbBytesRead = read(sock, (char*) bufferRecv, NBR_BYTES_TO_RCV)) > 0){
        bufferRecv[nbBytesRead] = 0x00;
        currentOutFile << bufferRecv << std::endl;            // Adding server's answer to output file
    }

    std::cout << "Server exit" << std::endl; 

    close(sock);                                        // client socket is closed

    pthread_exit(NULL);
}

/* ----------------------------------------------------------------------
    Thread : Sending WAV file to socket clients 
   ---------------------------------------------------------------------- */
void *sendData(void*){
    
    int filefd;                                                 // Input file descriptor
    int err;                                                    // error return by start_SoundSync
    std::string input = inputFolder;                            // Contains WAV files
    SoundInfo currentSoundInfo;                                 // Information about Sound in WAV file's header
    std::vector<std::string> currentFileList;                   // List of file in Input folder
    std::string currentFile;                                    // current WAV file

    GetFileList(currentFileList, input, ".wav");                // List of input files
    
    while(1)
     {
        //foreach input file we send to the clients
        BOOST_FOREACH(currentFile, currentFileList)
        {              
            // Openning file 
            filefd = open(currentFile.c_str(), O_RDONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

            init_SoundSync(filefd, &currentSoundInfo);        // Info (numChannels, freq, nbBits) about the file .WAV

            std::cout << "WAV FILE : " << currentFile.c_str() << std::endl;
            std::cout << "NbrChannel : "<< currentSoundInfo.numChannels << std::endl;
            std::cout << "Freq : " << currentSoundInfo.sampleRate << std::endl;
            std::cout << "NbBits :" << currentSoundInfo.bitDepth << std::endl;
            std::cout << "Sending..." << std::endl;

            if(filefd < 0)
            {
                std::cout << "Can't open file:" << strerror(errno) << std::endl; 
                break; 
            }
             
            err = start_SendWavFile(filefd);      
       
            if(0 > err)
                std::cout << "Failed to read file:" << strerror(errno) << std::endl;

            else if(0 < err){
                std::cout << "Failed to send file to the server:" << strerror(errno) << std::endl;
                close(filefd);
                break;
            }

            else
               std::cout << "finished.\n"  << std::endl ;
        
            close(filefd);
        } 

        if(0 < err)
           break;
    }

    pthread_exit(NULL);
}


int streamOnly_init()
{
    if(GetPorts(file, MACHINE, "PortKaldi", &portServer, 1) == -1)
        portServer = PORT;

    return GetConfig(file, MACHINE, inputFolder, outputFolder);
} 

int streamOnly_preprocess()
{
       return 1;
}

int streamOnly_process()
{
    std::cout << "Running StampStreamWav" << std::endl;

    socklen_t len;                                      // client size
    struct sockaddr_in server;                          // Informations about server
    int new_thread_rcv;                                 // return after thread creation
    int new_thread_send;                                // return after thread creation
    pthread_t thread_rcv;
    pthread_t thread_send;
    int err = errno;                                    // error's indications about socket connection 

    /* Creation du socket tcp client */
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(err != 0){
        std::cout << "Failed to create socket client... ERREUR : " << strerror(errno) << std::endl;
        close(sock);
        return EXIT_FAILURE;
    }

    /*Configuration de la connexion au serveur */
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons(portServer);

    /* Connexion au serveur distant */
    if (connect(sock, (struct sockaddr*)&server, sizeof(server) )!= 0){
        std::cout << "Failed to connect to the server..." << strerror(errno) << "port server :" << portServer << std::endl;
        close(sock);
        return EXIT_FAILURE;
    }

    std::cout << "Connected to the server..." << std::endl;

    // thread is created to receive data from server
    new_thread_rcv = pthread_create( &thread_rcv, NULL ,  receiveData , NULL);
    
    if(new_thread_rcv)
    {
        std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
        return EXIT_FAILURE;
    }

    // thread is created to send wav file to server
    new_thread_send = pthread_create( &thread_send, NULL ,  sendData , NULL);

    if(new_thread_send)
    {
        std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
        return EXIT_FAILURE;
    }

    if (pthread_join(thread_send, NULL) || pthread_join(thread_rcv, NULL)) 
    {
        perror("pthread_join");
        return EXIT_FAILURE;
    }

    
    return EXIT_SUCCESS;   
}

int streamOnly_postprocess()
{
    return 1;
}

int streamOnly_finish()
{
    return 1;
}
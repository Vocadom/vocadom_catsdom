/*
 * @Author: Clémence VIAL 
 * @Date: 2019-11-23 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2019-11-25 11:08:28 
 */

#ifndef RECOMOTSCLESV2_HPP
#define RECOMOTSCLESV2_HPP

#include <iostream>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

/* ====================
	Constants
   ==================== */  
#define SCRIPT_FILE "script/ichefix"		// command shell to execute a model Kaldi (example : model for ichefix word)
#define MACHINE "RecoMotsClesV2"			// Module's name
#define PORTKALDI 5050						// Kaldi server's port 
#define NUMBER_KALDI 4

/* ====================
	Prototypes
   ==================== */ 
extern "C" int recoMotsClesV2_init();
extern "C" int recoMotsClesV2_preprocess();
extern "C" int recoMotsClesV2_process();
extern "C" int recoMotsClesV2_postprocess();
extern "C" int recoMotsClesV2_finish();

#endif //RECOMOTSCLES_HPP
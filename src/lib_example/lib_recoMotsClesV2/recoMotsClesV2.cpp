/*
 * @Author: Clémence VIAL 
 * @Date: 2019-11-23 14:08:28 
 * @Last Modified by: Clémence VIAL
 * @Last Modified time: 2019-11-25 11:08:28 
 */

#include "recoMotsClesV2.hpp"
#include "config.hpp"
#include <unistd.h>
#include <string.h>

static std::string scriptFile;                 // command shell to execute a model Kaldi (example : model for ichefix word)
static std::string file = "VocadomConf.xml";
static int portAllKaldi[NUMBER_KALDI];                          // server's port


/**
 * @brief launchKaldi thread: Launch each kaldi with different port
 * @param port kaldi port
 * @return
 */
void* launchKaldi(void* port)
{
    std::string command;
    int portKaldi = *(int *) port;
    command = scriptFile + " " + std::to_string(portKaldi);
    std::cout << "Command Kaldi : " << command <<  std::endl;

    if(system(NULL))
        system(command.c_str());    // Script execution (ex : online2-tcp-nnet3-decode-faster) which launch TCP srever


    pthread_exit(NULL);
}

int recoMotsClesV2_init()
{
    if(GetScriptKaldi(file, MACHINE, scriptFile) != 1){
        scriptFile= SCRIPT_FILE;
        std::cout << "Failed to find scriptFile : " << std::endl;
    }

    if(GetPorts(file, MACHINE, "PortKaldi", portAllKaldi, NUMBER_KALDI) != 1){
        std::cout << "Failed to find portKaldi : " << std::endl;
        return -1 ;
    }

    return 1;
} 

int recoMotsClesV2_preprocess()
{
    return 1;
}

int recoMotsClesV2_process()
{
    std::cout << "Running RecoMotsClesV2" << std::endl;

    pthread_t thread_kaldi[NUMBER_KALDI];
    int new_thread_kaldi[NUMBER_KALDI];

    for(int i =0; i< NUMBER_KALDI; i++)
    {
        new_thread_kaldi[i] = pthread_create( &thread_kaldi[i] , NULL ,  launchKaldi , (void *) &portAllKaldi[i]);
        if(new_thread_kaldi[i])
        {
            std::cout << "Failed to create thread:" << strerror(errno) << std::endl;
            return EXIT_FAILURE;
        }
    }

    for(int i =0; i< NUMBER_KALDI; i++)
    {
        if (pthread_join(thread_kaldi[i], NULL)) 
        {
            perror("pthread_join");
            return EXIT_FAILURE;
        }
    }

    return 1;
}

int recoMotsClesV2_postprocess()
{
    return 1;
}

int recoMotsClesV2_finish()
{
    return 1;
}

/*
 * @Author: Tristan MORVAN 
 * @Date: 2019-01-29 14:30:37 
 * @Last Modified by: Tristan MORVAN
 * @Last Modified time: 2019-01-29 16:26:12
 */

#include "ConfigurationManager.hpp"
#include <boost/foreach.hpp>
#include <iostream>
#include <stdlib.h>

ConfigurationManager::ConfigurationManager()
{
    read_xml(file, ptree, boost::property_tree::xml_parser::trim_whitespace);
}

void ConfigurationManager::SetConfigurationFile(const std::string &file)
{
    this->file = file;
    if (!boost::filesystem::exists(file))
    {
        throw std::logic_error("Configuration file must exist");
    }
    read_xml(file, ptree, boost::property_tree::xml_parser::trim_whitespace);
}
bool ConfigurationManager::DoesMachineExist(const std::string &machine)
{
    BOOST_FOREACH (const boost::property_tree::ptree::value_type &v, ptree.get_child("VocadomConfiguration"))
    {
        const boost::property_tree::ptree &attributes = v.second.get_child("<xmlattr>");
        BOOST_FOREACH (const boost::property_tree::ptree::value_type &at, attributes)
        {
            if (at.second.data() == machine)
                return true;
        }
    }
    return false;
}
std::string ConfigurationManager::GetMachineName(const std::string &machine)
{
    return machine;
}
std::string ConfigurationManager::GetMachineType(const std::string &machine)
{
    BOOST_FOREACH (const boost::property_tree::ptree::value_type &v, ptree.get_child("VocadomConfiguration"))
    {
        const boost::property_tree::ptree &attributes = v.second.get_child("<xmlattr>");
        BOOST_FOREACH (const boost::property_tree::ptree::value_type &at, attributes)
        {
            if (at.second.data() == machine)
            {
                BOOST_FOREACH (const boost::property_tree::ptree::value_type &vs, v.second)
                {
                    if (vs.first == "Type")
                        return vs.second.data();
                }
            }
        }
    }
    return "none";
}
std::string ConfigurationManager::GetMachineInput(const std::string &machine)
{
    BOOST_FOREACH (const boost::property_tree::ptree::value_type &v, ptree.get_child("VocadomConfiguration"))
    {
        const boost::property_tree::ptree &attributes = v.second.get_child("<xmlattr>");
        BOOST_FOREACH (const boost::property_tree::ptree::value_type &at, attributes)
        {
            if (at.second.data() == machine)
            {
                BOOST_FOREACH (const boost::property_tree::ptree::value_type &vs, v.second)
                {
                    if (vs.first == "Input")
                        return vs.second.data();
                }
            }
        }
    }
    return "none";
}
std::string ConfigurationManager::GetMachineOutput(const std::string &machine)
{
    BOOST_FOREACH (const boost::property_tree::ptree::value_type &v, ptree.get_child("VocadomConfiguration"))
    {
        const boost::property_tree::ptree &attributes = v.second.get_child("<xmlattr>");
        BOOST_FOREACH (const boost::property_tree::ptree::value_type &at, attributes)
        {
            if (at.second.data() == machine)
            {
                BOOST_FOREACH (const boost::property_tree::ptree::value_type &vs, v.second)
                {
                    if (vs.first == "Output")
                        return vs.second.data();
                }
            }
        }
    }
    return "none";
}

int ConfigurationManager::GetSampleRate(const std::string &machine)
{
    BOOST_FOREACH (const boost::property_tree::ptree::value_type &v, ptree.get_child("VocadomConfiguration"))
    {
        const boost::property_tree::ptree &attributes = v.second.get_child("<xmlattr>");
        BOOST_FOREACH (const boost::property_tree::ptree::value_type &at, attributes)
        {
            if (at.second.data() == machine)
            {
                BOOST_FOREACH (const boost::property_tree::ptree::value_type &vs, v.second)
                {
                    if (vs.first == "SampleInMilliSeconds")
                        return atoi(vs.second.data().c_str());
                }
            }
        }
    }
    return 0;
}

std::list<std::string> ConfigurationManager::GetListOfPlugins(const std::string &machine)
{
    std::list<std::string> listOfPlugins;
    BOOST_FOREACH (const boost::property_tree::ptree::value_type &v, ptree.get_child("VocadomConfiguration"))
    {
        const boost::property_tree::ptree &attributes = v.second.get_child("<xmlattr>");
        BOOST_FOREACH (const boost::property_tree::ptree::value_type &at, attributes)
        {
            if (at.second.data() == machine)
            {
                BOOST_FOREACH (const boost::property_tree::ptree::value_type &vs, v.second)
                {
                    if (vs.first == "Plugin")
                        listOfPlugins.push_back(vs.second.data().c_str());
                }
            }
        }
    }
    return listOfPlugins;
}
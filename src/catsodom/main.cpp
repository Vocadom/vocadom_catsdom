/*
 * @Author: Tristan MORVAN
 * @Date: 2018-07-23 15:06:49
 * @Last Modified by: Tristan MORVAN
 * @Last Modified time: 2019-01-29 16:17:44
 */
#include <iostream>
#include <string>

#include "VocadomCore.hpp"

int main(int ac, char **av)
{
  std::string conf = "VocadomConf.xml";

  if (ac < 2)
  {
    std::cout << "Usage: " << av[0] << " MachineName" << std::endl;
    return -1;
  }
  VocadomCore core;
  return core.Run(av[1]);
  try
  {
  }
  catch (std::exception e)
  {
    e.what();
  }

  return 0;
}
/*
 * @Author: Tristan MORVAN 
 * @Date: 2019-01-25 15:24:25 
 * @Last Modified by: Tristan MORVAN
 * @Last Modified time: 2019-01-29 15:42:01
 */

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <string>
#include <list>

void AddMachine(boost::property_tree::ptree &tree,
                const std::string &name,
                const std::string &type,
                const std::string &input,
                const std::string &output,
                std::list<std::string> &listOfPlugins)
{
    boost::property_tree::ptree t;
    t.put("VocadomConfiguration.Machine.<xmlattr>.name", name);

    t.put("VocadomConfiguration.Machine.Type", type);
    t.put("VocadomConfiguration.Machine.Input", input);
    t.put("VocadomConfiguration.Machine.Output", output);
    if (name == "Server")
    {
        t.put("VocadomConfiguration.Machine.SampleInMilliSeconds", "10");
    }
    for (std::list<std::string>::iterator it = listOfPlugins.begin(); it != listOfPlugins.end(); ++it)
    {
        t.put("VocadomConfiguration.Machine.Plugin", *it);
    }

    tree.add_child("VocadomConfiguration.Machine", t.get_child("VocadomConfiguration.Machine"));
}

int main(int ac, char **av)
{
    boost::property_tree::ptree tree;

    std::list<std::string> pluginsServer;
    pluginsServer.push_back("acquisitor_portaudio_online");
    // pluginsServer.push_back("acquisitor_offline");
    AddMachine(tree, "Server", "Server", "wav", "SampleAudio", pluginsServer);

    std::list<std::string> pluginsClient1;
    AddMachine(tree, "Client1", "Client", "SampleAudio", "Out1", pluginsClient1);

    std::list<std::string> pluginsClient2;
    AddMachine(tree, "Client2", "Client", "Out1", "Out2", pluginsClient2);

    boost::property_tree::xml_writer_settings<std::string> settings('\t', 1);
    write_xml("VocadomConf.xml", tree, std::locale(), settings);
    return 0;
}
/*
 * @Author: Tristan MORVAN 
 * @Date: 2019-01-29 10:55:14 
 * @Last Modified by: Tristan MORVAN
 * @Last Modified time: 2019-01-31 16:00:43
 */

#include "VocadomCore.hpp"

#include <iostream>
#include <unistd.h>
#include <boost/foreach.hpp>

VocadomCore::VocadomCore()
{
}

VocadomCore::~VocadomCore()
{
}

const std::string VocadomCore::GetFirstFileFromDirectory(const boost::filesystem::path &path)
{
    boost::filesystem::recursive_directory_iterator iter(path), eod;

    BOOST_FOREACH (boost::filesystem::path const &i, std::make_pair(iter, eod))
    {
        if (is_regular_file(i))
        {
            return i.string();
        }
    }
    return "";
}


void VocadomCore::GetFileList(std::vector<std::string>& fileList,const std::string& path)
{
    
    if (!path.empty())
    {

        boost::filesystem::path apk_path(path);
        boost::filesystem::recursive_directory_iterator end;

        for (boost::filesystem::recursive_directory_iterator i(apk_path); i != end; ++i)
        {
            const boost::filesystem::path cp = (*i);
            fileList.push_back(cp.string());
        }
    }
}

void VocadomCore::LoadPlugins()
{
    std::list<std::string> listOfPluginsName = confManager.GetListOfPlugins(this->name);

    for (std::list<std::string>::iterator it = listOfPluginsName.begin(); it != listOfPluginsName.end(); ++it)
    {
        std::cout << "LOADING plugin: " << *it << std::endl;
        PluginHandler *plug = new PluginHandler("lib/", *it);
        listOfPlugins[*it] = plug;
    }
}
void VocadomCore::UnloadPlugin(const std::string &libName)
{
    listOfPlugins.erase(libName);
}

void VocadomCore::GenRandomWord(char *s, const int len)
{
    static const char alphanum[] = "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    s[len] = 0;
}

#define SIZE_WORD 20
#define NUMBER_WORD 100
#define NUMBER_FILE 100
#define CLIENT1_WORD_SIZE 8

int VocadomCore::RunServer()
{
    std::cout << "Running Server " << name << std::endl;
    boost::filesystem::path inputFolder(input);
    boost::filesystem::path outputFolder(output);
    std::string currentFile;
    int i=0;
    while (isRunning)
    {
        //RunPlugins();
        while(i < NUMBER_FILE)
        {
            currentFile = output + "/server_" + std::to_string(i++);
            boost::filesystem::path p{currentFile};
            boost::filesystem::ofstream serverFile{p};

            // Fulfill the file
            int j = 0;            
            while (j < NUMBER_WORD)
            {
                int size = rand() % SIZE_WORD;
                char* word = (char*) std::malloc(size* sizeof(char));
                GenRandomWord(word,size);
                serverFile <<  word << "\n" ;
                j++;
                free(word);
            }
        }
        std::cout << "Stoping Server!\n"; 
        exit(0);
    }
    return 0;
}

int VocadomCore::RunClient()
{
    std::cout << "Running Client " << name << std::endl;
    std::string outputFile = output + "/" + name;
    boost::filesystem::path p{outputFile};
    boost::filesystem::ofstream ouputClientFile{p};
    while (isRunning)
    {
        std::vector<std::string> currentFileList;
        GetFileList(currentFileList, input);
        BOOST_FOREACH(std::string &currentFile, currentFileList)
        {
            std::cout << currentFile << " found!" << std::endl; 
            if(name == "Client1")
            {
                std::string line;
                std::ifstream inputFile(currentFile);
                if(inputFile.is_open())
                {
                    while(getline(inputFile, line))
                    {
                        if(line.length() == CLIENT1_WORD_SIZE)
                        {
                            ouputClientFile << line << std::endl;
                        }
                    }
                    inputFile.close();
                }
                else
                {
                    std::cout << "Opening " << currentFile << " Failed!" << std::endl; 
                }
                
            }
            else if (name == "Client2")
            {

            }
        }
        return 0;
    }
    return 0;
}

void VocadomCore::RunPlugins()
{
    int ret = 0;
    for (std::map<std::string, PluginHandler *>::iterator it = listOfPlugins.begin(); it != listOfPlugins.end(); ++it)
    {
        PluginHandler::ePluginState state = (*it).second->GetState();
        switch (state)
        {
        case PluginHandler::INIT:
            std::cout << "INIT plugin " << (*it).first << std::endl;
            ret = (*it).second->Initialize();
            if (ret == -1)
            {
                std::cout << "failed to INIT plugin" << std::endl;
                (*it).second->SetState(PluginHandler::ERROR);
            }
            else if (ret == 1)
            {
                (*it).second->SetState(PluginHandler::PREPROCESS);
            }
            break;
        case PluginHandler::PREPROCESS:
            std::cout << "PREPROCESS plugin " << (*it).first << std::endl;
            ret = (*it).second->PreProcess();
            if (ret == -1)
            {
                std::cout << "failed to PREPROCESS plugin" << std::endl;
                (*it).second->SetState(PluginHandler::ERROR);
            }
            else if (ret == 1)
            {
                (*it).second->SetState(PluginHandler::PROCESS);
            }
            break;
        case PluginHandler::PROCESS:
            // std::cout << "PROCESS plugin " << (*it).first << std::endl;
            ret = (*it).second->Process();
            if (ret == -1)
            {
                std::cout << "failed to PROCESS plugin" << std::endl;
                (*it).second->SetState(PluginHandler::ERROR);
            }
            else if (ret == 1)
            {
                (*it).second->SetState(PluginHandler::POSTPROCESS);
            }
            break;
        case PluginHandler::POSTPROCESS:
            std::cout << "POSTPROCESS plugin " << (*it).first << std::endl;
            ret = (*it).second->PostProcess();
            if (ret == -1)
            {
                std::cout << "failed to POSTPROCESS plugin" << std::endl;
                (*it).second->SetState(PluginHandler::ERROR);
            }
            else if (ret == 1)
            {
                (*it).second->SetState(PluginHandler::FINISH);
            }
            break;
        case PluginHandler::FINISH:
            std::cout << "FINISH plugin " << (*it).first << std::endl;
            ret = (*it).second->Finish();
            if (ret == -1)
            {
                std::cout << "failed to FINISH plugin" << std::endl;
                (*it).second->SetState(PluginHandler::ERROR);
            }
            else if (ret == 1)
            {
                (*it).second->SetState(PluginHandler::NONE);
            }
            break;
        case PluginHandler::ERROR:
        case PluginHandler::NONE:
        default:
            UnloadPlugin((*it).first);
            break;
        }
    }
}

int VocadomCore::Run(const std::string &machineName)
{
    confManager.SetConfigurationFile("VocadomConf.xml");
    if (confManager.DoesMachineExist(machineName))
        std::cout << machineName << " exist" << std::endl;
    else
    {
        std::cout << machineName << " DOES NOT exist" << std::endl;
        return -1;
    }
    this->name = machineName;
    std::string type = confManager.GetMachineType(machineName);
    if (type == "Server")
        this->type = SERVER;
    else if (type == "Client")
        this->type = CLIENT;
    else
        this->type = UNKNOWN;
    input = confManager.GetMachineInput(machineName);
    output = confManager.GetMachineOutput(machineName);
    LoadPlugins();
    switch (this->type)
    {
    case SERVER:
        while (isRunning)
        {
            if (listOfPlugins.size() == 0)
                isRunning = false;
            else
                RunPlugins();
        }
        //return RunServer();
        break;
    case CLIENT:
        while (isRunning)
        {
            if (listOfPlugins.size() == 0)
                isRunning = false;
            else
                RunPlugins();
        }
        //return RunClient();
        break;
    case UNKNOWN:
    {
        while (isRunning)
        {
            if (listOfPlugins.size() == 0)
                isRunning = false;
            else
                RunPlugins();
        }
    }
    break;
    default:
        throw std::logic_error("Type of machine UNKNOWN");
        break;
    }
    return 0;
}
/*
 * @Author: Tristan MORVAN
 * @Date: 2018-07-23 16:33:56
 * @Last Modified by: Tristan MORVAN
 * @Last Modified time: 2018-10-24 15:24:04
 */

#include "PluginHandler.hpp"
#include <iostream>

PluginHandler::PluginHandler(std::string libraryPath, std::string libraryName)
{
    this->m_libraryPath = libraryPath;
    this->m_libraryName = "lib" + libraryName + ".so";

    LoadLibrary(libraryPath + m_libraryName, libraryName);

    std::cout << "PluginHandler load library: " << libraryPath + "/" + m_libraryName << std::endl;
}

PluginHandler::~PluginHandler()
{
}

int PluginHandler::Initialize()
{
    return CallInitialize();
}

int PluginHandler::PreProcess()
{
    return CallPreProcess();
}

int PluginHandler::Process()
{
    return CallProcess();
}

int PluginHandler::PostProcess()
{
    return CallPostProcess();
}

int PluginHandler::Finish()
{
    return CallFinish();
}

const PluginHandler::ePluginState PluginHandler::GetState()
{
    return state;
}

void PluginHandler::SetState(const ePluginState state)
{
    this->state = state;
}
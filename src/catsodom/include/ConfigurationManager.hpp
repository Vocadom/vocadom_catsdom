/*
 * @Author: Tristan MORVAN 
 * @Date: 2019-01-29 14:24:03 
 * @Last Modified by: Tristan MORVAN
 * @Last Modified time: 2019-01-29 16:25:46
 */

#ifndef CONFIGURATIONMANAGER_HPP
#define CONFIGURATIONMANAGER_HPP

#include <string>
#include <list>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

class ConfigurationManager
{
  boost::property_tree::ptree ptree;
  std::string file = "VocadomConf.xml";

public:
  ConfigurationManager();
  void SetConfigurationFile(const std::string &file);
  bool DoesMachineExist(const std::string &machine);
  std::string GetMachineName(const std::string &machine);
  std::string GetMachineType(const std::string &machine);
  std::string GetMachineInput(const std::string &machine);
  std::string GetMachineOutput(const std::string &machine);
  int GetSampleRate(const std::string &machine);
  std::list<std::string> GetListOfPlugins(const std::string &machine);
};

#endif // CONFIGURATIONMANAGER_HPP
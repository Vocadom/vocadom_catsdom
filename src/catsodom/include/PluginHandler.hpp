/*
 * @Author: Tristan MORVAN
 * @Date: 2018-07-23 16:32:28
 * @Last Modified by: Tristan MORVAN
 * @Last Modified time: 2019-02-20 14:48:56
 */
#ifndef CTSD_LIBRARYHANDLER_PLUGINHANDLER_HPP
#define CTSD_LIBRARYHANDLER_PLUGINHANDLER_HPP

#include <boost/filesystem.hpp>
#include <map>
#include <exception>
#include <string>
#include "LibraryHandler.hpp"
#include "ConfigurationManager.hpp"

// PluginHandler is used to manage a library using libraryManager

class PluginHandler : protected LibraryHandler
{
public:
  enum ePluginState
  {
    INIT = 0,
    PREPROCESS,
    PROCESS,
    POSTPROCESS,
    FINISH,
    ERROR,
    NONE,
  };

private:
  // Library manager is responsible of handling shared libraries and provid entry points adresses
  std::string m_moduleType;
  boost::filesystem::path m_libraryPath;
  std::string m_libraryName;
  std::string m_configuration;
  ePluginState state = INIT;

private:
  const std::string &GetModuleType() { return m_moduleType; }

public:
  PluginHandler() = delete;
  ~PluginHandler();
  PluginHandler(std::string libraryPath, std::string libraryName);
  PluginHandler(const PluginHandler &) = delete;
  PluginHandler &operator=(const PluginHandler &) = delete; // Disallow copying

  int Initialize();
  int PreProcess();
  int Process();
  int PostProcess();
  int Finish();

  const ePluginState GetState();
  void SetState(const ePluginState state);
};

#endif // CTSD_LIBRARYHANDLER_PLUGINHANDLER_HPP
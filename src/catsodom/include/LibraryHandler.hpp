/*
 * @Author: Tristan MORVAN
 * @Date: 2018-07-23 15:07:00
 * @Last Modified by: Tristan MORVAN
 * @Last Modified time: 2018-07-25 17:38:33
 */
#ifndef CTSD_LIBRARY_HANDLER_HPP
#define CTSD_LIBRARY_HANDLER_HPP

#include <map>
#include <string>
#include <boost/filesystem.hpp>

// LibraryHandler is used to load a dynamic library

class LibraryHandler
{
  typedef int (*m_funcPtr)();

private:
  void *libraryHandle;
  std::map<std::string, void *> mapToFunctions;
  std::string libraryFullPath;
  std::string libraryName;

private:
  void *GetFunction(const std::string &funcName);

public:
  LibraryHandler();
  ~LibraryHandler();
  LibraryHandler(const LibraryHandler &) = delete;
  LibraryHandler &operator=(const LibraryHandler &) = delete; // Disallow copying

protected:
  void LoadLibrary(const std::string &libraryFullPath, const std::string &libraryName);
  void UnloadLibrary();
  int CallInitialize();
  int CallPreProcess();
  int CallProcess();
  int CallPostProcess();
  int CallFinish();
};

#endif // CTSD_LIBRARY_HANDLER_HPP

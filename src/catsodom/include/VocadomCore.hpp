/*
 * @Author: Tristan MORVAN 
 * @Date: 2019-01-29 10:51:28 
 * @Last Modified by: Tristan MORVAN
 * @Last Modified time: 2019-01-29 14:55:04
 */

#ifndef VOCADOMCORE_HPP
#define VOCADOMCORE_HPP

#include <string>
#include <list>
#include <map>
#include "PluginHandler.hpp"
#include <boost/filesystem.hpp>
#include "ConfigurationManager.hpp"
#include "PluginHandler.hpp"

class VocadomCore
{
  enum eType
  {
    UNKNOWN = 0,
    SERVER,
    CLIENT
  };
  std::string name;
  eType type;
  std::string input;
  std::string output;
  std::map<std::string, PluginHandler *> listOfPlugins;

  bool isRunning = true;
  ConfigurationManager confManager;

public:
  VocadomCore();
  ~VocadomCore();
  VocadomCore(const VocadomCore &) = delete;
  VocadomCore &operator=(const VocadomCore &) = delete;

protected:
  const std::string GetFirstFileFromDirectory(const boost::filesystem::path &path);
  void LoadPlugins();
  void UnloadPlugin(const std::string &libName);

private:
  int RunServer();
  int RunClient();
  void GenRandomWord(char *s, const int len);
  void GetFileList(std::vector<std::string>& fileList,const std::string& path);
  void RunPlugins();

public:
  int Run(const std::string &machineName);
};

#endif //VOCADOMCORE_HPP
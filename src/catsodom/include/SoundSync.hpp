#ifndef SOUNDSYNC_HPP
#define SOUNDSYNC_HPP

#include <iostream>
#include <vector>
#include <assert.h>
#include <string>
#include <fstream>
#include <unordered_map>
#include <iterator>
#include <algorithm>
#include <cstdint>

#define MODERAW2SYNC 0
#define MODESYNC2RAW 1
#define BLOCK_SIZE 160
#define PORT 5050
#define SIZE_HEADER_WAV 44

enum modein{
	MODEWAV = 0,
	MODESTREAM
};

enum class Endianness
{
    LittleEndian,
    BigEndian
};

typedef struct {
    uint16_t numChannels;
    uint16_t sampleRate;
    uint16_t bitDepth;
} SoundInfo, *pSoundInfo;

int16_t twoBytesToInt16 (uint8_t *source, int startIndex,  Endianness endianness = Endianness::LittleEndian);
int32_t fourBytesToInt32 (uint8_t *source, int startIndex, Endianness endianness = Endianness::LittleEndian);
void start_SoundSync(pSoundInfo info, int fdin, int fdout);
void start_SoundDeSync(int fdin, int fdout);

#endif 

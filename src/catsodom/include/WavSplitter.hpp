/*
 * @Author: Tristan MORVAN 
 * @Date: 2019-01-31 14:59:47 
 * @Last Modified by: Tristan MORVAN
 * @Last Modified time: 2019-01-31 16:01:05
 */

#ifndef WAVSPLITTER_HPP
#define WAVSPLITTER_HPP

#include <boost/filesystem.hpp>
#include <string>

class WavSplitter
{
  typedef struct
  {
    // [Bloc de déclaration d'un fichier au format WAVE]
    char FileTypeBlocID[4]; // (4 octets) : Constante «RIFF»  (0x52,0x49,0x46,0x46)
    unsigned int FileSize;  // (4 octets) : Taille du fichier moins 8 octets
    char FileFormatID[4];   // (4 octets) : Format = «WAVE»  (0x57,0x41,0x56,0x45)
    // [Bloc décrivant le format audio]
    char FormatBlocID[4];    // (4 octets) : Identifiant «fmt »  (0x66,0x6D, 0x74,0x20)
    unsigned int BlocSize;   // (4 octets) : Nombre d'octets du bloc - 16  (0x10)
    short int AudioFormat;   // (2 octets) : Format du stockage dans le fichier (1: PCM, ...)
    short int NbrCanaux;     // (2 octets) : Nombre de canaux (de 1 à 6)
    unsigned int Frequence;  // (4 octets) : Fréquence d'échantillonnage (en hertz) [Valeurs standardisées : 11 025, 22 050, 44 100 et éventuellement 48 000 et 96 000]
    unsigned int BytePerSec; // (4 octets) : Nombre d'octets à lire par seconde (c.-à-d., Frequence * BytePerBloc).
    short int BytePerBloc;   // (2 octets) : Nombre d'octets par bloc d'échantillonnage (c.-à-d., tous canaux confondus : NbrCanaux * BitsPerSample/8).
    short int BitsPerSample; // (2 octets) : Nombre de bits utilisés pour le codage de chaque échantillon (8, 16, 24)
  } WavHeader;

  typedef struct
  {
    // [Bloc des données]
    char DataBlocID[4]; // 'data' or 'fact'
    unsigned int DataSize;
  } WavData;

private:
  std::string file;
  boost::filesystem::path outputDirectory;
  unsigned int timeSplitInMilli;

private:
  FILE *fdWavFile;
  unsigned int stat;
  WavHeader pWavHeader;
  WavData pChunkHeader;

private:
  void PrintHeader(WavHeader *pHDR);
  void LoadFile();
  void CloseFile();

public:
  void SplitFile(const std::string &file, const boost::filesystem::path &output, const unsigned int timeInMilliSeconds);
};

#endif //  WAVSPLITTER_HPP
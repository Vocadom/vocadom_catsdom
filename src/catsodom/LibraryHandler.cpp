/*
 * @Author: Tristan MORVAN
 * @Date: 2018-07-23 15:06:31
 * @Last Modified by: Tristan MORVAN
 * @Last Modified time: 2018-07-25 17:38:55
 */

#include <dlfcn.h>
#include <iostream>

#include "LibraryHandler.hpp"

LibraryHandler::LibraryHandler() : libraryHandle(nullptr)
{
}

LibraryHandler::~LibraryHandler()
{
}

void *LibraryHandler::GetFunction(const std::string &funcName)
{
  void *funcPtr = nullptr;
  if (libraryHandle == nullptr)
  {
    std::cerr << "library not loaded" << std::endl;
    throw std::runtime_error("Library not loaded");
  }
  if (mapToFunctions.count(funcName))
  {
    // If the function is already in the slot
    return mapToFunctions[funcName];
  }
  else
  { // Else we need load the symbols
    funcPtr = dlsym(libraryHandle, funcName.c_str());
    if (!funcPtr)
    {
      std::cerr << "dlsym failed" << std::endl;
      throw std::runtime_error(dlerror());
    }
    mapToFunctions[funcName] = funcPtr;
  }
  return funcPtr;
}

void LibraryHandler::LoadLibrary(const std::string &libraryFullPath, const std::string &libraryName)
{
  this->libraryFullPath = libraryFullPath;
  this->libraryName = libraryName;

  if (libraryHandle != nullptr)
  {
    std::cerr << "Library already loaded" << std::endl;
    throw std::runtime_error("Library already loaded");
  }
  if (!boost::filesystem::exists(libraryFullPath))
  {
    std::cerr << "Can't load library: " + libraryFullPath << std::endl;
    throw std::runtime_error("Can't load library: " + libraryFullPath);
  }
  std::cout << "Loading library " + libraryFullPath << std::endl;
  // Getting the allocated slot and load the library
  // Checking if there is any free slot

  // load the library
  libraryHandle = dlopen(libraryFullPath.c_str(), RTLD_NOW);
  if (libraryHandle == nullptr)
  {
    std::cerr << "dlopen failed" << std::endl;
    throw std::runtime_error(dlerror());
  }
}

void LibraryHandler::UnloadLibrary()
{
  if (libraryHandle == nullptr)
  {
    std::cerr << "Library not loaded" << std::endl;
    throw std::runtime_error("Library not loaded");
  }
  dlclose(libraryHandle);
  libraryHandle = nullptr;
}

int LibraryHandler::CallInitialize()
{
  m_funcPtr funcPtr = nullptr;
  funcPtr = (m_funcPtr)GetFunction(libraryName + "_init");
  if (funcPtr != nullptr)
    return (*funcPtr)();
  throw std::runtime_error("Function not found");
}

int LibraryHandler::CallPreProcess()
{
  m_funcPtr funcPtr = nullptr;
  funcPtr = (m_funcPtr)GetFunction(libraryName + "_preprocess");
  if (funcPtr != nullptr)
    return (*funcPtr)();
  throw std::runtime_error("Function not found");
}

int LibraryHandler::CallProcess()
{
  m_funcPtr funcPtr = nullptr;
  funcPtr = (m_funcPtr)GetFunction(libraryName + "_process");
  if (funcPtr != nullptr)
    return (*funcPtr)();
  throw std::runtime_error("Function not found");
}

int LibraryHandler::CallPostProcess()
{
  m_funcPtr funcPtr = nullptr;
  funcPtr = (m_funcPtr)GetFunction(libraryName + "_postprocess");
  if (funcPtr != nullptr)
    return (*funcPtr)();
  throw std::runtime_error("Function not found");
}

int LibraryHandler::CallFinish()
{
  m_funcPtr funcPtr = nullptr;
  funcPtr = (m_funcPtr)GetFunction(libraryName + "_finish");
  if (funcPtr != nullptr)
    return (*funcPtr)();
  throw std::runtime_error("Function not found");
}

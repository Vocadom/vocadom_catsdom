/*
 * @Author: Tristan MORVAN 
 * @Date: 2019-01-31 15:07:52 
 * @Last Modified by: Tristan MORVAN
 * @Last Modified time: 2019-01-31 15:56:07
 */

#include "WavSplitter.hpp"
#include <iostream>
#include <boost/filesystem/fstream.hpp>
#define DEBUG

void WavSplitter::PrintHeader(WavHeader *pHDR)
{
    std::cerr << std::endl;
    std::cerr << "FileTypeBlocID : " << pHDR->FileTypeBlocID << std::endl
              << "FileSize : " << pHDR->FileSize << std::endl
              << "FileFormatID : " << pHDR->FileFormatID << std::endl
              << "FormatBlocID : " << pHDR->FormatBlocID << std::endl
              << "BlocSize: " << pHDR->BlocSize << std::endl
              << "AudioFormat: " << pHDR->AudioFormat << std::endl
              << "NbrCanaux: " << pHDR->NbrCanaux << std::endl
              << "Frequence: " << pHDR->Frequence << std::endl
              << "BytePerSec: " << pHDR->BytePerSec << std::endl
              << "BytePerBloc: " << pHDR->BytePerBloc << std::endl
              << "BitsPerSample: " << pHDR->BitsPerSample << std::endl;
}

void WavSplitter::LoadFile()
{
    char tmpBuffer[8];

    fdWavFile = fopen(file.c_str(), "rb");
    if (fdWavFile == NULL)
        throw std::logic_error("Can't open wav file.");

    /* read riff/wav header */
    stat = fread((void *)&pWavHeader, sizeof(WavHeader), (size_t)1, fdWavFile);
    if (stat != 1)
        throw std::logic_error("WavHeader missing. May be format is not OK!"); // This is tested.

    /* check format of header */
    for (int i = 0; i < 4; i++)
        tmpBuffer[i] = pWavHeader.FileTypeBlocID[i];
    tmpBuffer[4] = 0;
    if (strcmp(tmpBuffer, "RIFF") != 0)
        throw std::logic_error("Bad RIFF format. I am not cool enough to support everything");

    for (int i = 0; i < 4; i++)
        tmpBuffer[i] = pWavHeader.FileFormatID[i];
    tmpBuffer[4] = 0;
    PrintHeader(&pWavHeader);

    if (strcmp(tmpBuffer, "WAVE") != 0)
        throw std::logic_error("Bad WAVE format");
    for (int i = 0; i < 4; i++)
        tmpBuffer[i] = pWavHeader.FormatBlocID[i];
    tmpBuffer[4] = '\0';
    if (strcmp(tmpBuffer, "fmt ") != 0) // not with "fmt" since 4th pos is blank
        throw std::logic_error("Bad fmt format");
    if (pWavHeader.AudioFormat != 1)
        throw std::logic_error(" Bad wav AudioFormat");
    if ((pWavHeader.BitsPerSample != 16) && (pWavHeader.BitsPerSample != 8))
        throw std::logic_error("Bad wav bits per sample");

    //  Skip over any remaining portion of wav header.
    {
        long int rMore = pWavHeader.BlocSize - (sizeof(WavHeader) - 20);
        if (0 != fseek(fdWavFile, rMore, SEEK_CUR))
            throw std::logic_error("Can't seek.");
    }

    // read chunk untill a data chunk is found.
    for (int sFlag = 1; sFlag != 0;)
    {
        // check attempts.
        if (sFlag > 10)
            throw std::logic_error("Too many chunks");

        // read chunk header
        std::cout << "Size of chunk " << sizeof(WavData) << std::endl;
        stat = fread((void *)&pChunkHeader, sizeof(WavData), (size_t)1, fdWavFile);
        if (1 != stat)
            throw std::logic_error(" I just can't read data. Sorry!");

        // check chunk type.
        for (int i = 0; i < 4; i++)
            tmpBuffer[i] = pChunkHeader.DataBlocID[i];
        tmpBuffer[4] = 0;
        if (strcmp(tmpBuffer, "data") == 0)
            break;

        // skip over chunk.
        sFlag++;
        stat = fseek(fdWavFile, pChunkHeader.DataSize, SEEK_CUR);
        if (stat != 0)
            throw std::logic_error("Can't seek.");
    }
}

void WavSplitter::CloseFile()
{
    fclose(fdWavFile);
}

void WavSplitter::SplitFile(const std::string &fileName, const boost::filesystem::path &output, const unsigned int timeInMilliSeconds)
{
    WavHeader pWavHeaderSplit;
    WavData pChunkHeaderSplit;
    char *buff;

    file = fileName;
    outputDirectory = output;
    timeSplitInMilli = timeInMilliSeconds;
    LoadFile();
    memcpy(&pWavHeaderSplit, &pWavHeader, sizeof(WavHeader));
    memcpy(&pChunkHeaderSplit, &pChunkHeader, sizeof(WavData));
    unsigned int NumberOfbytesToWrite = (pWavHeader.BytePerSec * timeInMilliSeconds) / 1000;
    std::cout << "NumberOfbytesToWrite = " << NumberOfbytesToWrite << std::endl;
    pChunkHeaderSplit.DataSize = NumberOfbytesToWrite;
    buff = new char[NumberOfbytesToWrite + 1];

    bool readFile = true;
    int i = 0;
    boost::filesystem::path rawFilename = boost::filesystem::change_extension(file, "");

    while (readFile)
    {
        boost::filesystem::path newFile = output;
        newFile += "/";
        newFile += rawFilename.filename();
        newFile += "_";
        newFile += std::to_string(i);
        newFile += ".wav";
        stat = fread((void *)buff, NumberOfbytesToWrite, (size_t)1, fdWavFile);
        if (1 != stat)
            readFile = false;
        boost::filesystem::ofstream outputFile{newFile};
        outputFile.write(reinterpret_cast<char *>(&pWavHeaderSplit), sizeof(WavHeader));
        outputFile.write(reinterpret_cast<char *>(&pChunkHeaderSplit), sizeof(WavData));
        outputFile.write(reinterpret_cast<char *>(buff), NumberOfbytesToWrite);
        i++;
    }
    std::cout << "Split into " << i << " files" << std::endl;

    delete buff;
    CloseFile();
}
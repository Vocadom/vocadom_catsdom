#!/bin/bash

function start(){
	cd bin
	tmux new-session -d -s catsodom

	## Create the windows on which each node or .launch file is going to run
	tmux send-keys -t catsodom 'tmux new-window -n Event-Manager ' ENTER
	tmux send-keys -t catsodom 'tmux new-window -n StampAudioOnline ' ENTER
	tmux send-keys -t catsodom 'tmux new-window -n Rehauss ' ENTER
	tmux send-keys -t catsodom 'tmux new-window -n RecoMotsClesV2 ' ENTER
	tmux send-keys -t catsodom 'tmux new-window -n SRTU ' ENTER
	tmux send-keys -t catsodom 'tmux new-window -n VAD ' ENTER
	tmux send-keys -t catsodom 'tmux new-window -n RAP ' ENTER
	tmux send-keys -t catsodom 'tmux new-window -n NLU ' ENTER

	## Send the command to each window from window 0
	# Event-Manager
	tmux send-keys -t catsodom "tmux send-keys -t Event-Manager './Vocadom EventManager' ENTER" ENTER
	# StampAudioOnline
	sleep 3
	tmux send-keys -t catsodom "tmux send-keys -t StampAudioOnline './Vocadom StampAudioOnline' ENTER" ENTER
	# Rehauss
	sleep 3
	tmux send-keys -t catsodom "tmux send-keys -t Rehauss 'conda activate $HOME/anaconda3/envs/location_env ;./Vocadom Rehauss' ENTER" ENTER
	# RecoMotsClesV2
	sleep 3
	tmux send-keys -t catsodom "tmux send-keys -t RecoMotsClesV2 './Vocadom RecoMotsClesV2' ENTER" ENTER
	# SRTU
	sleep 3
	tmux send-keys -t catsodom "tmux send-keys -t SRTU './Vocadom SRTU' ENTER" ENTER
	# VAD
	sleep 3
	tmux send-keys -t catsodom "tmux send-keys -t VAD 'conda activate $HOME/anaconda3/envs/SinaVAD ;./Vocadom VAD' ENTER" ENTER
	# RAP
	sleep 3
	tmux send-keys -t catsodom "tmux send-keys -t RAP './Vocadom RAP' ENTER" ENTER
	#NLU
	sleep 3
	tmux send-keys -t catsodom "tmux send-keys -t NLU 'source $HOME/virtenv_seq2seq/anaconda3/bin/activate ;./Vocadom NLU' ENTER" ENTER

	## Start a new line on window 0
	tmux send-keys -t catsodom ENTER

	## Attach to session
	tmux send-keys -t catsodom "tmux select-window -t Event-Manager" ENTER
	tmux attach -t catsodom
	cd ..
}

function stop(){
	killall -9 Vocadom python
	fuser -k 5000/tcp 5002/tcp 5004/tcp 5006/tcp 5008/tcp 50000/tcp
	curl -X POST -H  "Content-Type: text/plain" -H  "accept: application/json" "http://domus-anubis.u-ga.fr:8080/rest/items/StatusMinoucheToggle" -d "OFF"
	tmux kill-ses -t catsodom
}

function test(){
	echo "i bon konsa"
}

case ${1} in
start) start
;;
stop) stop
;;
teste) test
;;
esac





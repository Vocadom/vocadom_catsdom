# **************** Catsodom/Vocadom **********************

Logiciel Catsodom d'exécution de code de traitement du signal acoustique (modules)
Castodom a été instancié pour les besoins du projet VOCADOM, financé par l'Agence Nationale de la Recherche (ANR-16-CE33-0006).
Le logiciel a été testé sur un PC avec un système d'exploitation Ubuntu 20.04 et une carte RME Hammerfall HDSP 9652 (16 voies par fibre optique)

 
LE LOGICIEL EST FOURNI « EN L'ÉTAT », SANS GARANTIE D'AUCUNE SORTE, EXPRESSE OU IMPLICITE, Y COMPRIS, MAIS SANS S'Y LIMITER, LES GARANTIES DE QUALITÉ MARCHANDE, D'ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON.

Le code est fourni avec des logiciels tiers en respectant leur licence dans le dossier 3rd_party. 

Par ailleurs, les modèles de reconnaissance automatique de la parole dans le répertoire Models ne sont pas fournis dans le répertoire du logiciel, car ils ne sont pas compatibles avec une licence MIT. Ceux-ci peuvent être accessibles sur demande et uniquement à des fins non commerciales auprès de francois.portet 'a' imag 'un point' fr. 

Si vous utilisez ce logiciel, veuillez mentionner l'article suivant relatif au projet VOCADOM : *The VocADom Project: Speech Interaction for Well-being and Reliance Improvement*

```
@inproceedings{vacher:hal-01830217,
  TITLE = {{The VocADom Project: Speech Interaction for Well-being and Reliance Improvement}},
  AUTHOR = {Vacher, Michel and Vincent, Emmanuel and Bobillier Chaumon, Marc-Eric and Joubert, Thierry and Portet, Fran{\c c}ois and Fohr, Dominique and Caffiau, Sybille and Desot, Thierry},
  BOOKTITLE = {{MobileHCI 2018 - 20th International Conference on Human-Computer Interaction with Mobile Devices and Services}},
  ADDRESS = {Barcelona, Spain},
  YEAR = {2018}
}


```


# **************** Prérequis ******************************
-	bibliothèque Boost :
	- Vérifier l’existence des bibliothèques Boost 
		-$: cat /usr/include/boost/version.hpp | grep "BOOST_LIB_VERSION"
	- Si vous n'obtenez pas de réponse, installez avec la commande
		- $: sudo apt-get install libboost-all-dev

- environnement d'exécution 
	- sudo apt-get install tmux
	- sudo apt install libmkl-intel-lp64 libmkl-core  libmkl-sequential 
	
	
-	bibliothèque pour l’acquisition audio (Cirdox) :
	- $: sudo apt-get install libsndfile1-dev
	- $: sudo apt-get install libglib2.0-dev
	- $: sudo apt-get install portaudio19-dev
ATTENTION: si les bibliothèques sont obsolètes, il faut simplement essayer les versions supérieures 

-	bibliothèque Kaldi
	- $: ls /usr/lib/ |grep libfst.so.10 (si rien)
	- $: sudo cp 3rd_party/libfst.so.10 /usr/lib/
ATTENTION: la bibliothèque est dans le dossier 3rd_party du projet 



-	installation anaconda3 pour les environnements virtuels des modules Rehauss et VAD
	- https://www.digitalocean.com/community/tutorials/how-to-install-anaconda-on-ubuntu-18-04-quickstart-fr
	- $: bash Anaconda3-2021.05-Linux-x86_64.sh
À la fin de l’installation d’anaconda3 le message suivant s’affiche 
...
Anaconda3 will now be installed into this location:
$HOME/anaconda3
 Press ENTER to confirm the location
 Press CTRL-C to abort the installation
 Or specify a different location below
…
Pour désactiver l’environnement conda à l'ouverture d'un terminal, il faut désactiver le paramètre auto activate base
	- $: conda config --set auto_activate_base false
	
- 	Après l'installation d'anaconda installer les bibliothèques utilisées dans la VAD
	- $: conda install pytorch==1.6.0 cpuonly -c pytorch # or: pip3 install torch==1.6.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
	- $: conda install -c conda-forge pydub
	- $: pip install python_speech_features 
	- $: conda install -c anaconda scipy
	- $: conda install -c conda-forge pysoundfile

- Avant d'installer l'enrênement NLU il faut absolument installer la chaine 

-	Virtenv_seq2seq
      - $: sudo apt install python3-virtualenv
sur votre $HOME/ installer virtenv comme indiqué ci-dessous 
	- $: virtualenv --system-site-packages -p python3 virtenv_seq2seq
Puis activer environnement virtuel (en vous plaçant à l’extérieur de virtenv)
	- $: source virtenv_seq2seq/bin/activate
	
-	Anaconda3
	
Faire un lien symbolique depuis virtenv_seq2seq.
	- $: ln -s $HOME/anaconda3 $HOME/virtenv_seq2seq/anaconda3
Un dossier anaconda3 doit être présent dans virtenv_seq2seq


-	Torch
Activer l'environnement anaconda3 (en vous plaçant à l’extérieur de virtenv)
	- $: source virtenv_seq2seq/anaconda3/bin/activate
Au début de la ligne de commande, « (base) » doit apparaître. L’environnement anaconda3 dans virtenv_seq2seq est maintenant actif.
	- $: pip install torch===1.5.0 -f https://download.pytorch.org/whl/torch_stable.html
Connaître la version de torch installée 
	- $: pip install torchtext==0.7
	- $: python3
	- $: import torch
	- $: print(torch.version.__version__)
	
# **************** Executer les modules***************

-	Compile the project
	- $:  cd catsodom
	- $:  cmake .
	- $:  ./build.sh
	- $: cd bin
	- $: ln -s ../lib lib
	- $: chmod 777 script/*
	- $: cd ../
	- $: ./voc_completion.sh
	- $: ./setenv.sh
	- $: mkdir Out_RecordAudio
	
-	Start the pipe
	- $: ./startStopScript.sh start
-	Stop the pipe
	- $: ./startStopScript.sh stop

# **************** Descriptions des modules ***************
- 	EventManager : 
	- Server TCP
	- Reçoit les événements des modules SRTU/VAD/REHAUSS et les dispatche

- 	StampAudioOnline : 
	- Server TCP 
	- récupère le flux audio d'un micro 
	- ajoute un stamp tout les 10ms 
	- envoie le flux aux clients TCP.

- 	Rehauss : 
	- écoute les 16voix 
	- après détection du mot clé, il sélectionne une voix de l'antenne qui a détecté le  mot clé
	- envoie de la voix détectée à la VAD.
	
- 	RecoMotsClesV2 : 
	- Active les servers Kaldis (4) qui reconnaissent le mot minouche

- 	StreamReadyToUse :	
	- Client du serveur StampStream (Rec ou Wav) 
	- récupère le flux 
	- retire les stamps et enregistre le premier stamp 
	- Envoie aux serveurs Kaldis le flux sans stamps
	- Client du serveur EventManager et lui transmet la détection de mots clés
	
- 	VAD : 
	- écoute la sortie du rehaussement
	- cherche la fin de voix
	- envoie la commande (du mot clé à la fin de voix) à la RAP
	
- 	RAP : 
	- transcription de la commande 
	- envoye la commande transcrite à la NLU
	
- 	NLU : 
	- transcription de la parole en ordre et en action 
	- envoye la commande transcrite à la OpenHab	

- 	StampAudioOffline : 
	- Server TCP
	- récupère des fichiers .WAV 
	- ajoute un stamp tout les 10ms
	- envoie le flux aux clients TCP.


# **************** Arborescence ************************

Catsodom
-	3rd party (Kaldi)
	-	online2-tcp-nnet3-decode-faster
	-	...
-	Models
	-	KeysWords
   		-	ichefix
   		-	minouche
   	-	EnglishWords
-	src
	-	Catsodom
	-	lib_example
		-	lib_RecoMotsCles
		-	...
-	bin
	-	Vocadom
	-	script
		-	ichefix
		-	minouche
-	Data 
	-	Out_RecordAudio
	-	Out_RecoMotsCles
	-	Out_VAD
	-	Out_RAP
	-	Out_NLU
	-	Out_StreamOnly
-	README.md


# **************** Descriptions des dossiers de sorties ***************

-	Out_RecoMotsCles : sortie des Kaldis sans filtre.
-	Out_RecordAudio  : sortie des micros dans des fichiers wav.
-	Out_VAD          : commande en wav 
-	Out_RAP          : commande transcrite
-	Out_NLU          : entrée d'Openhab

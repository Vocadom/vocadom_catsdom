import os

print("Copy all script needed in bin/script")

try: 
	os.makedirs('bin/script/')
except OSError:
	if not os.path.isdir('bin/script/'):
		print('Error to create bin/script')

path = 'src/lib_example/'

def apply_installer(path):
	listfichier = os.listdir(path)
	for f in listfichier:
	        if f=="installer.sh":
		        os.system(path +f)
	        elif (os.path.isdir(os.path.join(path,f)) and (f != "CMakeFiles")):
		        newpath = path + f + '/'
		        apply_installer(newpath)

apply_installer(path)
print("Copy VocadomConf.xml in bin/")
os.system("cp VocadomConf.xml bin/")

print("Copy TestUnitaires in bin/")
os.system("cp TestUnitaires/* bin/")

#!/bin/bash
path=$(pwd)
path+="/VocadomConf.xml"
echo "conf_path=${path}" > tmp_path.txt
cat tmp_path.txt voc_completion.sh > tmp_compl
sudo cp tmp_compl /etc/bash_completion.d/Vocadom_Completion
rm tmp_compl
rm tmp_path.txt

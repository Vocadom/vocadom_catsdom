# /*
#  * @Author: Tristan MORVAN 
#  * @Date: 2019-02-19 11:32:57 
#  * @Last Modified by:   Tristan MORVAN 
#  * @Last Modified time: 2019-02-19 11:32:57 
#  */

find_package(Boost COMPONENTS system filesystem serialization REQUIRED)

set(CMAKE_BUILD_TYPE Debug)

set(CMAKE_CXX_COMPILER             "/usr/bin/g++")
set(CMAKE_CXX_FLAGS                ${CMAKE_CXX_FLAGS} "-std=c++17 -Wall -pthread")
set(CMAKE_CXX_FLAGS_DEBUG          "-g")
set(CMAKE_CXX_FLAGS_MINSIZEREL     "-Os -DNDEBUG")
set(CMAKE_CXX_FLAGS_RELEASE        "-O4 -DNDEBUG")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O2 -g")

include_directories(${Log4cxx_INCLUDE_DIR})


ADD_DEFINITIONS(-DBOOST_TEST_DYN_LINK)
if(Boost_FOUND)
  include_directories(${Boost_INCLUDE_DIRS})
endif()

ADD_DEFINITIONS(-Wfatal-errors)


set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

link_directories(${CMAKE_BINARY_DIR}/lib)


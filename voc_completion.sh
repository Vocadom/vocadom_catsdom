_Vocadom()
{
    local cur opts
    cur="${COMP_WORDS[COMP_CWORD]}"
    opts=$(cat ${conf_path} | grep -oP '(?<=<Machine name=").*(?=")')

    COMPREPLY=( $(compgen -W "${opts}" "${cur}") )

}
complete -F _Vocadom Vocadom
#!/bin/sh

#     Copyright (C) 2012-2016 Laboratoire d'Informatique de Grenoble (www.liglab.fr),
#     Centre National de la Recherche Scientifique (www.cnrs.fr).
#     The initial authors of the original code are Michel Vacher, François Portet 
#     and Frédéric Aman.
#     Contributors: Michel Vacher, François Portet, Frédéric Aman, Fabien Eloy, 
#     William Duclot, Cheick Mahady Sissoko, Dan Istrate, Hubert Glasson, Stéphane 
#     Chaillol and Alain Dufaux.
#     
#     This file is part of CirdoX.
#
#     CirdoX is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     CirdoX is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with CirdoX. If not, see <http://www.gnu.org/licenses/>.
#
#     CirdoX is a software project initiated by the French Centre National 
#     de la Recherche Scientifique (CNRS) and the Laboratoire d'Informatique de 
#     Grenoble  (LIG) - France (https://www.liglab.fr/).
#     This work was supported by the French funding agencies ANR and CNSA through 
#     the CIRDO project (ANR-2010-TECS-012).
#     CirdoX is a modular software able to analyse online the audio environment, 
#     to extract the uttered sentences and then to process them thanks to an ASR stage 
#     followed by a filtering stage.
#
#     Contact Frédéric Aman <frederic.aman@gmail.com>, Michel Vacher
#     <michel.vacher@imag.fr> or François Portet <francois.portet@imag.fr>
#     for more information about the licence or the use of CirdoX.


# specific external dev and libs

export CIRDOX_ROOT=`pwd`
export CIRDOX_GMM_PATH=$CIRDOX_ROOT/files/model/gmm
#get the architecture
if [ "$(uname -m)" != 'x86_64' ];
then
	export USR_LIB_GLIB_INCLUDE_PATH=/usr/lib/i386-linux-gnu/glib-2.0/include
	export CIRDOX_GMM_ALIZE_PATH=$CIRDOX_GMM_PATH/alize/32bits
else
	export USR_LIB_GLIB_INCLUDE_PATH=/usr/lib/x86_64-linux-gnu/glib-2.0/include/
	export CIRDOX_GMM_ALIZE_PATH=$CIRDOX_GMM_PATH/alize/64bits
fi
export USR_INCLUDE_GLIB_PATH=/usr/include/glib-2.0
export USR_INCLUDE_LIBFREENECT_PATH=/usr/local/include/libfreenect
export CIRDOX_LIB_PATH=$CIRDOX_ROOT/lib
export CIRDOX_LIB_PLUGINS_PATH=$CIRDOX_LIB_PATH/plugins
export CIRDOX_INCLUDE_PATH=$CIRDOX_ROOT/src/include
export CIRDOX_PLUGINS_INCLUDE_PATH=./include
export PORTAUDIO_INCLUDE_PATH=$CIRDOX_ROOT/include/plugins/portaudio
export CIRDOX_TOOLS_PATH=$CIRDOX_ROOT/src/tools
export CIRDOX_TOOLS_INCLUDE_PATH=$CIRDOX_TOOLS_PATH/include
export CIRDOX_BIN_PATH=$CIRDOX_ROOT/bin
export CIRDOX_HYP_PATH=$CIRDOX_ROOT/files/output/hyp
export CIRDOX_WAV_PATH=$CIRDOX_ROOT/wav
export CIRDOX_LOG_PATH=$CIRDOX_ROOT/files/output/log
export CIRDOX_XML_PATH=$CIRDOX_ROOT/files/output/xml
export CIRDOX_LAT_PATH=$CIRDOX_ROOT/files/output/lat
export CIRDOX_NBEST_PATH=$CIRDOX_ROOT/files/output/nbest
export CIRDOX_PRM_PATH=$CIRDOX_ROOT/files/output/prm
export CIRDOX_TRN_PATH=$CIRDOX_ROOT/files/output/trn
export CIRDOX_MFC_PATH=$CIRDOX_ROOT/files/output/mfc
export CIRDOX_DAM_PATH=$CIRDOX_ROOT/files/output/dam
export CIRDOX_CFG_PATH=$CIRDOX_ROOT/config
export CIRDOX_ASR_PATH=$CIRDOX_ROOT/asr
export CIRDOX_ASR_TMP_PATH=$CIRDOX_ASR_PATH/tmp
export CIRDOX_SPHINX_TK_PATH=$CIRDOX_ASR_PATH/sphinx
export LIA_PHON_REP=$CIRDOX_ROOT/src/tools/lia_phon
export SPHINX_PATH=$CIRDOX_ROOT/../../sphinx
export SPHINX_MODELS_PATH=$SPHINX_PATH/models
export SPHINX_ACOUSTIC_MODEL=$SPHINX_MODELS_PATH/acoustic_models/am
export SPHINX_LANGUAGE_MODEL=$SPHINX_MODELS_PATH/language_models/lm.DMP
export SPHINX_DICT=$SPHINX_MODELS_PATH/dict/dic.txt
export SPHINX_FILLER=$SPHINX_MODELS_PATH/dict/filler.txt
export CIRDOX_FILTER_PATH=$CIRDOX_ROOT/files/filter

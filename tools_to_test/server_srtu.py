import socket
import subprocess
import os
import signal
import time

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    #connect server to port 55110
    while True:
        try:
            s.bind(('10.5.0.15',55110))
            print("server connected")
            break
        except:
            print("try to connect fail retry in one second")
            time.sleep(1)
            pass
        
    s.listen()

    
    while 1:
        #accept one connection 
        conn, addr = s.accept()

        run       = False
        connected = True
        
        global subchild1
        global subchild2

        #while connection still going
        while connected:
            try:
                data = conn.recv(4)
            except:
                break

            if not data:
                break

            #cmd stop has been sent
            if data == b"stop" and run == True:
                #kill SRTU + RecoMotsCleV2 process
                os.system("python kill_SRTU.py")

                #terminate subprocess
                subchild1.kill()
                subchild2.kill()
                
                conn.send(b"SRTU stop\n")
                run = False

            #cmd play has been sent
            elif data == b"play" and run == False:
                #run RecoMotsClesV2
                subchild2 = subprocess.Popen("exec ./Vocadom RecoMotsClesV2", stdout=subprocess.PIPE,shell=True)
                #wait for process to launch
                time.sleep(2)

                #run SRTU
                subchild1 = subprocess.Popen("exec ./Vocadom SRTU", stdout=subprocess.PIPE,shell=True)
                conn.send(b"SRTU Running\n")

                run = True
            

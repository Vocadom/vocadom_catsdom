from threading import Thread

from struct import pack,unpack

import numpy as np
import socket
import sys

SAMPLING_RATE       = 16000


BytesPerSample = 2

StampPerSec = 100
SizeOfStamp = 8


BytesPerSec     = (SAMPLING_RATE*BytesPerSample) + StampPerSec*SizeOfStamp #Audio Rate bytes plus size of stamps
BytesPer10mSec  =  (SAMPLING_RATE*BytesPerSample)/100
SamplePer10mSec = BytesPer10mSec // BytesPerSample
chunk           = int(BytesPerSec/4)

listSocket = []

def getUnstampData(data,stampChar):
    previousPosition = 0
    unstampData = ()
    t = '<' +str(len(data)//2)+ 'h'

    #get a tuples of little endian short instead of bytes
    dataTuples = unpack(t,data)

    #get list of stamps position
    positions = [i for i in range(len(dataTuples)-1) if dataTuples[i] == stampChar]
    #file has no stamp
    if len(positions) == 0:
        return data, -1

    #get first stamp, convert to int from two short in litlle endian
    firstStamp = unpack('<I',pack('<2h',*dataTuples[positions[0]+2:positions[0]+4]))

    #get data without stamp
    for pos in positions:
        if pos % 164 == 0:
            unstampData     += dataTuples[previousPosition:pos]
            previousPosition = pos + 4

    unstampData += dataTuples[previousPosition:]

    t = '<' + str(len(unstampData)) + 'h'

    #return bytes of data without stamp
    return pack(t,*unstampData), firstStamp[0]

def getStampData(data, firstStamp, nbChannel,stampChar):
    stampData = ()
    t = '<' +str(len(data)//2)+ 'h'

    dataTuples = unpack(t,data)
    stampCount = 0

    samplePerStamp = nbChannel*160
    nbStamp = len(dataTuples)//(nbChannel*160)
    previous = 0


    for i in range(nbStamp):

        stampData  += unpack('<2h',pack('<2h',stampChar,0))
        stampData  += unpack('<2h',pack('<I',firstStamp + stampCount))
        stampData  += dataTuples[previous:previous+samplePerStamp]
        previous   += samplePerStamp
        stampCount += 1



    t = '<' + str(len(stampData)) + 'h'
    return pack(t,*stampData)

def acceptClient(sock:socket):
    while True:
        print("try to connect")
        conn,addr = sock.accept()
        listSocket.append(conn)

def duplicateFromNonStamp(buffer,nbOutChannel,currStamp):
    tmp = "<" +str(len(buffer)//2) +"h"
    buffer2 = unpack(tmp, buffer)
    arr = np.repeat(np.array(buffer2,dtype=np.int16),nbOutChannel)
    arr2 = getStampData(arr.tobytes(),currStamp ,16,0x7fff)
    return arr2
def duplicate(buffer, nbOutChannel):
    unstampbuff,firstStamp = getUnstampData(buffer,0x7fff)
    tmp = "<" +str(len(unstampbuff)//2) +"h"
    buffer2 = unpack(tmp, unstampbuff)
    arr = np.repeat(np.array(buffer2,dtype=np.int16),nbOutChannel)
    arr2 = getStampData(arr.tobytes(),firstStamp ,16,0x7fff)
    return arr2

def main(argv):

    last = b""

    #    f = open("real result","ab+")
    f = open("savingstamp.txt","bw+")
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect(("127.0.0.1",4000))
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s1:


            s1.bind(('127.0.0.1',55000))
            s1.listen()
            t1 = Thread(target=acceptClient,args=(s1,))
            t1.start()

            s.settimeout(100)
            currStamp = 0
            while True:
                try:
                    dataRcv = s.recv(chunk)
                    data = last + dataRcv
                except socket.timeout:
                    print("connection failed")
                    break

                if len(data) < chunk:
                    last = data
                    continue
                elif len(data) >  chunk:
                    last = data[chunk:]
                    data = data[:chunk]
                else:
                    last = b""

                multiChannelData = duplicate(data,16)
                currStamp += StampPerSec//4

                for i,s3 in zip(range(len(listSocket)), listSocket):
                    try:
                        s3.send(multiChannelData)
                       # print(i,"send")
                    except :
                        print("remove")
                        listSocket.remove(s3)



if __name__ == "__main__":
    main(sys.argv)

To test Rehauss without running every module:
run from bin directory in the following order in a new terminal every time:
./Vocadom EventManager
./Vocadom StampAudioOnline
./Vocadom VAD (running SinaVAD conda environnement)
python ../tools_to_test/DuplicateChannel.py (running SinaVAD conda environnement)
./Vocadom Rehauss (running rehauss conda environnement)



When everything is launch and rehauss python script that it is connected to 127.0.0.1 run the following command:
python ../tools_to_test/cmd_to_evt.py



To lauch SRTU server:
-copy kill_SRTU.py and server_srtu.py to bin
-change SRTU stamp_stream and event manager socket to the adress you wan't to use
-on the client side use telnet to send command play and stop. Change Adress of EventManager, Server sending data stream and every process connected to that server.

import subprocess
import os

#get all pid of LORIA process
p  = subprocess.Popen( "ps -a | grep LORIA",
    stdout=subprocess.PIPE, shell=True ).communicate()[0]
LORIA_PROCESS = [s.split()[0] for s in p.decode("utf-8").split("\n") if len(s) != 0]

#kill all LORIA process
for i in LORIA_PROCESS:
    os.system("kill -9 " + i)

#kill Vocadom Process
os.system("killall -9 Vocadom")    
